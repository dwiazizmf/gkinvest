-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2018 at 09:30 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `job_kuningan_city_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `ecom_barang`
--

CREATE TABLE `ecom_barang` (
  `id_barang` int(4) UNSIGNED ZEROFILL NOT NULL,
  `id_jenis_barang` int(3) UNSIGNED ZEROFILL NOT NULL,
  `kode_barang` varchar(20) NOT NULL,
  `id_merk` int(11) NOT NULL,
  `nama_barang` text NOT NULL,
  `type_barang` text NOT NULL,
  `harga_barang` bigint(20) NOT NULL,
  `stock_awal_barang` tinyint(4) NOT NULL,
  `stock_min_barang` tinyint(4) NOT NULL,
  `stock_max_barang` tinyint(4) NOT NULL,
  `satuan_barang` varchar(20) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecom_barang`
--

INSERT INTO `ecom_barang` (`id_barang`, `id_jenis_barang`, `kode_barang`, `id_merk`, `nama_barang`, `type_barang`, `harga_barang`, `stock_awal_barang`, `stock_min_barang`, `stock_max_barang`, `satuan_barang`, `createdAt`, `updatedAt`) VALUES
(0000, 004, '20040002', 15, 'Amplop Coklat Folio', 'Amplop Coklat 1/2 Folio', 24500, 50, 5, 10, 'Pak/100pcs', '2018-10-29 04:50:58', '0000-00-00 00:00:00'),
(0001, 003, '10030001', 7, 'JOYKO Paper Fasteners @ 50 Pcs PF-50C Asourted Clr', 'JOYKO Paper Fasteners @ 50 Pcs PF-50C Asourted Clr', 200000, 0, 3, 3, 'Pak/50pcs', '2018-10-24 00:00:00', '0000-00-00 00:00:00'),
(0002, 004, '10040001', 3, 'GARDA Envelopes Brown  Super Cabinet 140x270mm', 'GARDA Envelopes Brown  Super Cabinet 140x270mm@100', 25400, 5, 5, 10, 'Pak/100pcs', '2018-10-25 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ecom_detail_permintaan_barang`
--

CREATE TABLE `ecom_detail_permintaan_barang` (
  `id_detail_permintaan_barang` int(9) NOT NULL,
  `id_permintaan_barang` int(9) UNSIGNED ZEROFILL NOT NULL,
  `id_barang` int(4) UNSIGNED ZEROFILL NOT NULL,
  `quantity_permintaan_barang` tinyint(3) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecom_detail_permintaan_barang`
--

INSERT INTO `ecom_detail_permintaan_barang` (`id_detail_permintaan_barang`, `id_permintaan_barang`, `id_barang`, `quantity_permintaan_barang`, `createdAt`, `updatedAt`) VALUES
(17, 000000016, 20040002, 1, '2018-11-06 04:13:43', '0000-00-00 00:00:00'),
(18, 000000016, 10040001, 1, '2018-11-06 04:13:43', '0000-00-00 00:00:00'),
(19, 000000017, 10040001, 1, '2018-11-06 04:39:06', '0000-00-00 00:00:00'),
(20, 000000018, 20040002, 1, '2018-11-07 03:29:56', '0000-00-00 00:00:00'),
(21, 000000019, 10040001, 2, '2018-11-07 06:11:58', '0000-00-00 00:00:00'),
(22, 000000020, 10040001, 2, '2018-11-07 08:30:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ecom_jenis_barang`
--

CREATE TABLE `ecom_jenis_barang` (
  `id_jenis_barang` int(3) UNSIGNED ZEROFILL NOT NULL,
  `nama_jenis_barang` varchar(255) NOT NULL,
  `deskripsi_jenis_barang` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecom_jenis_barang`
--

INSERT INTO `ecom_jenis_barang` (`id_jenis_barang`, `nama_jenis_barang`, `deskripsi_jenis_barang`, `createdAt`, `updatedAt`) VALUES
(001, 'Cetakan', 'Globally aggregate mission-critical networks whereas distributed methods of empowerment. Objectively foster visionary systems with progressive e-services. Phosfluorescently streamline excellent human capital via quality outsourcing. Seamlessly.', '2018-10-23 11:42:30', '2018-10-23 11:55:58'),
(002, 'Computer Supplies', 'Globally aggregate mission-critical networks whereas distributed methods of empowerment. Objectively foster visionary systems with progressive e-services. Phosfluorescently streamline excellent human capital via quality outsourcing. Seamlessly.', '2018-10-23 11:43:01', '2018-10-23 11:57:33'),
(003, 'Fileing', 'Globally aggregate mission-critical networks whereas distributed methods of empowerment. Objectively foster visionary systems with progressive e-services. Phosfluorescently streamline excellent human capital via quality outsourcing. Seamlessly.', '2018-10-23 11:57:42', '0000-00-00 00:00:00'),
(004, 'ATK', 'Globally aggregate mission-critical networks whereas distributed methods of empowerment. Objectively foster visionary systems with progressive e-services. Phosfluorescently streamline excellent human capital via quality outsourcing. Seamlessly.', '2018-10-23 11:58:14', '0000-00-00 00:00:00'),
(005, 'Peralatan', 'Globally aggregate mission-critical networks whereas distributed methods of empowerment. Objectively foster visionary systems with progressive e-services. Phosfluorescently streamline excellent human capital via quality outsourcing. Seamlessly.', '2018-10-23 11:58:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ecom_merk`
--

CREATE TABLE `ecom_merk` (
  `id_merk` int(11) NOT NULL,
  `nama_merk` varchar(255) NOT NULL,
  `deskripsi_merk` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecom_merk`
--

INSERT INTO `ecom_merk` (`id_merk`, `nama_merk`, `deskripsi_merk`, `createdAt`, `updatedAt`) VALUES
(1, 'Jaya', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:26:14', '2018-10-26 11:05:58'),
(2, 'Bindek', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:28:55', '2018-10-26 11:04:40'),
(3, 'Kenko/Bona', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:29:09', '2018-10-26 11:06:21'),
(4, 'Rotring', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:29:42', '2018-10-26 11:06:29'),
(5, 'Faster C6', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:29:55', '2018-10-26 11:05:40'),
(6, 'Faster C600', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:30:06', '2018-10-26 11:05:46'),
(7, 'Joyko', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:30:32', '2018-10-26 11:06:04'),
(8, 'Kenko', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:30:44', '2018-10-26 11:06:09'),
(9, 'Daiichi', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:31:01', '2018-10-26 11:04:45'),
(10, 'Bambi 7515', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:31:32', '2018-10-26 11:04:29'),
(11, 'Index', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:31:54', '2018-10-26 11:05:52'),
(12, 'SK', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-23 12:32:02', '2018-10-26 11:06:37'),
(13, 'Test', 'Rapidiously conceptualize one-to-one e-commerce before client-focused convergence. ', '2018-10-26 10:58:19', '2018-10-26 11:06:46'),
(15, 'Tast', '', '2018-10-29 03:44:00', '2018-10-29 03:44:00'),
(16, 'Tost', '', '2018-10-31 10:59:28', '2018-10-31 10:59:28');

-- --------------------------------------------------------

--
-- Table structure for table `ecom_pengguna`
--

CREATE TABLE `ecom_pengguna` (
  `nik` int(10) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `departemen` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `pendidikan_terakhir` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `photo` varchar(50) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecom_pengguna`
--

INSERT INTO `ecom_pengguna` (`nik`, `nama_lengkap`, `username`, `email`, `password`, `departemen`, `tanggal_lahir`, `alamat`, `kode_pos`, `pendidikan_terakhir`, `status`, `photo`, `createdAt`, `updatedAt`) VALUES
(2018013364, 'Rico Oktavian Adhi Wibowo', 'ricooktavianaw', 'ricooktavianaw@gmail.com', '7b307c984ece29310bdfee47a3042d14', 'HR, GA and Warehouse', '1991-10-26', '', 40377, 'S1 Teknik Informatika', '0', 'pict_2018_10_30_10_16_10.jpg', '2018-10-23 05:25:13', '2018-10-30 10:19:51'),
(2018013365, 'Rud Van Nistelrooy', 'salmonnusa-1', 'salmonnusa-1@gmail.com', '7b307c984ece29310bdfee47a3042d14', 'Engineering', '1991-10-26', 'JL. RADEN GANDA KUSUMA NO 20 KEC BANJARAN KAB BANDUNG', 40377, '', '1', 'pict_2018_10_30_11_06_27.jpg', '2018-10-24 06:33:32', '2018-10-30 11:06:20'),
(2018013367, 'Edwin Van Der Sar', 'salmonnusa', 'salmonnusa@gmail.com', '7b307c984ece29310bdfee47a3042d14', 'HR, GA and Warehouse', '1991-10-26', 'JL. RADEN GANDA KUSUMA NO 20 KEC BANJARAN KAB BANDUNG', 40377, '', '1', 'pict_2018_10_30_11_06_27.jpg', '2018-10-24 06:33:32', '2018-10-30 11:06:20'),
(2018013368, 'Paul Scholes', 'admin', 'ricooaw@gmail.com', '7b307c984ece29310bdfee47a3042d14', 'Engineering', '1991-04-11', '', 40378, '', '2', '', '2018-11-01 08:49:02', '2018-11-02 09:05:17'),
(2018013369, 'Eric Cantona', 'ericcantona', 'ericcantona@gmail.com', '7b307c984ece29310bdfee47a3042d14', 'HR, GA and Warehouse', '1991-10-26', 'JL. RADEN GANDA KUSUMA NO 20 KEC BANJARAN KAB BANDUNG', 40377, 'S2', '2', '', '2018-11-06 10:40:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ecom_permintaan_barang`
--

CREATE TABLE `ecom_permintaan_barang` (
  `id_permintaan_barang` int(9) UNSIGNED ZEROFILL NOT NULL,
  `nik` int(10) NOT NULL,
  `status_manager_departemen` varchar(50) NOT NULL,
  `status_permintaan_barang` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecom_permintaan_barang`
--

INSERT INTO `ecom_permintaan_barang` (`id_permintaan_barang`, `nik`, `status_manager_departemen`, `status_permintaan_barang`, `keterangan`, `createdAt`, `updatedAt`) VALUES
(000000020, 2018013369, 'Not Approved', '', '', '2018-11-07 08:30:09', '2018-11-07 08:50:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ecom_barang`
--
ALTER TABLE `ecom_barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_jenis_barang` (`id_jenis_barang`),
  ADD KEY `id_merk` (`id_merk`);

--
-- Indexes for table `ecom_detail_permintaan_barang`
--
ALTER TABLE `ecom_detail_permintaan_barang`
  ADD PRIMARY KEY (`id_detail_permintaan_barang`);

--
-- Indexes for table `ecom_jenis_barang`
--
ALTER TABLE `ecom_jenis_barang`
  ADD PRIMARY KEY (`id_jenis_barang`);

--
-- Indexes for table `ecom_merk`
--
ALTER TABLE `ecom_merk`
  ADD PRIMARY KEY (`id_merk`);

--
-- Indexes for table `ecom_pengguna`
--
ALTER TABLE `ecom_pengguna`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `ecom_permintaan_barang`
--
ALTER TABLE `ecom_permintaan_barang`
  ADD PRIMARY KEY (`id_permintaan_barang`),
  ADD KEY `nik` (`nik`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ecom_detail_permintaan_barang`
--
ALTER TABLE `ecom_detail_permintaan_barang`
  MODIFY `id_detail_permintaan_barang` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `ecom_jenis_barang`
--
ALTER TABLE `ecom_jenis_barang`
  MODIFY `id_jenis_barang` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ecom_merk`
--
ALTER TABLE `ecom_merk`
  MODIFY `id_merk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ecom_permintaan_barang`
--
ALTER TABLE `ecom_permintaan_barang`
  MODIFY `id_permintaan_barang` int(9) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ecom_barang`
--
ALTER TABLE `ecom_barang`
  ADD CONSTRAINT `ecom_barang_ibfk_1` FOREIGN KEY (`id_jenis_barang`) REFERENCES `ecom_jenis_barang` (`id_jenis_barang`),
  ADD CONSTRAINT `ecom_barang_ibfk_2` FOREIGN KEY (`id_merk`) REFERENCES `ecom_merk` (`id_merk`);

--
-- Constraints for table `ecom_permintaan_barang`
--
ALTER TABLE `ecom_permintaan_barang`
  ADD CONSTRAINT `ecom_permintaan_barang_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `ecom_pengguna` (`nik`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
