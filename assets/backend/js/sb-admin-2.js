/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
  // ecom side menu
  $('#side-menu').metisMenu();

  // ecom dataTable
  $('#dataTables-example').DataTable({
    responsive: true
  });

  // ecom wysiwyg
  $('#summernote').summernote({
    height: "350px"
  });

  // ecom hide and password
  $('#ecom-password-hideshow').hidePassword(true);

  // ecom datepicker
  $('.ecom-datepicker').datepicker({});

  // ecom timepicker
  $('.ecom-timepicker').datetimepicker({
    format: 'HH:mm:ss',
    locale: 'ID'
  });

  // ecom jquery mask input for assets code
  $('.ecom-kode-barang').mask('0000');

  // ecom upload image
  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
          filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip col-lg-3\">" +
            "<img class=\"img-rounded img-responsive\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<span class=\"remove btn btn-danger margin-vert-10px\">Hapus</span>" +
            "</span>").appendTo(".result-image");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });          
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
  $(window).bind("load resize", function() {
    var topOffset = 50;
    var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
    if (width < 768) {
      $('div.navbar-collapse').addClass('collapse');
      topOffset = 100; // 2-row-menu
    } else {
      $('div.navbar-collapse').removeClass('collapse');
    }

    var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
    height = height - topOffset;
    if (height < 1) height = 1;
    if (height > topOffset) {
      $("#page-wrapper").css("min-height", (height) + "px");
    }
  });

  var url = window.location;
  // var element = $('ul.nav a').filter(function() {
  //     return this.href == url;
  // }).addClass('active').parent().parent().addClass('in').parent();
  var element = $('ul.nav a').filter(function() {
    return this.href == url;
  }).addClass('active').parent();

  while (true) {
    if (element.is('li')) {
        element = element.parent().addClass('in').parent();
    } else {
        break;
    }
  }
});