<div class="page-content-wrapper">
  <div class="container">
    <!-- Offline Area-->
    <div class="offline-area-wrapper py-3 d-flex align-items-center justify-content-center">
      <div class="offline-text text-center"><img class="mb-4 px-5" src="<?php echo base_url('assets/frontend/img/bg-img/no-internet.png') ?>" alt="">
        <h5>Uh Oh! You're lost</h5>
        <p>The page you are looking for doesn't exist. How you got here is a mystery, But you can click the button below to go back the homepage.</p><a class="btn btn-primary" href="<?php echo base_url('/'); ?>">Back Home</a>
      </div>
    </div>
  </div>
</div>