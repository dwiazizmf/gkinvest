<div class="header-left">
    <a href="#" class="mobile-menu-toggle">
        <i class="d-icon-bars2"></i>
    </a>
</div>
<div class="header-center">
    <a href="<?php echo base_url(); ?>" class="logo">
        <img src="<?php echo base_url(); ?>assets/images/footer_gkinvest_logo.png" alt="logo" width="163" height="39" />
    </a>
    <!-- End Logo -->
    <nav class="main-nav">
        <ul class="menu">
            <!--<li class="active">
                <a href="demo13.html">Home</a>
            </li>-->
            <!--<li>
                <a href="demo13-shop.html">Categories</a>
                <div class="megamenu">
                    <div class="row">
                        <div class="col-6 col-sm-4 col-md-3 col-lg-4">
                            <h4 class="menu-title">Variations 1</h4>
                            <ul>
                                <li><a href="shop-banner-sidebar.html">Banner With Sidebar</a></li>
                                <li><a href="shop-boxed-banner.html">Boxed Banner</a></li>
                                <li><a href="shop-infinite-scroll.html">Infinite Ajaxscroll</a></li>
                                <li><a href="shop-horizontal-filter.html">Horizontal Filter</a>
                                </li>
                                <li><a href="shop-navigation-filter.html">Navigation Filter<span class="tip tip-hot">Hot</span></a></li>

                                <li><a href="shop-off-canvas.html">Off-Canvas Filter</a></li>
                                <li><a href="shop-right-sidebar.html">Right Toggle Sidebar</a></li>
                            </ul>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 col-lg-4">
                            <h4 class="menu-title">Variations 2</h4>
                            <ul>

                                <li><a href="shop-grid-3cols.html">3 Columns Mode<span class="tip tip-new">New</span></a></li>
                                <li><a href="shop-grid-4cols.html">4 Columns Mode</a></li>
                                <li><a href="shop-grid-5cols.html">5 Columns Mode</a></li>
                                <li><a href="shop-grid-6cols.html">6 Columns Mode</a></li>
                                <li><a href="shop-grid-7cols.html">7 Columns Mode</a></li>
                                <li><a href="shop-grid-8cols.html">8 Columns Mode</a></li>
                                <li><a href="shop-list.html">List Mode</a></li>
                            </ul>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 col-lg-4 menu-banner menu-banner1 banner banner-fixed">
                            <figure>
                                <img src="<?php echo base_url(); ?>assets/images/menu/banner-1.jpg" alt="Menu banner" width="221" height="330" />
                            </figure>
                            <div class="banner-content y-50">
                                <h4 class="banner-subtitle font-weight-bold text-primary ls-m">Sale.
                                </h4>
                                <h3 class="banner-title font-weight-bold"><span class="text-uppercase">Up to</span>70% Off</h3>
                                <a href="#" class="btn btn-link btn-underline">shop now<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <a href="demo13-product.html">Products</a>
                <div class="megamenu">
                    <div class="row">
                        <div class="col-6 col-sm-4 col-md-3 col-lg-4">
                            <h4 class="menu-title">Product Pages</h4>
                            <ul>
                                <li><a href="product-simple.html">Simple Product</a></li>
                                <li><a href="product.html">Variable Product</a></li>
                                <li><a href="product-sale.html">Sale Product</a></li>
                                <li><a href="product-featured.html">Featured &amp; On Sale</a></li>

                                <li><a href="product-left-sidebar.html">With Left Sidebar</a></li>
                                <li><a href="product-right-sidebar.html">With Right Sidebar</a></li>
                                <li><a href="product-sticky-cart.html">Add Cart Sticky<span class="tip tip-hot">Hot</span></a></li>
                                <li><a href="product-tabinside.html">Tab Inside</a></li>
                            </ul>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 col-lg-4">
                            <h4 class="menu-title">Product Layouts</h4>
                            <ul>
                                <li><a href="product-grid.html">Grid Images<span class="tip tip-new">New</span></a></li>
                                <li><a href="product-masonry.html">Masonry</a></li>
                                <li><a href="product-gallery.html">Gallery Type</a></li>
                                <li><a href="product-full.html">Full Width Layout</a></li>
                                <li><a href="product-sticky.html">Sticky Info</a></li>
                                <li><a href="product-sticky-both.html">Left &amp; Right Sticky</a>
                                </li>
                                <li><a href="product-horizontal.html">Horizontal Thumb</a></li>

                                <li><a href="#">Build Your Own</a></li>
                            </ul>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3 col-lg-4 menu-banner menu-banner2 banner banner-fixed">
                            <figure>
                                <img src="<?php echo base_url(); ?>assets/images/menu/banner-2.jpg" alt="Menu banner" width="221" height="330" />
                            </figure>
                            <div class="banner-content x-50 text-center">
                                <h3 class="banner-title text-white text-uppercase">Sunglasses</h3>
                                <h4 class="banner-subtitle font-weight-bold text-white mb-0">$23.00
                                    -
                                    $120.00</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <a href="#">Pages</a>
                <ul>
                    <li><a href="about-us.html">About</a></li>
                    <li><a href="contact-us.html">Contact Us</a></li>
                    <li><a href="account.html">Login</a></li>
                    <li><a href="#">FAQs</a></li>
                    <li><a href="error-404.html">Error 404</a></li>
                    <li><a href="coming-soon.html">Coming Soon</a></li>
                </ul>
            </li>
            <li>
                <a href="#">Blog</a>
                <ul>
                    <li><a href="blog-classic.html">Classic</a></li>
                    <li><a href="blog-listing.html">Listing</a></li>
                    <li>
                        <a href="#">Grid</a>
                        <ul>
                            <li><a href="blog-grid-2col.html">Grid 2 columns</a></li>
                            <li><a href="blog-grid-3col.html">Grid 3 columns</a></li>
                            <li><a href="blog-grid-4col.html">Grid 4 columns</a></li>
                            <li><a href="blog-grid-sidebar.html">Grid sidebar</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Masonry</a>
                        <ul>
                            <li><a href="blog-masonry-2col.html">Masonry 2 columns</a></li>
                            <li><a href="blog-masonry-3col.html">Masonry 3 columns</a></li>
                            <li><a href="blog-masonry-4col.html">Masonry 4 columns</a></li>
                            <li><a href="blog-masonry-sidebar.html">Masonry sidebar</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Mask</a>
                        <ul>
                            <li><a href="blog-mask-grid.html">Blog mask grid</a></li>
                            <li><a href="blog-mask-masonry.html">Blog mask masonry</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="post-single.html">Single Post</a>
                    </li>
                </ul>
            </li>
            <li class="d-xl-show">
                <a href="#">Elements</a>
                <ul>
                    <li><a href="element-products.html">Products</a></li>
                    <li><a href="element-typography.html">Typography</a></li>
                    <li><a href="element-titles.html">Titles</a></li>
                    <li><a href="element-categories.html">Product Category</a></li>
                    <li><a href="element-buttons.html">Buttons</a></li>
                    <li><a href="element-accordions.html">Accordions</a></li>
                    <li><a href="element-alerts.html">Alert &amp; Notification</a></li>
                    <li><a href="element-tabs.html">Tabs</a></li>
                    <li><a href="element-testimonials.html">Testimonials</a></li>
                    <li><a href="element-blog-posts.html">Blog Posts</a></li>
                    <li><a href="element-instagrams.html">Instagrams</a></li>
                    <li><a href="element-cta.html">Call to Action</a></li>
                    <li><a href="element-icon-boxes.html">Icon Boxes</a></li>
                    <li><a href="element-icons.html">Icons</a></li>
                </ul>
            </li>
            <li>
                <a href="#">Buy Donald!</a>
            </li>-->
        </ul>
    </nav>
    <!-- <span class="divider"></span>
     End Divider
    <div class="header-search hs-toggle">
        <a href="#" class="search-toggle">
            <i class="d-icon-search"></i>
        </a>
        <form action="#" class="input-wrapper">
            <input type="text" class="form-control" name="search" autocomplete="off" placeholder="Search your keyword..." required />
            <button class="btn btn-search" type="submit">
                <i class="d-icon-search"></i>
            </button>
        </form>
    </div>
     End Header Search -->
</div>
<div class="header-right">
    <!--<a class="wishlist" href="wishlist.html">
        <i class="d-icon-heart"></i>
        <span>Wishlist</span>
    </a>
     End Login -->
    <?php if ($this->session->userdata('is_login') == TRUE) : ?>
        <a href="<?php echo base_url(); ?>Auth/logout">
            <span>LOGOUT</span>
        </a>
    <?php else : ?>
        <a class="login" href="<?php echo base_url(); ?>home/login_view">
            <i class="d-icon-user"></i>
            <span>Login</span>
        </a>
    <?php endif; ?>
    <a class="mobile-link" href="wishlist.html">
        <i class="d-icon-heart"></i>
    </a>
    <!-- End Login -->
    <?php if ($this->session->userdata('is_login') == TRUE) : ?>
        <div class="dropdown cart-dropdown">
            <a href="#" class="cart-toggle">
                <span class="cart-label">
                    <span class="cart-name"><?php echo gkInvestRupiah($point[0]['yourPoint']); ?></span>
                    <span class="cart-price"><?php echo gkInvestRupiah($product_reward_active['minimum_deposit']); ?></span>
                </span>
                <i class="minicart-icon">
                    <span class="cart-count">1</span>
                </i>
            </a>
            <div class="dropdown-box">
                <div class="product product-cart-header">
                    <span class="product-cart-counts">1 items</span>
                    <span><a href="users/your_reward_active">View Detail</a></span>
                </div>
                <div class="products scrollable">
                    <div class="product product-cart">
                        <div class="product-detail">
                            <a href="users/your_reward_active" class="product-name"><?php echo $product_reward_active['name_reward']; ?></a>
                            <div class="price-box">
                                <span class="product-quantity"> <?php
                                                                $datetime1 = new DateTime($product_reward_active['start_reward']);
                                                                $datetime2 = new DateTime($product_reward_active['end_reward']);
                                                                $interval = $datetime1->diff($datetime2);
                                                                $interval_format = $interval->format('%a');
                                                                echo $interval_format . " days";
                                                                ?></span>
                                <span class="product-price"><?php echo gkInvestRupiah($product_reward_active['minimum_deposit']); ?></span>
                            </div>
                        </div>
                        <figure class="product-media">
                            <a href="#">
                                <img src="<?php echo base_url('upload/be/product-rewards/') . $product_reward_active['photo_reward']; ?>" alt="product" width="90" height="90" />
                            </a>
                            <!--<button class="btn btn-link btn-close">
                                <i class="fas fa-times"></i>
                            </button>-->
                        </figure>
                    </div>

                </div>
                <div class="cart-total">

                    <?php
                    $datetime1 = new DateTime("now");
                    $datetime2 = new DateTime($product_reward_active['end_reward']);
                    $interval = $datetime1->diff($datetime2);
                    $interval_format = $interval->format('%a');

                    ?>

                    <span class="cart-label">
                        <?php echo (strtotime("now") > strtotime($product_reward_active['end_reward'])) ? '<span class="cart-name" style="color:red;"> Expired' :  '<span class="cart-name">' . $interval_format . " days left  "; ?></span>
                    <span class="cart-price"><?php
                                                if ($product_reward_active['minimum_deposit'] > $point[0]['yourPoint']) {
                                                    echo " " . gkInvestRupiah($product_reward_active['minimum_deposit'] - $point[0]['yourPoint']) . " point left";
                                                } else {
                                                    echo "  Take it";
                                                } ?></span>
                    </span>
                </div>
                <div class="cart-action">
                    <a href="users/your_reward_active" class="btn btn-dark"><span>Detail</span></a>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>