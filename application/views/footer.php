<footer class="footer">
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-lg-3">
                    <a href="demo13.html" class="logo-footer">
                        <img src="<?php echo base_url(); ?>assets/images/footer_gkinvest_logo.png" alt="logo-footer" width="163" height="39" />
                    </a>
                    <!-- End FooterLogo -->
                </div>
                <div class="col-lg-9">
                    <div class="widget widget-newsletter form-wrapper form-wrapper-inline">
                        <div class="newsletter-info mx-auto mr-lg-2 ml-lg-4">
                            <h4 class="widget-title">Subscribe to our Newsletter</h4>
                            <p>Get all the latest information on Events, Sales and Offers.</p>
                        </div>
                        <form action="#" class="input-wrapper input-wrapper-inline">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email address here..." required />
                            <button class="btn btn-primary btn-md ml-2" type="submit">subscribe<i class="d-icon-arrow-right"></i></button>
                        </form>
                    </div>
                    <!-- End Newsletter -->
                </div>
            </div>
        </div>
        <!-- End FooterTop -->
        <div class="footer-middle">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="widget">
                        <h4 class="widget-title">Contact Info</h4>
                        <ul class="widget-body">
                            <li>
                                <label>Phone:</label>
                                <a href="#">Toll Free +6221 22083010</a>
                            </li>
                            <li>
                                <label>Email:</label>
                                <a href="#">contact@gkinvest.co.id</a>
                            </li>
                            <li>
                                <label>Address:</label>
                                <a href="#">Multivision Tower Lt. 20, Jl. Kuningan Mulia Lot 9B - Jakarta Selatan 12980</a>
                            </li>
                            <li>
                                <label>WORKING DAYS/HOURS</label>
                            </li>
                            <li>
                                <a href="#">Mon - Sun / 9:00 AM - 6:00 PM</a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Widget -->
                </div>
                <div class="col-lg-6 col-md-6"></div>
                <!--<div class="col-lg-3 col-md-6">
                    <div class="widget ml-lg-4">
                        <h4 class="widget-title">My Account</h4>
                        <ul class="widget-body">
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Order History</a>
                            </li>
                            <li>
                                <a href="#">Returns</a>
                            </li>
                            <li>
                                <a href="#">Custom Service</a>
                            </li>
                            <li>
                                <a href="#">Terms &amp; Condition</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="widget ml-lg-4">
                        <h4 class="widget-title">Customer Service</h4>
                        <ul class="widget-body">
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Order History</a>
                            </li>
                            <li>
                                <a href="#">Returns</a>
                            </li>
                            <li>
                                <a href="#">Custom Service</a>
                            </li>
                            <li>
                                <a href="#">Terms &amp; Condition</a>
                            </li>
                        </ul>
                    </div>
                   
                </div>-->
                <div class="col-lg-3 col-md-6 pull-right">
                    <div class="widget widget-instagram">
                        <h4 class="widget-title">SOCIAL MEDIA</h4>
                        <figure class="widget-body">
                            <div class="footer-right">
                                <div class="social-links">
                                    <a href="https://www.facebook.com/GlobalKapitalInvestama/" target="_blank" class="social-link social-facebook fab fa-facebook-f"></a>
                                    <a href="https://www.instagram.com/gkinvestindonesia/" target="_blank" class="social-link social-instagram fab fa-instagram"></a>
                                    <a href="https://www.youtube.com/channel/UCadtu0ONHmLQciK1Oxo3xjg" class="social-link social-youtube fab fa-youtube" target="_blank"></a>
                                    <a href="https://twitter.com/GKInvest" target="_blank" class="social-link social-twitter fab fa-twitter"></a>
                                    <a href="https://www.linkedin.com/company/gkinvest" target="_blank" class="social-link social-linkedin fab fa-linkedin-in"></a>
                                    <a href="https://t.me/gkinvestID" target="_blank" class="social-link social-telegram fab fa-telegram"></a>
                                </div>
                            </div>
                        </figure>
                    </div>
                    <!-- End Instagram -->
                </div>
            </div>
        </div>
        <!-- End FooterMiddle -->
        <div class="footer-bottom">
            <div class="footer-left">
                <p class="copyright">GkInvest &copy; 2020. All Rights Reserved</p>
            </div>
        </div>
        <!-- End FooterBottom -->
    </div>
</footer>