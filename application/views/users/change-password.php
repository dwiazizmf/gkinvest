<?php 
if( !$this->session->userdata('is_login') && !$this->session->userdata('id') ){ redirect('/'); }
?>

<section class="ecom-header-title-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="text-light">Profil</h1>
				<ul class="breadcumb">
					<li><a href="<?php echo base_url('/') ?>">Beranda</a></li>
					<li><span>Ubah Password</span></li>
				</ul>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section><!-- /.ecom-header-title-area -->

<section>
	<div class="ecom-homepage container margin-top-60px">
	  <div class="row">
	    <div class="col-lg-12">
	      <?php $this->load->view('notification') ?>
	    </div><!-- /.col-lg-12 -->
	  </div><!-- /.row -->


	  <div class="row">
	    <div class="col-md-12 margin-bottom-60px">
			  <form method="post" action="<?php echo base_url('user/put') ?>">
			    <div class="row">
			      <div class="col-lg-12">
			        <div class="form-group">
			          <input type="hidden" value="<?php echo $user['nik']; ?>" name="nik" />
			        </div>
			      </div><!-- /.col-lg-12 -->
			    </div><!-- /.row -->

			    <div class="row">
			      <div class="col-lg-4">
			        <h4>Password</h4>
			      </div><!-- /.col-lg-4 -->

			      <div class="col-lg-8">
			        <div class="form-group">
			          <label>Password Baru</label>
			          <input  type="password" 
			                  class="form-control" 
			                  name="password"
			                  placeholder="Masukkan password baru untuk akses masuk sistem" 
			                  minlength="6" 
			                  required>
			        </div><!-- /.form-group -->

			        <div class="form-group">
			          <label>Username</label>
			          <input  type="password" 
			                  class="form-control" 
			                  name="confirm_password"
			                  placeholder="Masukkan konfirmasi password"
			                  required="required">
			        </div><!-- /.form-group -->

			        <div class="form-group">
			          <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="updatedAt">
			        </div><!-- /.form-group -->
			      </div><!-- /.col-lg-8 -->
			    </div><!-- /.row -->

			    <div class="row">
			      <div class="col-lg-4">
			        &nbsp;
			      </div><!-- /.col-lg-4 -->

			      <div class="col-lg-8">
			        <button class="btn btn-primary" type="submit">Ubah Data Password Pengguna</button>
			      </div><!-- /.col-lg-8 -->
			    </div><!-- /.row -->
			  </form>
	    </div><!-- /.col-md-12 -->
	  </div><!-- /.row -->
	</div><!-- /.container -->
</section>



