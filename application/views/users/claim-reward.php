<div class="page-content-wrapper">
  <div class="container">
    <div class="my-order-wrapper py-3">
      <!-- Single Order Status-->
      <div class="single-order-status">
        <div class="card bg-warning mb-3">
          <div class="card-body d-flex align-items-center">
            <div class="order-icon"><i class="lni lni-checkmark-circle"></i></div>
            <div class="order-status">Claimed</div>
          </div>
        </div>
        <div class="row g-3">
          <!-- Single Ordered Product Card-->
          <?php foreach($product_reward_active as $row): ?>
          <div class="col-4 col-sm-4 col-lg-3">
            <div class="card flash-sale-card">
              <div class="card-body"><a href="<?php echo base_url('product_rewards/detail/'.$row['id_reward']); ?>"><img src="<?php echo base_url('upload/be/product-rewards/').$row['photo_reward']; ?>" alt=""><span class="product-title"><?php echo $row['name_reward']; ?></span>
                  <p class="sale-price"><?php echo $row['minimum_deposit']; ?></p></a></div>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</div>