<?php 
if( !$this->session->userdata('is_login') && !$this->session->userdata('id') ){ redirect('/'); }
?>

<div class="page-content-wrapper">
  <div class="container">
    <!-- Profile Wrapper-->
    <div class="profile-wrapper-area py-3">
      <!-- User Information-->
      <div class="card user-info-card">
        <div class="card-body p-4 d-flex align-items-center">
          <div class="user-profile mr-3">
          	<img src="<?php echo base_url('upload/fe/users/').$user['photo']; ?>" alt="">
          </div><!-- /.user-profile mr-3 -->

          <div class="user-info">
            <p class="mb-0 text-white">@<?php echo $user['username']; ?></p>
            <h5 class="mb-0"><?php echo $user['fullname']; ?></h5>
          </div><!-- /.user-info -->
        </div><!-- /.card-body p-4 d-flex align-items-center -->
      </div><!-- /.card user-info-card -->
      <!-- User Meta Data-->
      <div class="card user-data-card">
        <div class="card-body">
          <form class="" method="post" action="<?php echo base_url('users/put'); ?>" enctype="multipart/form-data">
          	<div class="row">
				      <div class="col-lg-4 profile-settings-sidebar">
				        <h4>Profile Photo</h4>
				        <p>Anda dapat mengubah avatar Anda di sini</p>
				      </div><!-- /.col-lg-4 profile-settings-sidebar -->
				      <div class="col-lg-8">
				        <div class="row">
				          <?php // if($user['photo'] == NULL): ?>
				          <div class="col-lg-6 result-image"></div><!-- /.col-lg-6 -->

				          <div class="col-lg-6">
				            <h5>Upload New Photo</h5>

				            <label class="btn btn-primary">
				              Browse&hellip; <input type="file" name="file" id="files" style="display: none;">
				            </label>
				            <div class="form-text text-muted">The maximum file size allowed is 200KB.</div>  
				          </div><!-- /.col-lg-6 -->

				          <?php // else : ?>

				          <?php 
				          /*
				          <div class="col-lg-12">
				            <img src="<?php echo base_url('upload/fe/users/').$user['photo']; ?>" alt="">
				          </div><!-- /.col-lg-12 -->
				          */
				         	?>

				          <?php // endif; ?>    
				        </div><!-- /.row -->
				      </div><!-- /.col-lg-8 -->
				    </div><!-- /.row -->

				    <hr>

				    <div class="mb-3">
				    	<input class="form-control" type="hidden" value="<?php echo $user['id_user']; ?>" name="id_user">
				    </div><!-- /.mb-3 -->
            <div class="mb-3">
              <div class="title mb-2"><i class="lni lni-user"></i><span>Username</span></div>
              <input class="form-control" type="text" name="username" value="<?php echo $user['username']; ?>">
            </div><!-- /.mb-3 -->
            <div class="mb-3">
              <div class="title mb-2"><i class="lni lni-user"></i><span>Full Name</span></div>
              <input class="form-control" type="text" name="fullname" value="<?php echo $user['fullname']; ?>">
            </div><!-- /.mb-3 -->
            <div class="mb-3">
              <div class="title mb-2"><i class="lni lni-phone"></i><span>Phone</span></div>
              <input class="form-control" type="text" name="handphone" value="<?php echo $user['handphone']; ?>">
            </div><!-- /.mb-3 -->
            <div class="mb-3">
              <div class="title mb-2"><i class="lni lni-envelope"></i><span>Email Address</span></div>
              <input class="form-control" type="email" name="email" value="<?php echo $user['email']; ?>">
            </div><!-- /.mb-3 -->
            <div class="mb-3">
              <div class="title mb-2"><i class="lni lni-map-marker"></i><span>Address</span></div>
              <input class="form-control" type="text" name="alamat" value="<?php echo $user['alamat']; ?>">
            </div><!-- /.mb-3 -->

            <button class="btn btn-success w-100" type="submit">Save All Changes</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>