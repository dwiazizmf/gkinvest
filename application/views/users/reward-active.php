<main class="main mt-6">
  <div class="page-content mb-10">
    <div class="container-fluid">
      <div class="product product-single row">
        <div class="col-md-6">
          <div class="product-gallery pg-vertical">
            <div class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1">
              <figure class="product-image">
                <img src="<?php echo base_url('upload/be/product-rewards/') . $product_reward_active['photo_reward']; ?>" data-zoom-image="<?php echo base_url('upload/be/product-rewards/') . $product_reward_active['photo_reward']; ?>" alt="Blue Pinafore Denim Dress" width="732" height="822">
              </figure>
            </div>
            <div class="product-thumbs-wrap">
              <div class="product-thumbs">
                <div class="product-thumb active">
                  <img src="<?php echo base_url('upload/be/product-rewards/') . $product_reward_active['photo_reward']; ?>" alt="product thumbnail" width="130" height="146">
                </div>
              </div>
              <button class="thumb-up disabled"><i class="fas fa-chevron-left"></i></button>
              <button class="thumb-down disabled"><i class="fas fa-chevron-right"></i></button>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="product-details">
            <div class="product-navigation">
              <ul class="breadcrumb breadcrumb-lg">
                <li><a href="demo13.html"><i class="d-icon-home"></i></a></li>
                <li><a href="#" class="active">Products</a></li>
                <li>Detail</li>
              </ul>

              <ul class="product-nav">
                <li class="product-nav-prev">
                  <a href="#">
                    <i class="d-icon-arrow-left"></i> Prev
                    <span class="product-nav-popup">
                      <img src="<?php echo base_url(); ?>assets/images/product/product-thumb-prev.jpg" alt="product thumbnail" width="110" height="123">
                      <span class="product-name">Sed egtas Dnte Comfort</span>
                    </span>
                  </a>
                </li>
                <li class="product-nav-next">
                  <a href="#">
                    Next <i class="d-icon-arrow-right"></i>
                    <span class="product-nav-popup">
                      <img src="<?php echo base_url(); ?>assets/images/product/product-thumb-next.jpg" alt="product thumbnail" width="110" height="123">
                      <span class="product-name">Sed egtas Dnte Comfort</span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>

            <h1 class="product-name"><?php echo $product_reward_active['name_reward']; ?></h1>
            <div class="product-meta">
              SKU: <span class="product-sku">12345670</span>
              BRAND: <span class="product-brand">The Northland</span>
            </div>
            <div class="product-price">Minimum Point :</div>
            <div class="product-price"><?php echo gkInvestRupiah($product_reward_active['minimum_deposit']); ?></div>
            <div class="ratings-container">
              <!--<div class="ratings-full">
                <span class="ratings" style="width:80%"></span>
                <span class="tooltiptext tooltip-top"></span>
            </div>-->
              <a href="#product-tab-reviews" class="link-to-tab rating-reviews">( 6 reviews )</a>
            </div>
            <p class="product-short-desc">Short detail here </p>
            <!--<div class="product-form product-color">
              <label>Color:</label>
              <div class="product-variations">
                <a class="color" data-src="<?php echo base_url(); ?>assets/images/demos/demo7/products/big1.jpg" href="#" style="background-color: #d99e76"></a>
                <a class="color" data-src="<?php echo base_url(); ?>assets/images/demos/demo7/products/2.jpg" href="#" style="background-color: #267497"></a>
                <a class="color" data-src="<?php echo base_url(); ?>assets/images/demos/demo7/products/3.jpg" href="#" style="background-color: #9a999d"></a>
                <a class="color" data-src="<?php echo base_url(); ?>assets/images/demos/demo7/products/4.jpg" href="#" style="background-color: #2b2b2b"></a>
              </div>
            </div>
            <div class="product-form product-size">
              <label>Size:</label>
              <div class="product-form-group">
                <div class="product-variations">
                  <a class="size" href="#">S</a>
                  <a class="size" href="#">M</a>
                  <a class="size" href="#">L</a>
                  <a class="size" href="#">XL</a>
                  <a class="size" href="#">2XL</a>
                </div>
                <a href="#" class="size-guide"><i class="d-icon-ruler"></i>Size
                  Guide</a>
                <a href="#" class="product-variation-clean">Clean All</a>
              </div>
            </div>
            <div class="product-variation-price">
              <span>$239.00</span>
            </div>-->

            <hr class="product-divider">

            <div class="product-form product-qty">
              <!--<label>QTY:</label>-->
              <div class="product-form-group">
                <!--<div class="input-group">
                  <button class="quantity-minus d-icon-minus"></button>
                  <input class="quantity form-control" type="number" min="1" max="1000000">
                  <button class="quantity-plus d-icon-plus"></button>
                </div>-->
                <button class="btn-product btn-cart"><i class="d-icon-bag"></i>Join Reward</button>
              </div>
            </div>
            <!--
            <hr class="product-divider mb-3">

            <div class="product-footer">
              <div class="social-links">
                <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
                <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                <a href="#" class="social-link social-vimeo fab fa-vimeo-v"></a>
              </div>
              <div class="product-action">
                <a href="#" class="btn-product btn-wishlist"><i class="d-icon-heart"></i>Add To
                  Wishlist</a>
                <span class="divider"></span>
                <a href="#" class="btn-product btn-compare"><i class="d-icon-random"></i>Add To
                  Compare</a>
              </div>
            </div>-->

            <div class="tab tab-nav-simple product-tabs">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" href="#product-tab-description">Description</a>
                </li>
                <!--
                <li class="nav-item">
                  <a class="nav-link" href="#product-tab-shipping-returns">Shipping &amp;
                    Returns</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#product-tab-reviews">Reviews (8)</a>
                </li>-->
              </ul>
              <div class="tab-content">
                <div class="tab-pane active in" id="product-tab-description">
                  <p><?php echo $product_reward_active['detail_reward']; ?> </p>
                </div>
                <div class="tab-pane " id="product-tab-shipping-returns">
                  <h6 class="mb-2 font-secondary">Free Shipping</h6>
                  <p class="mb-0">We deliver to over 100 countries around the world. For full
                    details of the delivery options we offer, please view our <a href="#" class="text-primary">Delivery information</a><br />We hope you’ll
                    love every purchase, but if you ever need to return an item you can do
                    so within a month of receipt. For full details of how to make a return,
                    please view our <br /><a href="#" class="text-primary">Returns
                      information</a></p>
                </div>
                <div class="tab-pane " id="product-tab-reviews">
                  <div class="d-flex align-items-center mb-5">
                    <h4 class="mb-0 mr-2 font-secondary">Average Rating:</h4>
                    <div class="ratings-container average-rating mb-0">
                      <div class="ratings-full">
                        <span class="ratings" style="width:80%"></span>
                        <span class="tooltiptext tooltip-top">4.00</span>
                      </div>
                    </div>
                  </div>

                  <div class="comments mb-6">
                    <ul>
                      <li>
                        <div class="comment">
                          <figure class="comment-media">
                            <a href="#">
                              <img src="<?php echo base_url(); ?>assets/images/blog/comments/1.jpg" alt="avatar">
                            </a>
                          </figure>
                          <div class="comment-body">
                            <div class="comment-rating ratings-container mb-0">
                              <div class="ratings-full">
                                <span class="ratings" style="width:80%"></span>
                                <span class="tooltiptext tooltip-top">4.00</span>
                              </div>
                            </div>
                            <div class="comment-user">
                              <h4><a href="#">Jimmy Pearson</a></h4>
                              <span class="comment-date">November 9, 2018 at 2:19
                                pm</span>
                            </div>

                            <div class="comment-content">
                              <p>Sed pretium, ligula sollicitudin laoreet viverra,
                                tortor libero sodales leo, eget blandit nunc
                                tortor eu nibh. Nullam mollis. Ut justo.
                                Suspendisse potenti.
                                Sed egestas, ante et vulputate volutpat, eros
                                pede semper est, vitae luctus metus libero eu
                                augue. Morbi purus libero, faucibus adipiscing,
                                commodo quis, avida id, est. Sed lectus.
                                Praesent elementum hendrerit tortor. Sed semper
                                lorem at felis. Vestibulum volutpat.</p>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="comment">
                          <figure class="comment-media">
                            <a href="#">
                              <img src="<?php echo base_url(); ?>assets/images/blog/comments/3.jpg" alt="avatar">
                            </a>
                          </figure>

                          <div class="comment-body">
                            <div class="comment-rating ratings-container mb-0">
                              <div class="ratings-full">
                                <span class="ratings" style="width:80%"></span>
                                <span class="tooltiptext tooltip-top">4.00</span>
                              </div>
                            </div>
                            <div class="comment-user">
                              <h4><a href="#">Johnathan Castillo</a></h4>
                              <span class="comment-date">November 9, 2018 at 2:19
                                pm</span>
                            </div>

                            <div class="comment-content">
                              <p>Vestibulum volutpat, lacus a ultrices sagittis,
                                mi neque euismod dui, eu pulvinar nunc sapien
                                ornare nisl. Phasellus pede arcu, dapibus eu,
                                fermentum et, dapibus sed, urna.</p>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <!-- End Comments -->
                  <div class="reply">
                    <div class="title-wrapper text-left">
                      <h3 class="title title-simple text-left text-normal">Add a Review
                      </h3>
                      <p>Your email address will not be published. Required fields are
                        marked *</p>
                    </div>
                    <div class="rating-form">
                      <label for="rating">Your rating: </label>
                      <span class="rating-stars selected">
                        <a class="star-1" href="#">1</a>
                        <a class="star-2" href="#">2</a>
                        <a class="star-3" href="#">3</a>
                        <a class="star-4 active" href="#">4</a>
                        <a class="star-5" href="#">5</a>
                      </span>

                      <select name="rating" id="rating" required="" style="display: none;">
                        <option value="">Rate…</option>
                        <option value="5">Perfect</option>
                        <option value="4">Good</option>
                        <option value="3">Average</option>
                        <option value="2">Not that bad</option>
                        <option value="1">Very poor</option>
                      </select>
                    </div>
                    <form action="#">
                      <textarea id="reply-message" cols="30" rows="4" class="form-control mb-4" placeholder="Comment *" required></textarea>
                      <div class="row">
                        <div class="col-md-6 mb-5">
                          <input type="text" class="form-control" id="reply-name" name="reply-name" placeholder="Name *" required />
                        </div>
                        <div class="col-md-6 mb-5">
                          <input type="email" class="form-control" id="reply-email" name="reply-email" placeholder="Email *" required />
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary btn-md">Submit<i class="d-icon-arrow-right"></i></button>
                    </form>
                  </div>
                  <!-- End Reply -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section>
        <h2 class="title title-simple">Featured Item</h2>


        <div class="owl-carousel owl-theme row cols-xl-6 cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                            'items': 5,
                            'nav': false,
                            'dots': true,
                            'loop': false,
                            'margin': 20,
                            'responsive': {
                                '0': {
                                    'items': 2
                                },
                                '768': {
                                    'items': 3
                                },
                                '992': {
                                    'items': 4
                                },
                                '1200': {
                                    'items': 6,
                                    'dots': false
                                }
                            }
                        }">

          <?php foreach ($product_random as $row) : ?>
            <div class="product">
              <figure class="product-media">
                <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>">
                  <img src="<?php echo base_url('upload/be/product-rewards/') . $row['photo_reward']; ?>" alt="product" width="220" height="245">
                </a>
                <div class="product-action-vertical">
                  <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                </div>
                <div class="product-action">
                  <a href="#" class="btn-product btn-quickview" id="<?php echo $row['id_reward']; ?>" title="Quick View">Quick
                    View</a>
                </div>
              </figure>
              <div class="product-details">
                <!--<a href="#" class="btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>-->
                <div class="product-cat">
                  <a href="shop-grid-3col.html">categories</a>
                </div>
                <h3 class="product-name">
                  <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>"><?php echo $row['name_reward']; ?></a>
                </h3>
                <div class="product-price">
                  <span class="price"><?php echo $row['minimum_deposit']; ?></span>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </section>
    </div>
  </div>
</main>