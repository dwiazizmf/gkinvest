<div class="product product-single row product-popup">
    <div class="col-md-6">
        <div class="product-gallery">
            <div class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1">
                <figure class="product-image">
                    <img src="<?php echo base_url('upload/be/product-rewards/') . $product_rewards['photo_reward']; ?>" alt="Blue Pinafore Denim Dress" width="580" height="580">
                </figure>

            </div>
            <div class="product-thumbs-wrap">
                <div class="product-thumbs">
                    <div class="product-thumb active">
                        <img src="<?php echo base_url('upload/be/product-rewards/') . $product_rewards['photo_reward']; ?>" alt="product thumbnail" width="137" height="137">
                    </div>

                </div>
                <button class="thumb-up disabled"><i class="fas fa-chevron-left"></i></button>
                <button class="thumb-down disabled"><i class="fas fa-chevron-right"></i></button>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="product-details scrollable">
            <h2 class="product-name"><a href="product.html"><?php echo $product_rewards['name_reward']; ?></a></h2>
            <div class="product-meta">
                SKU: <span class="product-sku">12345670</span>
                BRAND: <span class="product-brand">The Northland</span>
            </div>
            <div class="product-price"><?php echo gkInvestRupiah($product_rewards['minimum_deposit']); ?></div>
            <div class="ratings-container">
                <div class="ratings-full">
                    <span class="ratings" style="width:80%"></span>
                    <span class="tooltiptext tooltip-top"></span>
                </div>
                <a href="#product-tab-reviews" class="rating-reviews">( 6 reviews )</a>
            </div>
            <p class="product-short-desc"><?php echo $product_rewards['detail_reward']; ?></p>
            <!--
            <div class="product-form product-color">
                <label>Color:</label>
                <div class="product-variations">
                    <a class="color" data-src="<?php echo base_url(); ?>assets/images/demos/demo7/products/big1.jpg" href="#" style="background-color: #d99e76"></a>
                    <a class="color" data-src="<?php echo base_url(); ?>assets/images/demos/demo7/products/2.jpg" href="#" style="background-color: #267497"></a>
                    <a class="color" data-src="<?php echo base_url(); ?>assets/images/demos/demo7/products/3.jpg" href="#" style="background-color: #9a999d"></a>
                    <a class="color" data-src="<?php echo base_url(); ?>assets/images/demos/demo7/products/4.jpg" href="#" style="background-color: #2b2b2b"></a>
                </div>
            </div>
            <div class="product-form product-size">
                <label>Size:</label>
                <div class="product-form-group">
                    <div class="product-variations">
                        <a class="size" href="#">S</a>
                        <a class="size" href="#">M</a>
                        <a class="size" href="#">L</a>
                        <a class="size" href="#">XL</a>
                        <a class="size" href="#">2XL</a>
                    </div>
                    <a href="#" class="size-guide"><i class="d-icon-ruler"></i>Size Guide</a>
                    <a href="#" class="product-variation-clean">Clean All</a>
                </div>
            </div>
            
            <div class="product-variation-price">
                <span><?php echo gkInvestRupiah($product_rewards['minimum_deposit']); ?></span>
            </div>-->

            <hr class="product-divider">

            <div class="product-form product-qty">
                <!--<label>QTY:</label>-->
                <div class="product-form-group">
                    <!--<div class="input-group">
                        <button class="quantity-minus d-icon-minus"></button>
                        <input class="quantity form-control" type="number" min="1" max="1000000">
                        <button class="quantity-plus d-icon-plus"></button>
                    </div>-->
                    <?php if (empty($reward_active['id_user'])) : ?>
                        <form method="post" action="<?php echo base_url('reward_active/add'); ?>">
                            <input type="hidden" name="point_reward_active" value="0">
                            <input type="hidden" name="id_user" value="<?php echo $this->session->userdata('id'); ?>">
                            <input type="hidden" name="id_reward" value="<?php echo $product_rewards['id_reward']; ?>">
                            <input type="hidden" name="join_date" value="<?php echo date("Y-m-d H:i:s"); ?>">
                            <button class="btn-product btn-cart" type="submit"><i class="d-icon-bag"></i>Join Reward</button>
                        </form>
                    <?php else : ?>
                        <button class="btn-product btn-cart" type="button" style="background: #ccc;"><i class="d-icon-bag"></i>Join Reward</button>
                    <?php endif; ?>

                </div>
            </div>
            <!--
            <hr class="product-divider mb-3">

            <div class="product-footer">
                <div class="social-links mr-2">
                    <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
                    <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                    <a href="#" class="social-link social-vimeo fab fa-vimeo-v"></a>
                </div>
                <div class="product-action">
                    <a href="#" class="btn-product btn-wishlist"><i class="d-icon-heart"></i>Add To Wishlist</a>
                    <span class="divider"></span>
                    <a href="#" class="btn-product btn-compare"><i class="d-icon-random"></i>Add To Compare</a>
                </div>
            </div> -->
        </div>
    </div>
</div>