<section class="ecom-header-title-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="text-light">Daftar Barang</h1>
				<ul class="breadcumb">
					<li><a href="<?php echo base_url('/') ?>">Beranda</a></li>
					<li><span>Daftar Barang</span></li>
				</ul>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section><!-- /.ecom-header-title-area -->

<section>
	<div class="ecom-homepage container margin-top-60px">
	  <div class="row">
	    <div class="col-md-12 entry-title">
	      <h2 class="text-uppercase">Barang</h2>
	    </div><!-- /.col-md-12 -->

	    <div class="col-md-12 margin-bottom-60px">
	    	<div class="row">
			    <div class="col-lg-12">
			      <?php $this->load->view('notification') ?>
			    </div><!-- /.col-lg-12 -->
			  </div><!-- /.row -->

	    	<div class="row">
	    		<?php 
	    		// date_default_timezone_set('Asia/Jakarta');

	    		foreach ($barang as $row) : 
	    			foreach($stock as $s) :
	    				if($s['id_barang'] == $row['kode_barang']) :
	    		?>
	    		<div class="col-md-3 col-xs-6 product product-grid">
	    			<?php 
	    				if( $row['photo_barang'] != '' ) : 
	    					$arrayPhotoBarang = explode(',', $row['photo_barang']);
	    			?>
	          <a class="popup" href="<?php echo base_url('barang/detail/'.$row['kode_barang']) ?>"><img src="<?php echo base_url('upload/be/barang/'.$arrayPhotoBarang[0]) ?>" alt=""></a>
	         	<?php else : ?>
	         	<a class="popup" href="<?php echo base_url('barang/detail/'.$row['kode_barang']) ?>"><img src="https://dummyimage.com/265x199/f0f0f0/fcfcfc" alt=""></a>
	        	<?php endif; ?>
	          <button type="button" class="add2cart">
	          	<span class="mdi mdi-crosshairs"></span> <?php if(substr($row['kode_barang'], 0, 1) == 1) { echo "Mall"; } else { echo "Office"; } ?>
	          </button>

	          <?php if( $this->session->userdata('id') != null ): ?>

		          <?php if ( $request['status_manager_departemen'] == 'Pending' && $request['status_admin_ga'] == '' || $request['status_manager_departemen'] == 'Approved' && $request['status_admin_ga'] == '' || $request['status_manager_departemen'] == 'Approved' && $request['status_admin_ga'] == 'Being Processed' ) : ?>
		          <button type="button" class="eye"><i class="mdi mdi-close" title="Maaf permintaan barang Anda sebelumnya belum di Approved"></i></button>
		          <?php elseif ( $s['stock'] > 0 && date('H:i:s') < date('H:i:s', strtotime($row['waktu_limit_order'])) && $request['status_manager_departemen'] == 'Approved' && $request['status_admin_ga'] != 'Not Approved' ) : ?>
		          <form action="<?php echo base_url('request/add') ?>" method="post">
		      			<input 	type="hidden" class="form-control" name="kode_barang" value="<?php echo $row['kode_barang']; ?>">
							  <input 	type="hidden" class="form-control" name="nama_barang" value="<?php echo $row['nama_barang']; ?>">
		  					<input 	type="hidden" class="form_control" name="harga_barang" value="<?php echo $row['harga_barang']; ?>" />
							  <input 	type="hidden" class="form-control" name="qty" value="1">
		          	<button type="submit" class="eye"><i class="mdi mdi-cart"></i></button>
		          </form>
		          <?php echo $s['stock'] ?>
		        	<?php elseif( $s['stock'] <= 0 && date('H:i:s') < date('H:i:s', strtotime($row['waktu_limit_order'])) ): ?>
		        	<button type="button" class="eye"><i class="mdi mdi-close" title="Maaf sudah lewat waktunya atau barang ini kosong"></i></button>
		        	<?php endif ?>

		        <?php else: ?>
		        	<?php if ( $s['stock'] > 0 && date('H:i:s') < date('H:i:s', strtotime($row['waktu_limit_order'])) ) : ?>
		          <form action="<?php echo base_url('request/add') ?>" method="post">
		      			<input 	type="hidden" class="form-control" name="kode_barang" value="<?php echo $row['kode_barang']; ?>">
							  <input 	type="hidden" class="form-control" name="nama_barang" value="<?php echo $row['nama_barang']; ?>">
		  					<input 	type="hidden" class="form_control" name="harga_barang" value="<?php echo $row['harga_barang']; ?>" />
							  <input 	type="hidden" class="form-control" name="qty" value="1">
		          	<button type="submit" class="eye"><i class="mdi mdi-cart"></i></button>
		          </form>
		        	<?php elseif( $s['stock'] <= 0 && date('H:i:s') < date('H:i:s', strtotime($row['waktu_limit_order'])) ): ?>
		        	<button type="button" class="eye"><i class="mdi mdi-close" title="Maaf sudah lewat waktunya atau barang ini kosong"></i></button>
		        	<?php endif ?>
		        <?php endif; ?>

	          <h5 class="this-title"><a href="<?php echo base_url('barang/detail/'.$row['kode_barang']) ?>"><?php echo $row['nama_barang'] ?></a></h5>
	        </div><!-- /.col-md-3 col-xs-6 product product-grid -->
	        <?php 
	        		endif;
	      		endforeach;
	      	endforeach; 
	      	?>
	      </div><!-- /.row -->

	      <div class="row">
					<div class="col-lg-12 text-center">
					  <?php echo $this->pagination->create_links() ?>
					</div><!-- /.col-lg-12 -->
        </div><!-- /.row --> 
	    </div><!-- /.col-md-12 -->
	  </div><!-- /.row -->
	</div><!-- /.container -->
</section>