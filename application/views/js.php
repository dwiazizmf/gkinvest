 <!-- Plugins JS File -->
 <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/vendor/parallax/parallax.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/vendor/elevatezoom/jquery.elevatezoom.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

 <script src="<?php echo base_url(); ?>assets/vendor/owl-carousel/owl.carousel.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/vendor/isotope/isotope.pkgd.min.js"></script>

 <!-- Main JS File -->
 <script src="<?php echo base_url(); ?>assets/js/main.js"></script>