<main class="main">
  <div class="page-header" style="background-image: url(<?= base_url(); ?>assets/images/page-header.jpg)">
    <h1 class="page-title">Our Blog post</h1>
  </div>
  <nav class="breadcrumb-nav">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?= base_url(); ?>"><i class="d-icon-home"></i></a></li>
        <li><a href="#" class="active">Blog</a></li>
        <li>Single Post</li>
      </ul>
    </div>
  </nav>
  <div class="page-content mt-6">
    <div class="container">
      <div class="row gutter-lg">
        <div class="col-lg-9">
          <article class="post-single">
            <figure class="post-media">
              <a href="#">
                <img src="<?php echo base_url('upload/be/news/' . $news['post_image']); ?>" width="870" height="420" alt="post" />
              </a>
            </figure>
            <div class="post-details">
              <div class="post-meta">
                Posted by <a href="#" class="post-author">John Doe</a>
                /
                <a href="#" class="post-date">Nov 22, 2019</a>
                /
                <a href="#" class="post-comment">2 Comments</a>
              </div>
              <h4 class="post-title"><a href="#"><?= $news['post_title']; ?></a></h4>
              <div class="post-cats">
                in <a href="#"><?= $news['category_post']; ?></a>, <a href="#"><?= $news['tag_post']; ?></a>
              </div>
              <div class="post-body mb-7">
                <?= $news['post_content']; ?>
              </div>
              <!-- End Post Body -->
              <div class="post-footer d-flex justify-content-between align-items-center">
                <div class="tags mb-6">
                  <label class="mr-2">Tags:</label> <a href="#" class="tag">photography</a> <a href="#" class="tag">style</a>
                </div>
                <div class="social-links inline-links mb-6">
                  <label>Share this post:</label>
                  <a href="#" class="social-link social-facebook fab fa-facebook-f" style="color: #8f79ed"></a>
                  <a href="#" class="social-link social-twitter fab fa-twitter" style="color: #79c8ed"></a>
                  <a href="#" class="social-link fab fa-pinterest" style="color: #e66262"></a>
                </div>
              </div>
              <!-- End Post Footer -->
              <div class="post-author-detail">
                <figure class="author-media">
                  <a href="#">
                    <img src="<?= base_url(); ?>assets/images/blog/comments/1.jpg" alt="avatar" width="50" height="50">
                  </a>
                </figure>
                <div class="author-body">
                  <div class="author-header d-flex justify-content-between align-items-center">
                    <div>
                      <h6 class="author-name"><a href="#">Jimmy Pearson</a></h6>
                      <span>November 9, 2018 at 2:19 pm</span>
                    </div>
                    <div>
                      <a href="#" class="author-link">View all posts by John Doe <i class="d-icon-arrow-right"></i></a>
                    </div>
                  </div>
                  <div class="author-content">
                    <p class="mb-0">Praesent dapibus, neque id cursus faucibus, tortor neque
                      egestas auguae, eu vulputate magna eros euerat. Aliquam erat
                      volutpat.</p>
                  </div>
                </div>
              </div>
              <!-- End Author Detail -->
            </div>
          </article>
          <nav class="page-nav">
            <a class="pager-link pager-link-prev" href="#">
              Previous Post
              <span class="pager-link-title">Cras iaculis ultricies nulla</span>
            </a>
            <a class="pager-link pager-link-next" href="#">
              Next Post
              <span class="pager-link-title">Praesent placerat risus</span>
            </a>
          </nav>
          <!-- End Page Navigation -->
          <div class="related-posts mt-9 mb-9">
            <h3 class="title title-simple text-left text-normal">Related Posts</h3>
            <div class="owl-carousel owl-theme row cols-lg-3 cols-sm-2" data-owl-options="{
                                    'items': 1,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '576': {
                                            'items': 2
                                        },
                                        '992': {
                                            'items': 3
                                        }
                                    }
                                }">

              <div class="post">
                <figure class="post-media">
                  <a href="#">
                    <img src="<?= base_url(); ?>assets/images/blog/single/4.jpg" width="380" height="250" alt="post" />
                  </a>
                </figure>
                <div class="post-details">
                  <div class="post-meta">
                    <a href="#" class="post-date">Nov 22, 2019</a>
                    |
                    <a href="#" class="post-comment">2 Comments</a>
                  </div>
                  <h4 class="post-title"><a href="#">Quisque a lectus</a></h4>
                  <div class="post-cats">
                    in <a href="#">Fashion</a>
                  </div>
                </div>
              </div>
              <div class="post">
                <figure class="post-media">
                  <a href="#">
                    <img src="<?= base_url(); ?>assets/images/blog/single/5.jpg" width="380" height="250" alt="post" />
                  </a>
                </figure>
                <div class="post-details">
                  <div class="post-meta">
                    <a href="#" class="post-date">Nov 22, 2019</a>
                    |
                    <a href="#" class="post-comment">2 Comments</a>
                  </div>
                  <h4 class="post-title"><a href="#">Fusce pellentesque suscipit.</a></h4>
                  <div class="post-cats">
                    in <a href="#">Travel</a>
                  </div>
                </div>
              </div>
              <div class="post">
                <figure class="post-media">
                  <a href="#">
                    <img src="<?= base_url(); ?>assets/images/blog/single/6.jpg" width="380" height="250" alt="post" />
                  </a>
                </figure>
                <div class="post-details">
                  <div class="post-meta">
                    <a href="#" class="post-date">Nov 22, 2019</a>
                    |
                    <a href="#" class="post-comment">2 Comments</a>
                  </div>
                  <h4 class="post-title"><a href="#">Aenean dignissim pellente squefelis.</a>
                  </h4>
                  <div class="post-cats">
                    in <a href="#">Travel</a>, <a href="#">Hobbies</a>
                  </div>
                </div>
              </div>
              <div class="post">
                <figure class="post-media">
                  <a href="#">
                    <img src="<?= base_url(); ?>assets/images/blog/single/4.jpg" width="380" height="250" alt="post" />
                  </a>
                </figure>
                <div class="post-details">
                  <div class="post-meta">
                    <a href="#" class="post-date">Nov 22, 2019</a>
                    |
                    <a href="#" class="post-comment">2 Comments</a>
                  </div>
                  <h4 class="post-title"><a href="#">Quisque a lectus</a></h4>
                  <div class="post-cats">
                    in <a href="#">Lifestyle</a>, <a href="#">Shopping</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="comments mb-8">
            <h3 class="title title-simple text-left text-normal mb-6">3 Comments</h3>
            <ul>
              <li>
                <div class="comment">
                  <figure class="comment-media">
                    <a href="#">
                      <img src="<?= base_url(); ?>assets/images/blog/comments/1.jpg" alt="avatar">
                    </a>
                  </figure>
                  <div class="comment-body">
                    <a href="#" class="comment-reply">Reply</a>
                    <div class="comment-user">
                      <h4><a href="#">Jimmy Pearson</a></h4>
                      <span class="comment-date">November 9, 2018 at 2:19 pm</span>
                    </div>

                    <div class="comment-content">
                      <p>Sed pretium, ligula sollicitudin laoreet viverra, tortor libero
                        sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut
                        justo. Suspendisse potenti. </p>
                    </div>
                  </div>
                </div>
                <ul>
                  <li>
                    <div class="comment">
                      <figure class="comment-media">
                        <a href="#">
                          <img src="<?= base_url(); ?>assets/images/blog/comments/2.jpg" alt="avatar">
                        </a>
                      </figure>

                      <div class="comment-body">
                        <a href="#" class="comment-reply">Reply</a>
                        <div class="comment-user">
                          <h4><a href="#">Lena Knight</a></h4>
                          <span class="comment-date">November 9, 2018 at 2:19
                            pm</span>
                        </div>

                        <div class="comment-content">
                          <p>Morbi interdum mollis sapien. Sed ac risus.</p>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
              <li>
                <div class="comment">
                  <figure class="comment-media">
                    <a href="#">
                      <img src="<?= base_url(); ?>assets/images/blog/comments/3.jpg" alt="avatar">
                    </a>
                  </figure>

                  <div class="comment-body">
                    <a href="#" class="comment-reply">Reply</a>
                    <div class="comment-user">
                      <h4><a href="#">Johnathan Castillo</a></h4>
                      <span class="comment-date">November 9, 2018 at 2:19 pm</span>
                    </div>

                    <div class="comment-content">
                      <p>Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod
                        dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu,
                        dapibus eu, fermentum et, dapibus sed, urna.</p>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <!-- End Comments -->
          <div class="reply">
            <div class="title-wrapper text-left">
              <h3 class="title title-simple text-left text-normal">Leave A Reply</h3>
              <p>Your email address will not be published. Required fields are marked *</p>
            </div>
            <form action="#">
              <textarea id="reply-message" cols="30" rows="4" class="form-control mb-4" placeholder="Comment *" required></textarea>
              <div class="row">
                <div class="col-md-6 mb-5">
                  <input type="text" class="form-control" id="reply-name" name="reply-name" placeholder="Name *" required />
                </div>
                <div class="col-md-6 mb-5">
                  <input type="email" class="form-control" id="reply-email" name="reply-email" placeholder="Email *" required />
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-md">POST COMMENT<i class="d-icon-arrow-right"></i></button>
            </form>
          </div>
          <!-- End Reply -->
        </div>
        <aside class="col-lg-3 right-sidebar blog-sidebar sidebar-fixed sticky-sidebar-wrapper">
          <div class="sidebar-overlay">
            <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
          </div>
          <a href="#" class="sidebar-toggle"><i class="fas fa-chevron-left"></i></a>
          <div class="sidebar-content">
            <div class="sticky-sidebar" data-sticky-options="{'top': 89, 'bottom': 70}">
              <div class="widget widget-search mb-5">
                <form action="#" class="input-wrapper input-wrapper-inline btn-absolute">
                  <input type="text" class="form-control" name="search" autocomplete="off" placeholder="Search in blog" required />
                  <button class="btn btn-search btn-link" type="submit">
                    <i class="d-icon-search"></i>
                  </button>
                </form>
              </div>
              <div class="widget widget-categories">
                <h3 class="widget-title">Blog Categories</h3>
                <ul class="widget-body filter-items search-ul">
                  <li><a href="#">Travel</a></li>
                  <li><a href="#">Sport</a></li>
                  <li>
                    <a href="#" class="show">Shopping</a>
                    <ul style="display: block">
                      <li><a href="#">Summer Sale</a></li>
                      <li><a href="#">Winter Sale</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Fashion</a></li>
                  <li><a href="#">Lifestyle</a></li>
                  <li><a href="#">Uncategorized</a></li>
                </ul>
              </div>
              <div class="widget widget-posts mb-5">
                <h3 class="widget-title">Popular Posts</h3>
                <div class="widget-body">
                  <div class="post-col">
                    <div class="post post-list-sm">
                      <figure class="post-media">
                        <a href="post-single.html">
                          <img src="<?= base_url(); ?>assets/images/blog/1_xs.jpg" width="100" height="100" alt="post" />
                        </a>
                      </figure>
                      <div class="post-details">
                        <div class="post-meta">
                          <a href="#" class="post-date">Feb 11, 2020</a>
                        </div>
                        <h4 class="post-title"><a href="post-single.html">Lorem ipsum
                            dolor sim ectur adipiscing</a></h4>
                      </div>
                    </div>
                    <div class="post post-list-sm">
                      <figure class="post-media">
                        <a href="post-single.html">
                          <img src="<?= base_url(); ?>assets/images/blog/2_xs.jpg" width="100" height="100" alt="post" />
                        </a>
                      </figure>
                      <div class="post-details">
                        <div class="post-meta">
                          <a href="#" class="post-date">Jan 11, 2020</a>
                        </div>
                        <h4 class="post-title"><a href="post-single.html">Vel pretium
                            lectus qua id leo in vitae</a></h4>
                      </div>
                    </div>
                    <div class="post post-list-sm">
                      <figure class="post-media">
                        <a href="post-single.html">
                          <img src="<?= base_url(); ?>assets/images/blog/3_xs.jpg" width="100" height="100" alt="post" />
                        </a>
                      </figure>
                      <div class="post-details">
                        <div class="post-meta">
                          <a href="#" class="post-date">Feb 11, 2020</a>
                        </div>
                        <h4 class="post-title"><a href="post-single.html">Sagittis id
                            consectetur purus ut.</a></h4>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="widget widget-about mb-4">
                <h3 class="widget-title">About us</h3>
                <div class="widget-body">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                    nonummy nibh euismod tincidunt.</p>
                </div>
              </div>

              <div class="widget widget-posts">
                <h3 class="widget-title">Tag Cloud</h3>
                <div class="widget-body">
                  <a href="#" class="tag">Bag</a>
                  <a href="#" class="tag">Classic</a>
                  <a href="#" class="tag">Converse</a>
                  <a href="#" class="tag">Leather</a>
                  <a href="#" class="tag">Fit</a>
                  <a href="#" class="tag">Green</a>
                  <a href="#" class="tag">Diesel</a>
                  <a href="#" class="tag">Jack and Jones</a>
                  <a href="#" class="tag">Jeans</a>
                  <a href="#" class="tag">Jumper</a>
                  <a href="#" class="tag">Man</a>
                </div>
              </div>
            </div>
          </div>
        </aside>
      </div>
    </div>
  </div>
</main>