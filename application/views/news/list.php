<main class="main">
    <div class="page-header" style="background-image: url(<?= base_url(); ?>assets/images/page-header.jpg)">
        <h1 class="page-title">Our Blog post</h1>
    </div>
    <nav class="breadcrumb-nav">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="demo1.html"><i class="d-icon-home"></i></a></li>
                <li><a href="#" class="active">Blog</a></li>
                <li>Grid 3 Columns</li>
            </ul>
        </div>
    </nav>
    <div class="page-content mt-10 pt-2">
        <div class="container">
            <ul class="nav-filters filter-underline blog-filters justify-content-center mb-4" data-target=".posts">
                <li><a href="#" class="nav-filter active" data-filter="*">All Blog Post</a><span>12</span></li>
                <li><a href="#" class="nav-filter" data-filter=".lifestyle">Lifestyle</a><span>3</span></li>
                <li><a href="#" class="nav-filter" data-filter=".shopping">Shopping</a><span>3</span></li>
                <li><a href="#" class="nav-filter" data-filter=".fashion">Fashion</a><span>1</span></li>
                <li><a href="#" class="nav-filter" data-filter=".travel">Travel</a><span>3</span></li>
                <li><a href="#" class="nav-filter" data-filter=".hobbies">Hobbies</a><span>2</span></li>
            </ul>
            <div class="posts grid row" data-grid-options="{
                        'layoutMode': 'fitRows'
                    }">

                <?php foreach ($news as $row) { ?>
                    <div class="grid-item col-sm-6 col-lg-4 lifestyle shopping">
                        <article class="post text-center mb-5">
                            <figure class="post-media">
                                <a href="post-single.html">
                                    <img src="<?php echo base_url('upload/be/news/' . $row['post_image']); ?>" width="380" height="250" alt="post" />
                                </a>
                            </figure>
                            <div class="post-details">
                                <div class="post-meta">
                                    <a href="#" class="post-author">by John Doe</a>
                                    |
                                    <a href="#" class="post-date">Nov 22, 2019</a>
                                    |
                                    <a href="#" class="post-comment">2 Comments</a>
                                </div>
                                <h4 class="post-title"><a href="post-single.html"><?= $row['post_title']; ?></a>
                                </h4>
                                <div class="post-cats">
                                    in <a href="#"><?= $row['category_post']; ?></a>, <a href="#"><?= $row['tag_post']; ?></a>
                                </div>
                                <p class="post-content">Sed pretium, ligula sollicitudin laoreet viverra, tortor
                                    libero sodales leo, eget blandit nunc tortor eu nibh. Suspendisse potenti. Sed
                                    egestas, ante et vulputate volutpat, uctus metus libero eu augue.</p>
                                <a href="post-single.html" class="btn btn-link btn-underline btn-primary btn-reveal-right">Read more<i class="d-icon-arrow-right"></i></a>
                            </div>
                        </article>
                    </div>
                <?php } ?>

            </div>
            <ul class="pagination mb-10 pb-4 justify-content-center">
                <li class="page-item disabled">
                    <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1" aria-disabled="true">
                        <i class="d-icon-arrow-left"></i>Prev
                    </a>
                </li>
                <li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item">
                    <a class="page-link page-link-next" href="#" aria-label="Next">
                        Next<i class="d-icon-arrow-right"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</main>