<main class="main">
    <div class="page-content">
        <section class="intro-slider animation-slider owl-carousel owl-dot-inner owl-dot-white owl-theme row cols-1 gutter-no" data-owl-options="{
                    'items': 1,
                    'dots': true,
                    'nav': false,
                    'loop': true,
                    'autoplay': true
                }">
            <div class="intro-slide1 banner banner-fixed" style="background-image: url(assets/images/demos/demo13/slides/1.jpg); background-color: #212629">
                <!--<div class="container">
                    <div class="banner-content">
                        <h4 class="banner-subtitle mb-1 text-primary font-weight-normal slide-animate" data-animation-options="{'name': 'fadeInDownShorter', 'duration': '1.2s'}">Fashion
                            Trending 2020</h4>
                        <h3 class="banner-title text-uppercase text-white slide-animate" data-animation-options="{'name': 'fadeInDownShorter', 'duration': '1.2s', 'delay': '.2s'}">
                            LookBook</h3>
                        <p class="font-primary mb-7 text-white text-uppercase font-weight-bold slide-animate" data-animation-options="{'name': 'fadeInDownShorter', 'duration': '1.2s', 'delay': '.4s'}">
                            Men’s Collection</p>
                        <a href="#" class="btn btn-outline btn-white slide-animate" data-animation-options="{'name': 'fadeInDownShorter', 'duration': '1.2s', 'delay': '.6s'}">Shop
                            now</a>
                    </div>
                </div>-->
            </div>
            <div class="intro-slide2 banner banner-fixed" style="background-image: url(assets/images/demos/demo13/slides/2.jpg); background-color: #abacb1">
                <!--<div class="container">
                    <div class="banner-content y-50 text-center">
                        <h4 class="banner-subtitle d-inline-block mb-2 ls-l text-uppercase text-white bg-primary slide-animate" data-animation-options="{'name': 'fadeIn', 'duration': '1.2s'}">New Arrivals</h4>
                        <h3 class="banner-title mb-3 ls-m text-uppercase text-white slide-animate font-weight-bold" data-animation-options="{'name': 'fadeInLeftShorter', 'duration': '1.2s', 'delay': '.8s'}">
                            For Women’s</h3>
                        <p class="mb-7 ls-s font-primary text-white slide-animate" data-animation-options="{'name': 'fadeInRightShorter', 'duration': '1.2s', 'delay': '.8s'}">
                            Up to 50% Off this Season’s & Get free shipping<br />on all orders over $199.00</p>
                        <a href="#" class="btn btn-solid btn-primary slide-animate" data-animation-options="{'name': 'fadeInUp', 'duration': '1.2s', 'delay': '1.3s'}">Shop
                            now</a>
                    </div>
                </div>-->
            </div>
            <div class="intro-slide3 banner banner-fixed" style="background-image: url(assets/images/demos/demo13/slides/3.jpg); background-color: #423d3a">
                <!--<div class="container">
                    <div class="banner-content x-50 y-50 text-center">
                        <h4 class="banner-subtitle mb-5 text-primary font-tertiary font-weight-bold slide-animate" data-animation-options="{'name': 'fadeIn', 'duration': '1.2s'}">New Arrivals</h4>
                        <h3 class="banner-title text-white slide-animate" data-animation-options="{'name': 'blurIn', 'duration': '1.5s', 'delay': '.8s'}">
                            Starter Kits Sport<br />Essentials</h3>
                        <p class="mx-auto font-primary text-grey slide-animate" data-animation-options="{'name': 'fadeInUp', 'duration': '1.2s', 'delay': '1.1s'}">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore.</p>
                        <a href="#" class="btn btn-outline btn-primary text-white slide-animate" data-animation-options="{'name': 'fadeInUp', 'duration': '1.2s', 'delay': '1.3s'}">Shop
                            now</a>
                    </div>
                </div>-->
            </div>
        </section>
        <?php if ($this->session->userdata('is_login') == TRUE) : ?>
            <section class="blog container pt-3 mt-10 mb-4">
                <div class="row">
                    <div class="col-lg-4 appear-animate">
                        <h2 class="title title-simple text-left mb-4">Your Active Reward</h2>
                        <div class="post post-list overlay-light">
                            <figure class="post-media" style="width: 100%;">
                                <a href="post-single.html">
                                    <img src="<?php echo base_url('upload/be/product-rewards/') . $product_reward_active['photo_reward']; ?>" width="280" height="221" alt="post" />
                                </a>
                            </figure>

                        </div>
                    </div>
                    <div class="col-lg-8 mb-4 appear-animate">
                        <h2 class="title title-simple text-left mb-2"><?php echo $product_reward_active['name_reward']; ?><span class="divider"></span><?php echo gkInvestRupiah($product_reward_active['minimum_deposit']); ?>/
                            <?php
                            $datetime1 = new DateTime($product_reward_active['start_reward']);
                            $datetime2 = new DateTime($product_reward_active['end_reward']);
                            $interval = $datetime1->diff($datetime2);
                            $interval_format = $interval->format('%a');
                            echo $interval_format . " days";
                            ?><span class="divider"></span><a href="users/your_reward_active" class="btn btn-link btn-primary btn-underline btn-sm">View Detail<i class="d-icon-arrow-right"></i></a></h2>
                        <div class="post-details" style="padding: 0%;">
                            <p class="post-content text-grey">
                                <b class="pull-left">Profit</b>
                                <b class="pull-right"><?php echo gkInvestRupiah($point[0]['yourPoint']); ?> / <?php echo gkInvestRupiah($product_reward_active['minimum_deposit']); ?></b>
                            </p>
                            <div class="progress">
                                <?php
                                if ($point[0]['yourPoint'] > 0) :
                                    $progress = $point[0]['yourPoint'] / $product_reward_active['minimum_deposit'] * 100;
                                ?>
                                    <div class="progress-bar" role="progressbar" style="width: <?php echo $progress; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>

                                <?php
                                elseif ($point[0]['yourPoint'] == 0) :
                                ?>
                                    <div class="progress-bar" role="progressbar" style="width: 0" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>

                                <?php
                                endif;
                                ?>
                            </div>
                            <?php $datetime3 = new DateTime($product_reward_active['start_reward']);
                            $datetime4 = new DateTime("now");
                            $interval1 = $datetime3->diff($datetime4);
                            $interval_format1 = $interval1->format('%a'); ?>
                            <p class="post-content text-grey">
                                <b class="pull-left">Periode <?= ($interval_format1 > $interval_format) ? "<a style='color:red'>(EXPIRED)</a>" : ""; ?></b>
                                <b class="pull-right"><?php echo gkinvestPrettyDate($product_reward_active['start_reward']) . ' - ' . gkinvestPrettyDate($product_reward_active['end_reward']); ?></b>
                            </p>
                            <div class="progress">
                                <?php


                                $proggress_periode = $interval_format1 / $interval_format * 100;
                                if ($proggress_periode > 100) {
                                    echo '<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>';
                                } else {
                                    echo '<div class="progress-bar" role="progressbar" aria-valuenow="' . $proggress_periode . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $proggress_periode . '%;"></div>';
                                }
                                ?>

                            </div>

                            <h4 class="post-title"><a href="users/your_reward_active">Term and Condition.</a></h4>
                            <p class="post-content text-grey">
                                Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed do eiusmod temporncididunt ut labore et dolore magna aliqua.</p>
                            <a href="users/your_reward_active" class="btn btn-outline btn-sm btn-dark">Read More<i class="d-icon-arrow-right"></i></a>
                        </div>
                    </div>
            </section>
        <?php endif; ?>
        <section class="product-wrapper container pt-1 mt-10 mb-4 appear-animate">
            <div class="tab tab-nav-simple tab-nav-center">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#new-arrivals">Old event</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#best-sellers">Big Event</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#featured">Monthly Event</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="new-arrivals">
                        <div class="owl-carousel owl-theme row owl-nav-full cols-xl-5 cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                    'items': 5,
                                    'nav': true,
                                    'dots': false,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        },
                                        '1200': {
                                            'items': 5
                                        }
                                    }
                                }">
                            <?php foreach ($product_rewards_old as $row) : ?>
                                <div class="product">
                                    <figure class="product-media">
                                        <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>">
                                            <img src="<?php echo base_url('upload/be/product-rewards/') . $row['photo_reward']; ?>" alt="product" width="220" height="245">
                                        </a>
                                        <!--<div class="product-action-vertical">
                                            <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                                        </div>-->
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-quickview" id="<?php echo $row['id_reward']; ?>" title="Quick View">Quick
                                                View</a>
                                        </div>
                                        <!--<div class="product-label-group">
                                        <label class="product-label label-new">new</label>
                                    </div>-->
                                    </figure>
                                    <div class="product-details">
                                        <a href="#" class="btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                        <div class="product-cat">
                                            <a href="shop-grid-3col.html">categories</a>
                                        </div>
                                        <h3 class="product-name">
                                            <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>"><?php echo $row['name_reward']; ?></a>
                                        </h3>
                                        <div class="product-price">
                                            <span class="price"><?php echo gkInvestRupiah($row['minimum_deposit']); ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="tab-pane" id="best-sellers">
                        <div class="owl-carousel owl-theme row owl-nav-full cols-xl-5 cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                    'items': 5,
                                    'nav': true,
                                    'dots': false,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        },
                                        '1200': {
                                            'items': 5
                                        }
                                    }
                                }">
                            <?php foreach ($product_rewards_big as $row) : ?>
                                <div class="product">
                                    <figure class="product-media">
                                        <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>">
                                            <img src="<?php echo base_url('upload/be/product-rewards/') . $row['photo_reward']; ?>" alt="product" width="220" height="245">
                                        </a>
                                        <!--<div class="product-action-vertical">
                                            <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                                        </div>-->
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-quickview" id="<?php echo $row['id_reward']; ?>" title="Quick View">Quick
                                                View</a>
                                        </div>
                                        <!--<div class="product-label-group">
                                        <label class="product-label label-new">new</label>
                                    </div>-->
                                    </figure>
                                    <div class="product-details">
                                        <a href="#" class="btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                        <div class="product-cat">
                                            <a href="shop-grid-3col.html">categories</a>
                                        </div>
                                        <h3 class="product-name">
                                            <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>"><?php echo $row['name_reward']; ?></a>
                                        </h3>
                                        <div class="product-price">
                                            <span class="price"><?php echo gkInvestRupiah($row['minimum_deposit']); ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="tab-pane" id="featured">
                        <div class="owl-carousel owl-theme row owl-nav-full cols-xl-5 cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                                    'items': 5,
                                    'nav': true,
                                    'dots': false,
                                    'margin': 20,
                                    'loop': false,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 4
                                        },
                                        '1200': {
                                            'items': 5
                                        }
                                    }
                                }">
                            <?php foreach ($product_rewards_monthly as $row) : ?>
                                <div class="product">
                                    <figure class="product-media">
                                        <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>">
                                            <img src="<?php echo base_url('upload/be/product-rewards/') . $row['photo_reward']; ?>" alt="product" width="220" height="245">
                                        </a>
                                        <!--<div class="product-action-vertical">
                                            <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                                        </div>-->
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-quickview" id="<?php echo $row['id_reward']; ?>" title="Quick View">Quick
                                                View</a>
                                        </div>
                                        <!--<div class="product-label-group">
                                        <label class="product-label label-new">new</label>
                                    </div>-->
                                    </figure>
                                    <div class="product-details">
                                        <a href="#" class="btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                                        <div class="product-cat">
                                            <a href="shop-grid-3col.html">categories</a>
                                        </div>
                                        <h3 class="product-name">
                                            <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>"><?php echo $row['name_reward']; ?></a>
                                        </h3>
                                        <div class="product-price">
                                            <span class="price"><?php echo gkInvestRupiah($row['minimum_deposit']); ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="banner-group container mt-9">
            <div class="row grid">
                <div class="grid-item col-lg-3 col-sm-6 height-x1 appear-animate" data-animation-options="{
                            'name': 'fadeInRightShorter',
                            'delay': '.2s'
                        }">
                    <div class="banner1 banner banner-fixed overlay-zoom" style="background-color: #e4e4e4">
                        <a href="#">
                            <figure>
                                <img src="assets/images/demos/demo13/banners/1.jpg" alt="banner" width="280" height="230" />
                            </figure>
                        </a>
                        <div class="banner-content w-100 y-50 text-center">
                            <h3 class="banner-title mb-2 mt-2 text-uppercase font-weight-bold">Sporting goods
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="grid-item col-lg-5 col-sm-6 height-x1 appear-animate" data-animation-options="{
                            'name': 'fadeInDownShorter'
                        }">
                    <div class="banner2 banner banner-fixed overlay-zoom" style="background-color: #2b2d5e">
                        <figure>
                            <img src="assets/images/demos/demo13/banners/2.jpg" alt="banner" width="480" height="230" />
                        </figure>
                        <div class="banner-content w-100 y-50 text-left appear-animate">
                            <h4 class="banner-subtitle font-tertiary ls-s mb-1 text-white font-weight-normal">
                                Clearance</h4>
                            <h3 class="banner-title mb-0 text-white text-uppercase font-weight-bold">for fitness
                                classes</h3>
                            <p class="text-white">from $19.99</p>
                            <a href="#" class="btn btn-outline btn-primary text-white btn-sm">Shop now</a>
                        </div>
                    </div>
                </div>
                <div class="grid-item col-lg-4 col-sm-6 height-x2 appear-animate" data-animation-options="{
                            'name': 'fadeInLeftShorter'
                        }">
                    <div class="banner3 banner banner-fixed overlay-zoom" style="background-color: #5d5457">
                        <figure>
                            <img src="assets/images/demos/demo13/banners/3.jpg" alt="banner" width="280" height="231" />
                        </figure>
                        <div class="banner-content w-100 text-left appear-animate">
                            <h4 class="banner-subtitle font-tertiary ls-normal mb-1 text-white font-weight-normal">
                                On Sale</h4>
                            <h3 class="banner-title mb-0 text-white text-uppercase font-weight-bold">Transform
                                your life</h3>
                            <p class="text-white ls-s">from $39.00</p>
                            <a href="#" class="btn btn-outline btn-primary text-white">Discover now</a>
                        </div>
                    </div>
                </div>
                <div class="grid-item col-lg-5 col-sm-6 height-x1 appear-animate" data-animation-options="{
                            'name': 'fadeInRightShorter'
                        }">
                    <div class="banner4 banner banner-fixed overlay-zoom" style="background-color: #694932">
                        <figure>
                            <img src="assets/images/demos/demo13/banners/4.jpg" alt="banner" width="280" height="231" />
                        </figure>
                        <div class="banner-content w-100 y-50 text-left appear-animate">
                            <h4 class="banner-subtitle font-tertiary ls-s mb-1 text-white font-weight-normal">
                                Clearance</h4>
                            <h3 class="banner-title mb-0 text-white text-uppercase font-weight-bold">build your
                                body</h3>
                            <p class="text-white">from $19.99</p>
                            <a href="#" class="btn btn-outline btn-primary text-white btn-sm">Shop now</a>
                        </div>
                    </div>
                </div>
                <div class="grid-item col-lg-3 col-sm-6 height-x1 appear-animate" data-animation-options="{
                            'name': 'fadeInUpShorter'
                        }">
                    <div class="banner5 banner banner-fixed overlay-zoom bg-primary">
                        <div class="banner-content w-100 y-50 text-center appear-animate">
                            <h4 class="banner-subtitle font-tertiary ls-normal mb-1 text-white font-weight-bold">
                                Flash Sale</h4>
                            <h3 class="banner-title mb-3 text-white text-uppercase font-weight-bold">30% OFF
                            </h3>
                            <a href="#" class="btn btn-outline btn-white btn-sm">Shop all sale</a>
                        </div>
                    </div>
                </div>
                <div class="grid-space col-1"></div>
            </div>
        </section>
        <section class="product-wrapper container mt-10 mb-4 appear-animate">
            <h2 class="title title-simple text-left mb-4">New Reward<span class="divider"></span><a href="<?php echo base_url('product_rewards'); ?>" class="btn btn-link btn-primary btn-sm btn-underline">View more<i class="d-icon-arrow-right"></i></a></h2>

            <div class="owl-carousel owl-theme row owl-nav-full cols-xl-5 cols-lg-4 cols-md-3 cols-2" data-owl-options="{
                        'items': 5,
                        'nav': true,
                        'dots': false,
                        'margin': 20,
                        'loop': false,
                        'responsive': {
                            '0': {
                                'items': 2
                            },
                            '768': {
                                'items': 3
                            },
                            '992': {
                                'items': 4
                            },
                            '1200': {
                                'items': 5
                            }
                        }
                    }">
                <?php foreach ($product_rewards as $row) : ?>
                    <div class="product">
                        <figure class="product-media">
                            <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>">
                                <img src="<?php echo base_url('upload/be/product-rewards/') . $row['photo_reward']; ?>" alt="product" width="220" height="245">
                            </a>
                            <!--<div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                            </div>-->
                            <div class="product-action">
                                <a href="#" class="btn-product btn-quickview" id="<?php echo $row['id_reward']; ?>" title="Quick View">Quick View</a>
                            </div>
                            <div class="product-label-group">
                                <label class="product-label label-new">new</label>
                            </div>
                        </figure>
                        <div class="product-details">
                            <a href="#" class="btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                            <div class="product-cat">
                                <a href="shop-grid-3col.html">categories</a>
                            </div>
                            <h3 class="product-name">
                                <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>"><?php echo $row['name_reward']; ?></a>
                            </h3>
                            <div class="product-price">
                                <span class="price"><?php echo $row['minimum_deposit']; ?></span>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </section>
        <section class="banner banner-cta parallax mt-10" style="background-color: #5d4136" data-image-src="assets/images/demos/demo13/parallax.jpg">
            <div class="container">
                <div class="banner-content d-inline-block">
                    <h4 class="banner-subtitle mb-1 font-secondary ls-l text-white font-weight-normal appear-animate" data-animation-options="{
                                'name': 'fadeInLeftShorter',
                                'delay': '.1s'
                            }">Flash Sale 50% Off</h4>
                    <h3 class="banner-title mb-5 ls-m text-white appear-animate" data-animation-options="{
                                'name': 'fadeInLeftShorter',
                                'delay': '.3s'
                            }">Necessaries For Summer Season</h3>
                    <!--<a href="#" class="btn btn-outline btn-primary text-white appear-animate" data-animation-options="{
                                'name': 'fadeInLeftShorter',
                                'delay': '.2s'
                            }">Shop now</a>-->
                </div>
            </div>
        </section>
        <section class="blog container pt-3 mt-10 mb-10">
            <div class="row">
                <div class="col-lg-6 appear-animate">
                    <h2 class="title title-simple text-left mb-4">Who we are?</h2>
                    <div class="post post-list overlay-light">
                        <figure class="post-media">
                            <a href="post-single.html">
                                <img src="assets/images/demos/demo13/blog/1.jpg" width="280" height="221" alt="post" />
                            </a>
                        </figure>
                        <div class="post-details">
                            <h4 class="post-title"><a href="post-single.html">We are a Patient Experienced team
                                    with big Ambitions.</a></h4>
                            <p class="post-content text-grey">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed do eiusmod temporncididunt ut labore et dolore magna aliqua.</p>
                            <a href="post-single.html" class="btn btn-outline btn-sm btn-dark">Read More<i class="d-icon-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4 appear-animate">
                    <h2 class="title title-simple text-left mb-4">Latest News<span class="divider"></span><a href="#" class="btn btn-link btn-primary btn-underline btn-sm">View all<i class="d-icon-arrow-right"></i></a></h2>

                    <div class="owl-carousel owl-theme row owl-nav-full cols-sm-2 cols-1" data-owl-options="{
                                'margin': 20,
                                'loop': false,
                                'nav': true,   
                                'dots': false,
                                'responsive': {
                                    '0': {
                                        'items': 1
                                    },
                                    '576': {
                                        'items': 2
                                    }
                                }
                            }">
                        <?php foreach ($news as $row) : ?>
                            <div class="post post-mask overlay-zoom appear-animate" data-animation-options="{
                                    'name': 'fadeInRightShorter',
                                    'delay': '.2s'
                                }">
                                <figure class="post-media">
                                    <a href="<?php echo gkInvestPostUrl($row); ?>">
                                        <?php if ($row['post_image'] == '') : ?>
                                            <img src="https://dummyimage.com/600x400/000/cccccc.jpg" width="280" height="221" alt="post" />
                                        <?php else : ?>
                                            <img src="<?php echo base_url('upload/be/news/' . $row['post_image']); ?>" width="280" height="221" alt="post" />
                                        <?php endif; ?>
                                    </a>
                                </figure>
                                <div class="post-details">
                                    <div class="post-meta">
                                        <a href="#" class="post-author">by Casper Dalin</a> - <a href="#" class="post-date">20. 01. 2019</a>
                                    </div>
                                    <h4 class="post-title"><a href="<?php echo gkInvestPostUrl($row); ?>"> <?php echo $row['post_title']; ?></a>
                                    </h4>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="container appear-animate" data-animation-options="{
                    'delay': '.3s'
                }">
            <h2 class="title title-simple d-none">Our Clients</h2>
            <div class="owl-carousel owl-theme row brand-carousel cols-xl-6 cols-lg-5 cols-md-4 cols-sm-3 cols-2 gutter-no" data-owl-options="{
                        'items': 7,
                        'nav': false,
                        'dots': false,
                        'autoplay': true,
                        'margin': 30,
                        'loop': true,
                        'responsive': {
                            '0': {
                                'items': 2
                            },
                            '576': {
                                'items': 3
                            },
                            '768': {
                                'items': 4
                            },
                            '992': {
                                'items': 5
                            },
                            '1200': {
                                'items': 6
                            }
                        }
                    }">
                <img src="assets/images/brands/1.png" alt="brand" width="180" height="100" />
                <img src="assets/images/brands/2.png" alt="brand" width="180" height="100" />
                <img src="assets/images/brands/3.png" alt="brand" width="180" height="100" />
                <img src="assets/images/brands/4.png" alt="brand" width="180" height="100" />
                <img src="assets/images/brands/5.png" alt="brand" width="180" height="100" />
                <img src="assets/images/brands/6.png" alt="brand" width="180" height="100" />
            </div>
        </section>
    </div>
</main>