<main class="main">
  <div class="page-header bg-dark" style="background-image: url('assets/images/demos/demo13/page-header.jpg'); background-color: #3C63A4;">
    <h1 class="page-title">Categories</h1>
    <h4 class="page-subtitle font-weight-normal">26 Products</h4>
  </div>
  <!-- End PageHeader -->
  <div class="page-content mb-10">
    <div class="container-fluid">
      <div class="toolbox-wrap">
        <aside class="sidebar sidebar-fixed shop-sidebar closed">
          <div class="sidebar-overlay">
            <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
          </div>
          <div class="sidebar-content">
            <div class="filter-actions">
              <a href="#" class="sidebar-toggle-btn toggle-remain btn btn-sm btn-outline btn-primary">Filters<i class="d-icon-arrow-left"></i></a>
              <a href="#" class="filter-clean text-primary">Clean All</a>
            </div>
            <!-- <a href="#" class="filter-clean text-primary">Clean All</a> -->
            <div class="row cols-lg-4">
              <div class="widget">
                <h3 class="widget-title">Size</h3>
                <ul class="widget-body filter-items">
                  <li><a href="#">Small<span>(2)</span></a></li>
                  <li><a href="#">Medium<span>(1)</span></a></li>
                  <li><a href="#">Large<span>(9)</span></a></li>
                  <li><a href="#">Extra Large<span>(1)</span></a></li>
                </ul>
              </div>
              <div class="widget">
                <h3 class="widget-title">Color</h3>
                <ul class="widget-body filter-items">
                  <li><a href="#">Black<span>(2)</span></a></li>
                  <li><a href="#">Blue<span>(1)</span></a></li>
                  <li><a href="#">Green<span>(9)</span></a></li>
                  <li><a href="#">Gray<span>(9)</span></a></li>
                </ul>
              </div>
              <div class="widget">
                <h3 class="widget-title">Price</h3>
                <ul class="widget-body filter-items">
                  <li><a href="#">All<span>(10)</span></a></li>
                  <li><a href="#">$0.00 - $100.00<span>(1)</span></a></li>
                  <li><a href="#">$100.00 - $200.00<span>(9)</span></a></li>
                  <li><a href="#">$200.00+<span>(3)</span></a></li>
                </ul>
              </div>
              <div class="widget">
                <h3 class="widget-title">Tags</h3>
                <div class="widget-body pt-2">
                  <a href="#" class="tag">Bag</a>
                  <a href="#" class="tag">Classic</a>
                  <a href="#" class="tag">Converse</a>
                  <a href="#" class="tag">Fit</a>
                  <a href="#" class="tag">Green</a>
                  <a href="#" class="tag">Jack and Jones</a>
                  <a href="#" class="tag">Jeans</a>
                  <a href="#" class="tag">Jumper</a>
                  <a href="#" class="tag">Leather</a>
                  <a href="#" class="tag">Diesel</a>
                  <a href="#" class="tag">Man</a>
                </div>
              </div>
            </div>
          </div>
        </aside>
        <div class="toolbox sticky-toolbox sticky-content fix-top">
          <div class="toolbox-left">
            <a href="#" class="toolbox-item left-sidebar-toggle btn btn-outline btn-primary btn-icon-left font-primary"><i class="d-icon-filter-2"></i>Filters</a>
            <p class="toolbox-item show-info mr-sm-auto">Showing <span> 12 of 56 </span> Products
            </p>
          </div>
          <div class="toolbox-right">
            <div class="toolbox-item toolbox-sort select-box">
              <label>Sort By :</label>
              <select name="orderby" class="form-control">
                <option value="default">Default</option>
                <option value="popularity" selected="selected">Most Popular</option>
                <option value="rating">Average rating</option>
                <option value="date">Latest</option>
                <option value="price-low">Sort forward price low</option>
                <option value="price-high">Sort forward price high</option>
                <option value="">Clear custom sort</option>
              </select>
            </div>
            <div class="toolbox-item toolbox-layout">
              <a href="shop-list.html" class="d-icon-mode-list btn-layout"></a>
              <a href="shop.html" class="d-icon-mode-grid btn-layout active"></a>
            </div>
          </div>
        </div>
      </div>
      <div class="row cols-2 cols-sm-3 cols-md-4 cols-xl-6 product-wrapper">
        <?php foreach ($product_rewards as $row) : ?>
          <div class="product-wrap">
            <div class="product">
              <figure class="product-media">
                <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>">
                  <img src="<?php echo base_url('upload/be/product-rewards/') . $row['photo_reward']; ?>" alt="product" width="220" height="245">
                </a>
                <div class="product-action-vertical">
                  <a href="#" class="btn-product-icon btn-cart" data-toggle="modal" data-target="#addCartModal" title="Add to cart"><i class="d-icon-bag"></i></a>
                </div>
                <div class="product-action">
                  <a href="#" class="btn-product btn-quickview" id="<?php echo $row['id_reward']; ?>" title="Quick View">Quick
                    View</a>
                </div>
              </figure>
              <div class="product-details">
                <a href="#" class="btn-wishlist" title="Add to wishlist"><i class="d-icon-heart"></i></a>
                <div class="product-cat">
                  <a href="shop-grid-3col.html">categories</a>
                </div>
                <h3 class="product-name">
                  <a href="<?php echo base_url('product_rewards/detail/' . $row['id_reward']); ?>"><?php echo $row['name_reward']; ?></a>
                </h3>
                <div class="product-price">
                  <span class="price"><?php echo gkInvestRupiah($row['minimum_deposit']); ?></span>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      <nav class="toolbox toolbox-pagination justify-content-center">
        <ul class="pagination">
          <li class="page-item disabled">
            <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1" aria-disabled="true">
              <i class="d-icon-arrow-left"></i>Prev
            </a>
          </li>
          <li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item page-item-dots"><a class="page-link" href="#">6</a></li>
          <li class="page-item">
            <a class="page-link page-link-next" href="#" aria-label="Next">
              Next<i class="d-icon-arrow-right"></i>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</main>