<?php 
//rubah
if(empty($product_rewards_point)): 
//end rubah
?>
<div class="login-wrapper d-flex align-items-center justify-content-center text-center">               
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5">
        <div class="px-4">
          <!-- Success Check-->
          <div class="success-check"><i class="lni lni-emoji-smile"></i></div>
          <!-- Reset Password Message-->
          <p class="text-white mt-4 mb-4">Data Not Found</p>
          <!-- Go Back Button--><a class="btn btn-warning" href="<?php echo base_url('/') ?>">Go Home</a>
        </div><!-- /.px-4 -->
      </div><!-- /.col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5 -->
    </div><!-- /.row justify-content-center -->
  </div><!-- /.container -->
</div><!-- /.login-wrapper d-flex align-items-center justify-content-center text-center -->
<?php else: ?>
<div class="page-content-wrapper">
  <!-- Top Products-->
  <div class="top-products-area py-3">
    <div class="container">
      <div class="section-heading d-flex align-items-center justify-content-between">
        <h6 class="ml-1">All Rewards</h6>
      </div><!-- /.section-heading d-flex align-items-center justify-content-between -->
      <div class="row g-3">
        <!-- Single Top Product Card-->
        <?php //rubah 
          foreach($product_rewards_point as $row): 
          //end rubhah ?>
        <div class="col-6 col-md-4 col-lg-3">
          <div class="card top-product-card">
            <div class="card-body">
              <!-- ubah -->
              <a class="product-thumbnail d-block" href="<?php echo base_url('product_rewards/detail/'.$row['id_reward'].'/point'); ?>">
                <img class="mb-2" src="<?php echo base_url('upload/be/product-rewards/').$row['photo_reward']; ?>" alt=""></a>
              <a class="product-title d-block" href="<?php echo base_url('product_rewards/detail/'.$row['id_reward'].'/point'); ?>"><?php echo $row['name_reward']; ?></a>
              <!-- end ubah -->
              <p class="sale-price"><?php echo gkInvestRupiah($row['minimum_deposit']); ?></p>
              
              <?php 
              /*
              <div class="product-rating">
                <i class="lni lni-star-filled"></i>
                <i class="lni lni-star-filled"></i>
                <i class="lni lni-star-filled"></i>
                <i class="lni lni-star-filled"></i>
                <i class="lni lni-star-filled"></i>
              </div><!-- /.product-rating -->
              */
              ?>

              <a class="btn btn-success btn-sm add2cart-notify" href="<?php echo base_url('product_rewards/detail/'.$row['id_reward']); ?>/point"><i class="lni lni-plus"></i></a>
            </div><!-- /.card-body -->
          </div><!-- /.card top-product-card -->
        </div><!-- /.col-6 col-md-4 col-lg-3 -->
        <?php endforeach; ?>
      </div><!-- /.row g-3 -->

      <div class="row">
        <div class="col-lg-12 text-center mt-2">
          <?php echo $pagination; ?>
        </div><!-- /.col-lg-12 -->
      </div><!-- /.row g-3 --> 
    </div><!-- /.container -->
  </div><!-- /.top-products-area py-3 -->
</div><!-- /.page-content-wrapper -->
<?php endif; ?>