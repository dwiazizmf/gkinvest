<!DOCTYPE html>
<html lang="en">

<head>
    <?php echo $header; ?>
</head>

<body class="home">
    <div class="loading-overlay">
        <div class="bounce-loader">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
            <div class="bounce4"></div>
        </div>
    </div>
    <div class="page-wrapper">
        <h1 class="d-none">Donald - Responsive eCommerce HTML Template</h1>
        <header class="header header-transparent">
            <div class="header-middle sticky-header fix-top sticky-content">
                <div class="container-fluid">
                    <?php echo $menu; ?>
                </div>

            </div>
        </header>
        <!-- End Header -->
        <main class="main">
            <?php echo $content; ?>
        </main>
        <!-- End Main -->
        <!-- Footer -->
        <?php echo $footer; ?>
        <!-- End Footer -->
    </div>

    <!-- mobile -->
    <?php echo $mobile_view; ?>
    <!-- End of mobile -->
    <!-- javascript -->
    <?php echo $js; ?>
    <!-- end of javascript -->
</body>

</html>