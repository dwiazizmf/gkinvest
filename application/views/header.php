<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

<title>GK-Invest</title>

<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Donald - Bootstrap eCommerce Template">
<meta name="author" content="D-THEMES">

<!-- Favicon -->
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/favicon.ico">

<script>
    WebFontConfig = {
        google: {
            families: ['Open+Sans:400,600,700', 'Poppins:400,600,700']
        }
    };
    (function(d) {
        var wf = d.createElement('script'),
            s = d.scripts[0];
        wf.src = '<?php echo base_url(); ?>assets/js/webfont.js';
        wf.async = true;
        s.parentNode.insertBefore(wf, s);
    })(document);
</script>

<?php if (isset($home)) {
    echo '<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">';
} ?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/animate/animate.min.css">

<!-- Plugins CSS File -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/magnific-popup/magnific-popup.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/owl-carousel/owl.carousel.min.css">

<!-- Main CSS File -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/demo13.min.css">