<section class="ecom-header-title-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="text-light">Riwayat Pemesanan Barang</h1>
				<ul class="breadcumb">
					<li><a href="<?php echo base_url('/') ?>">Beranda</a></li>
					<li><span>Riwayat Pemesanan Barang</span></li>
				</ul>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section><!-- /.ecom-header-title-area -->

<section>
	<div class="ecom-homepage container margin-top-60px">
	  <div class="row">
	    <div class="col-md-12 entry-title">
	      <h2 class="text-uppercase">Detail Riwayat Pemesanan</h2>
	    </div><!-- /.col-md-12 -->

	    <div class="col-md-12 margin-bottom-60px">
    		<table class="table">
    			<thead>
    				<tr>
    					<th>Nama Barang</th>
    					<th>Merk</th>
              <th>Jumlah</th>
    				</tr>
    			</thead>

    			<?php $i = 1; ?>

    			<tbody>
    				<?php foreach($list as $item) : ?>
    				<tr>
    					<td>
    						<a href="<?php echo base_url('barang/detail/'.$item['kode_barang']) ?>"><?php echo $item['nama_barang'] ?></a>
    					</td>
    					<td><?php echo $item['nama_merk'] ?></td>
              <td><?php echo $item['quantity_permintaan_barang'] ?></td>
    				</tr>
    				<?php endforeach; ?>
    			</tbody>
    		</table><!-- /.table -->
	    </div><!-- /.col-md-12 -->
	  </div><!-- /.row -->
	</div><!-- /.container -->
</section>