<section class="ecom-header-title-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="text-light">Keranjang Permintaan Barang</h1>
				<ul class="breadcumb">
					<li><a href="<?php echo base_url('/') ?>">Beranda</a></li>
					<li><span>Keranjang Permintaan Barang</span></li>
				</ul>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section><!-- /.ecom-header-title-area -->

<section>
	<div class="ecom-homepage container margin-top-60px">
	  <div class="row">
	    <div class="col-md-12 entry-title">
	      <h2 class="text-uppercase">Daftar Keranjang Permintaan Barang</h2>
	    </div><!-- /.col-md-12 -->

	    <div class="col-md-12 margin-bottom-60px">
    		<table class="table">
    			<thead>
    				<tr>
    					<th>Kode Permintaan</th>
    					<th>NIK</th>
    					<th>Status Permintaan Barang</th>
              <th>Tanggal Pemesanan</th>
    				</tr>
    			</thead>

    			<?php $i = 1; ?>

    			<tbody>
    				<?php foreach($list as $item) : ?>
    				<tr>
    					<td>
                <a href="<?php echo base_url('request/detail/'.$item['id_permintaan_barang']) ?>"><?php echo $item['id_permintaan_barang'] ?></a>
              </td>
    					<td><?php echo $item['nik'] ?></td>
    					<td>
                <?php if($item['status_manager_departemen'] == 'Pending' && $item['status_permintaan_barang'] == '') :  ?>
                <small class="text-uppercase text-warning">
                  <strong>Menunggu Konfirmasi Dept. Head</strong>
                </small>
                -
                <small class="text-uppercase text-warning">
                  <strong>Menunggu Konfirmasi Admin GA</strong>
                </small>

                <!-- Status Manager Approved and Status Request Barang Null -->
    						<?php elseif($item['status_manager_departemen'] == 'Approved' && $item['status_permintaan_barang'] == '') :  ?>
    						<small class="text-uppercase text-primary">
    							<strong>Permintaan Disetujui Manager</strong> 
    						</small>
                -
                <small class="text-uppercase text-danger">
                  <strong>Menunggu Konfirmasi Admin GA</strong>
                </small>

                <!-- Status Manager Not Approved and Status Request Barang Null -->
    						<?php elseif($item['status_manager_departemen'] == 'Not Approved' && $item['status_permintaan_barang'] == '') : ?>
    						<small class="text-uppercase text-danger">
    							<strong>Permintaan Ditolak Manager</strong>
    						</small>
                -
                <small class="text-uppercase text-warning">
                  <strong>Permintaan Dibatalkan</strong>
                </small>

                <!-- Status Manager Approved and Status Request Barang Approved -->
                <?php elseif($item['status_manager_departemen'] == 'Approved' && $item['status_permintaan_barang'] == 'Approved') : ?>
                <small class="text-uppercase text-primary">
                  <strong>Permintaan Disetujui Manager</strong>
                </small>
                -
                <small class="text-uppercase text-primary">
                  <strong>Permintaan Disetujui Admin GA</strong>
                </small>

                <!-- Status Manager Approved and Status Request Barang Being Processed -->
                <?php elseif($item['status_manager_departemen'] == 'Approved' && $item['status_permintaan_barang'] == 'Being Processed') : ?>
                <small class="text-uppercase text-primary">
                  <strong>Permintaan Disetujui Manager</strong>
                </small>
                -
                <small class="text-uppercase text-warning">
                  <strong>Permintaan Sedang Diproses Admin GA</strong>
                </small>

                <!-- Status Manager Approved and Status Request Barang Not Approved -->
                <?php elseif($item['status_manager_departemen'] == 'Approved' && $item['status_permintaan_barang'] == 'Not Approved') : ?>
                <small class="text-uppercase text-primary">
                  <strong>Permintaan Disetujui Manager</strong>
                </small>
                -
                <small class="text-uppercase text-danger">
                  <strong>Permintaan Ditolak Admin GA</strong>
                </small>
    						<?php endif; ?>
    					</td>
              <td><?php echo ecomPrettyDate( $item['tanggal'] ) ?></td>
    				</tr>
    				<?php endforeach; ?>
    			</tbody>
    		</table><!-- /.table -->
	    </div><!-- /.col-md-12 -->
	  </div><!-- /.row -->
	</div><!-- /.container -->
</section>