<section class="ecom-header-title-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="text-light">Keranjang Permintaan Barang</h1>
				<ul class="breadcumb">
					<li><a href="<?php echo base_url('/') ?>">Beranda</a></li>
					<li><span>Keranjang Permintaan Barang</span></li>
				</ul>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section><!-- /.ecom-header-title-area -->

<section>
	<div class="ecom-homepage container margin-top-60px">
	  <div class="row">
	    <div class="col-md-12 entry-title">
	      <h2 class="text-uppercase">Daftar Keranjang Permintaan Barang</h2>
	    </div><!-- /.col-md-12 -->

	    <div class="col-md-12 margin-bottom-60px">
	    	<?php // echo ecomUUID(); ?>

	    	<?php if ($cart = $this->cart->contents()) : ?>

	    	<form action="<?php echo base_url('request/put') ?>" method="post" enctype="multipart/form-data">
	    		<?php $cart = $this->cart->contents(); ?>

	    		<table class="table">
	    			<thead>
	    				<tr>
	    					<th>No</th>
	    					<th>Kode Permintaan</th>
	    					<th>Item Barang</th>
	    					<th>Jumlah</th>
	    					<th>Hapus</th>
	    				</tr>
	    			</thead>

	    			<?php $i = 1; ?>

	    			<tbody>
	    				<?php foreach($cart as $item) : ?>
	    				<input type="hidden" name="cart[<?php echo $item['id']; ?>][id]" value="<?php echo $item['id']; ?>">
							<input type="hidden" name="cart[<?php echo $item['id']; ?>][rowid]" value="<?php echo $item['rowid']; ?>">
							<input type="hidden" name="cart[<?php echo $item['id']; ?>][name]" value="<?php echo $item['name']; ?>">
							<input type="hidden" name="cart[<?php echo $item['id']; ?>][price]" value="<?php echo $item['price']; ?>">
							<input type="hidden" name="cart[<?php echo $item['id']; ?>][qty]" value="<?php echo $item['qty']; ?>">

	    				<tr>
	    					<td><?php echo $i++; ?></td>
	    					<td><?php echo $item['id'] ?></td>
	    					<td><?php echo $item['name'] ?></td>
	    					<td>
	    						<input 	type="number" 
	    										class="form-control input-sm" 
	    										name="cart[<?php echo $item['id'] ?>][qty]" 
	    										value="<?php echo $item['qty'] ?>"
	    										min="1">
	    					</td>
	    					<td><a href="<?php echo base_url('request/delete/'.$item['rowid']) ?>" class="btn btn-sm btn-danger"><i class="mdi mdi-minus-circle"></i></a></td>
	    				</tr>
	    				<?php endforeach; ?>
	    				<tr>
	    					<td colspan="5" class="text-right">
	    						<button class="btn btn-success btn-sm" type="submit"><i class="mdi mdi-plus margin-right-10px"></i> Perbarui Quantity Barang</button>
	    						<a href="<?php echo base_url('request/checkout') ?>" class="btn btn-sm btn-primary">Checkout</a>
	    					</td>
	    				</tr>
	    			</tbody>
	    		</table><!-- /.table -->
	    	</form>

	    	<?php else : ?>
	    	<h5 class="text-center">Keranjang permintaan barang Anda masih kosong.</h5>
	    	<?php endif; ?>
	    </div><!-- /.col-md-12 -->
	  </div><!-- /.row -->
	</div><!-- /.container -->
</section>