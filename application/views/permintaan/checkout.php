<section class="ecom-header-title-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="text-light">Checkout</h1>
				<ul class="breadcumb">
					<li><a href="<?php echo base_url('/') ?>">Beranda</a></li>
					<li><span>Checkout</span></li>
				</ul>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section><!-- /.ecom-header-title-area -->

<section>
	<div class="ecom-homepage container margin-top-60px">
	  <div class="row">
	  	<div class="col-md-12 entry-title">
	      <h2 class="text-uppercase">Checkout</h2>
	    </div><!-- /.col-md-12 -->

	    <div class="col-lg-12 padded-bottom-60px">
	    	<?php date_default_timezone_set("Asia/Bangkok") ?>
	    	
	    	<form action="<?php echo base_url('request/postOrder') ?>" method="post">
	    		<div class="form-group">
	    			<input 	type="hidden" 
	    							class="form-control" 
	    							name="createdAt"
	    							value="<?php echo date("Y-m-d H:i:s") ?>">
	    		</div><!-- /.form-group -->

	    		<div class="form-group">
	    			<label>NIK</label>
	    			<input 	type="text" 
	    							class="form-control" 
	    							name="nik"
	    							value="<?php echo $this->session->userdata('id') ?>" 
	    							readonly="readonly">
	    		</div><!-- /.form-group -->

	    		<div class="form-group">
	    			<label>Nama</label>
	    			<input 	type="text" 
	    							class="form-control" 
	    							name="fullname"
	    							value="<?php echo $this->session->userdata('fullname') ?>" 
	    							readonly="readonly">
	    		</div><!-- /.form-group -->

	    		<div class="form-group">
	    			<label>Email</label>
	    			<input 	type="email" 
	    							class="form-control" 
	    							value="<?php echo $this->session->userdata('email') ?>" 
	    							readonly="readonly">
	    		</div><!-- /.form-group -->

	    		<div class="form-group text-right">
	    			<button class="btn btn-sm btn-primary text-uppercase" type="submit">
	    				<i class="mdi mdi-cart-plus margin-right-10px"></i> Checkout
	    			</button>
	    		</div><!-- /.form-group -->
	    	</form>
	    </div><!-- /.col-lg-12 -->
	  </div><!-- /.row -->
	</div><!-- /.container -->
</section>