<section class="ecom-header-title-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="text-light">Dokumentasi</h1>
				<ul class="breadcumb">
					<li><a href="<?php echo base_url('/') ?>">Home</a></li>
					<li><span>Dokumentasi</span></li>
				</ul>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section><!-- /.ecom-header-title-area -->

<section class="margin-bottom-60px">
	<div class="container ecom-panduan margin-top-60px">
	  <div class="row">
			<div class="col-lg-12">
				<p><strong>ECOM</strong> merupakan aplikasi permintaan alat tulis kantor milik Kuningan City Mall. Aplikasi ini boleh digunakan oleh seluruh karyawan <strong>PT. Sejahtera Kelola Abadi</strong> dan aplikasi ini hanya boleh digunakan dilingkungan perusahaan (tidak bisa digunakan diluar wilayah perusahaan).</p>

				<h5 class="text-success">Login</h5>
				<p>Klik tombol menu <strong>Masuk</strong> pada menu diatas terlebih dahulu. Setelah diklik menu tersebut Anda akan menuju halaman autentikasi login. Pada halaman tersebut Anda wajib memasukkan email dan password yang telah didaftarkan sebelumnya dan sesuai</p>
				<img src="<?php echo base_url('file-dokumentasi/fe/1.png') ?>" class="img-responsive" alt="">

				<h5 class="text-success">Register</h5>
				<p>Apabila Anda belum mempunyai akun aplikasi <strong>ECOM</strong> ini, Anda wajib mendaftarkan diri dengan meng-klik <strong>Belum Punya Akun?</strong>. Setelah meng-klik menu tersebut akan menuju halaman <strong>Register</strong>, dan Anda wajib mengisi beberapa <i>form input</i> yang tersedia.</p>
				<img src="<?php echo base_url('file-dokumentasi/fe/2.png') ?>" class="img-responsive" alt="">

				<h5 class="text-success">Barang</h5>
				<p>Pada menu <strong>Barang</strong>, terdapat beberapa barang-barang ATK yang telah didaftarkan oleh admin aplikasi <strong>ECOM</strong> ini. Seluruh karyawan PT. Sejahtera Kelola Abadi yang telah mendaftarkan dirinya pada aplikasi ini, dapat memilih barang-barang apa saja yang dibutuhkan. Klik gambar atau nama barang untuk menuju halaman <strong>Detail Barang</strong>.</p>
				<img src="<?php echo base_url('file-dokumentasi/fe/5.png') ?>" class="img-responsive" alt="">
				<p>Pada halaman tersebut ada beberapa ketentuan saat permintaan barang, untuk permintaan barang yang berjenis tinta boleh dilakukan pada hari Senin hingga Jumat mulai pukul 09.15 WIB hingga pukul 17.00 WIB. Sedangkan untuk permintaan barang yang bukan berjenis tinta boleh dilakukan pada hari Senin hingga Rabu dan mulai pukul 09.15 WIB hingga pukul 17.00 WIB. Karayawan tidak dapat melakukan permintaan barang melebihi jumlah stok barang yang tersedia.</p>
				<p>Apabila karyawan telah meilih barang, masukkan jumlah stok yang dibutuhkan kemudian klik tombol <strong>Tambahkan ke Dalam Keranjang</strong> untuk memilih barang yang dibutuhkan.</p>

				<h5 class="text-success">Keranjang</h5>
				<p>Menu keranjang ini berfungsi untuk melihat beberapa barang yang telah dipilih dan dimasukkan ke dalam keranjang permintaan barang. Lakukan proses checkout untuk menyelesaikan transaksi permintaan barang. Anda pun dapat menambahkan barang yang lainnya dengan cara meng-klik tombol <strong>Perbarui Quantity Barang</strong>.</p>
				<img src="<?php echo base_url('file-dokumentasi/fe/6.png') ?>" class="img-responsive" alt="">

				<h5 class="text-success">Riwayat Permintaan Barang</h5>
				<p>Sorot tombol nama pada bagian <i>header</i> halaman aplikasi, kemudian sorot menu <strong>Riwayat Permintaan Barang</strong>, pada menu tersebut karyawan dapat melihat histori permintaan barang, apakah permintaan barang ATK Anda telah disetujui oleh Manager Departemen Anda atau Admin Aplikasi ini.</p>
				<img src="<?php echo base_url('file-dokumentasi/fe/7.png') ?>" class="img-responsive" alt="">
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section>