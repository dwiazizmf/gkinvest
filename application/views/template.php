<!DOCTYPE html>
<html lang="en">

<head>
    <?php echo $header; ?>
</head>

<?php if (isset($home)) {
    echo '<body class="home">';
} elseif (isset($product)) {
    echo '<body class="shop">';
} else {
    echo '<body>';
} ?>


<div class="loading-overlay">
    <div class="bounce-loader">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
        <div class="bounce4"></div>
    </div>
</div>
<div class="page-wrapper">
    <h1 class="d-none">Donald - Responsive eCommerce HTML Template</h1>
    <?php if (isset($home)) {
        echo '<header class="header header-transparent">';
    } elseif (isset($product)) {
        echo '<header class="header header-transparent header-border">';
    } else {
        echo '<header class="header">';
    } ?>

    <div class="header-middle sticky-header fix-top sticky-content">
        <div class="container-fluid">
            <?php echo $menu; ?>
        </div>

    </div>
    </header>
    <!-- End Header -->

    <?php echo $content; ?>

    <!-- End Main -->
    <!-- Footer -->
    <?php echo $footer; ?>
    <!-- End Footer -->
</div>

<!-- mobile -->
<?php echo $mobile_view; ?>
<!-- End of mobile -->
<!-- javascript -->
<?php echo $js; ?>
<!-- end of javascript -->
</body>

</html>