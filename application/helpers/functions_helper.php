<?php

if ( !function_exists('gkinvestNewsUrl') ) {
  function gkinvestNewsUrl( $news = array() ){
      if(isset( $news['gkinvestNewsUrl']) ){
        return $news['gkinvestNewsUrl'];
      } else {
        list($date, $time) = explode(' ', $news['news_published_date']);
        list($year, $month, $day) = explode('-', $date);
        return site_url('news/read/'.$year.'/'.$month.'/'.$day.'/'.$news['news_id'].'/'.url_title($news['news_title'], '-', TRUE).'.html');
      }
  }
}
if ( !function_exists('gkinvestcURL') ) {
  function gkinvestcURL($Url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
  }
}
if ( !function_exists('gkinvestNewsCategoryUrl'))  {
  function gkinvestNewsCategoryUrl( $news = array() ) {
    return site_url('news/category/'.$news['category_id']);
  }
}
/*
 * [gkinvestEncrypt] 
 *   Function for encrypt all password
 *   @author Rico Oktavian Adhi Wibowo
 */
if ( !function_exists('gkinvestEncrypt') ) {
  function gkinvestEncrypt( $password ){
    $salt = "4ku6anttenGGG";
    $enc = md5( $salt.$password );
    return $enc;
  }
}

if ( !function_exists('gkinvestPenyebut')) {
  function gkinvestPenyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
      $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
      $temp = gkinvestPenyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
      $temp = gkinvestPenyebut($nilai/10)." puluh". gkinvestPenyebut($nilai % 10);
    } else if ($nilai < 200) {
      $temp = " seratus" . gkinvestPenyebut($nilai - 100);
    } else if ($nilai < 1000) {
      $temp = gkinvestPenyebut($nilai/100) . " ratus" . gkinvestPenyebut($nilai % 100);
    } else if ($nilai < 2000) {
      $temp = " seribu" . gkinvestPenyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
      $temp = gkinvestPenyebut($nilai/1000) . " ribu" . gkinvestPenyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
      $temp = gkinvestPenyebut($nilai/1000000) . " juta" . gkinvestPenyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
      $temp = gkinvestPenyebut($nilai/1000000000) . " milyar" . gkinvestPenyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
      $temp = gkinvestPenyebut($nilai/1000000000000) . " trilyun" . gkinvestPenyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
  }
}
if ( !function_exists('gkinvestTerbilang') ) {
  function gkinvestTerbilang($nilai) {
    if($nilai<0) {
      $hasil = "minus ". trim(gkinvestPenyebut($nilai));
    } else {
      $hasil = trim(gkinvestPenyebut($nilai));
    }         
    return $hasil;
  }
}
/*
 * [gkinvestPrettyDate] 
 *   Function for make a event date
 *   @author Rico Oktavian Adhi Wibowo
 */
if ( !function_exists('gkInvestEventDate') ) {
  function gkInvestEventDate($date = '', $format = '', $timezone = TRUE) {
    // Empty date
    if($date == '0000-00-00' OR empty($date)) {
      return '';
    }

    $dateStr = strtotime($date);

    if(empty($format)) {
      $datePretty = date('d/m/Y', $dateStr);
    } else {
      $datePretty = date($format, $dateStr);
    }

    // if($timezone) {
    //     $datePretty .= ' WIB';
    // }

    $datePretty = str_replace('Sunday', 'Minggu', $datePretty);
    $datePretty = str_replace('Monday', 'Senin', $datePretty);
    $datePretty = str_replace('Tuesday', 'Selasa', $datePretty);
    $datePretty = str_replace('Wednesday', 'Rabu', $datePretty);
    $datePretty = str_replace('Thursday', 'Kamis', $datePretty);
    $datePretty = str_replace('Friday', 'Jumat', $datePretty);
    $datePretty = str_replace('Saturday', 'Sabtu', $datePretty);

    $datePretty = str_replace('January', 'Januari', $datePretty);
    $datePretty = str_replace('February', 'Februari', $datePretty);
    $datePretty = str_replace('March', 'Maret', $datePretty);
    $datePretty = str_replace('April', 'April', $datePretty);
    $datePretty = str_replace('May', 'Mei', $datePretty);
    $datePretty = str_replace('June', 'Juni', $datePretty);
    $datePretty = str_replace('July', 'Juli', $datePretty);
    $datePretty = str_replace('August', 'Agustus', $datePretty);
    $datePretty = str_replace('September', 'September', $datePretty);
    $datePretty = str_replace('October', 'Oktober', $datePretty);
    $datePretty = str_replace('November', 'November', $datePretty);
    $datePretty = str_replace('December', 'Desember', $datePretty);
    
    return $datePretty;
  }
}
/*
 * [gkinvestPrettyDate] 
 *   Function for make a pretty date
 *   @author Rico Oktavian Adhi Wibowo
 */
if ( !function_exists('gkinvestPrettyDate') ) {
  function gkinvestPrettyDate($date = '', $format = '', $timezone = TRUE) {
    // Empty date
    if($date == '0000-00-00 00:00:00' OR empty($date)) {
      return '';
    }

    $dateStr = strtotime($date);

    if(empty($format)) {
      $datePretty = date('l, d/m/Y H:i', $dateStr);
    } else {
      $datePretty = date($format, $dateStr);
    }

    if($timezone) {
      $datePretty .= ' WIB';
    }

    $datePretty = str_replace('Sunday', 'Minggu', $datePretty);
    $datePretty = str_replace('Monday', 'Senin', $datePretty);
    $datePretty = str_replace('Tuesday', 'Selasa', $datePretty);
    $datePretty = str_replace('Wednesday', 'Rabu', $datePretty);
    $datePretty = str_replace('Thursday', 'Kamis', $datePretty);
    $datePretty = str_replace('Friday', 'Jumat', $datePretty);
    $datePretty = str_replace('Saturday', 'Sabtu', $datePretty);

    $datePretty = str_replace('January', 'Januari', $datePretty);
    $datePretty = str_replace('February', 'Februari', $datePretty);
    $datePretty = str_replace('March', 'Maret', $datePretty);
    $datePretty = str_replace('April', 'April', $datePretty);
    $datePretty = str_replace('May', 'Mei', $datePretty);
    $datePretty = str_replace('June', 'Juni', $datePretty);
    $datePretty = str_replace('July', 'Juli', $datePretty);
    $datePretty = str_replace('August', 'Agustus', $datePretty);
    $datePretty = str_replace('September', 'September', $datePretty);
    $datePretty = str_replace('October', 'Oktober', $datePretty);
    $datePretty = str_replace('November', 'November', $datePretty);
    $datePretty = str_replace('December', 'Desember', $datePretty);
    
    return $datePretty;
  }
}
/*
 * [gkInvestEventUrl] 
 *   Function for make an event url
 *   @author Rico Oktavian Adhi Wibowo
 */
if ( !function_exists('gkInvestEventUrl') ) {
  function gkInvestEventUrl($events = array()) {
    if(isset($events['gkInvestEventUrl'])) {
      return $events['gkInvestEventUrl'];
    } else {
      list($date, $time) = explode(' ', $events['createdAt']);
      list($year, $month, $day) = explode('-', $date);
      return site_url('event/read/'.$year.'/'.$month.'/'.$day.'/'.$events['id_event'].'/'.url_title($events['event_title'], '-', TRUE).'.html');
    }
  }
}
/*
 * [gkInvestPostUrl] 
 *   Function for make an post url
 *   @author Rico Oktavian Adhi Wibowo
 */
if ( !function_exists('gkInvestPostUrl') ) {
  function gkInvestPostUrl($posts = array()) {
    if(isset($posts['gkInvestPostUrl'])) {
      return $posts['gkInvestPostUrl'];
    } else {
      list($date, $time) = explode(' ', $posts['createdAt']);
      list($year, $month, $day) = explode('-', $date);
      return site_url('news/read/'.$year.'/'.$month.'/'.$day.'/'.$posts['id_post'].'/'.url_title($posts['post_title'], '-', TRUE).'.html');
    }
  }
}
/**
 * [gkInvestRupiah]
 *   Function for make an rupiah
 *   @author Rico Oktavian Adhi Wibowo
 */
if ( !function_exists('gkInvestRupiah') ) {
  function gkInvestRupiah($angka) {
    $hasil_rupiah = number_format($angka, 0,',','.');
    return $hasil_rupiah;
  }
}
/**
 * [gkInvestRupiah]
 *   Function for get a gkInvestGetId
 *   @author Rico Oktavian Adhi Wibowo 
 */
if ( !function_exists('gkInvestGetId') ) {
  function gkInvestGetId($n) { 
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
    $randomString = ''; 
  
    for ($i = 0; $i < $n; $i++) { 
      $index = rand(0, strlen($characters) - 1); 
      $randomString .= $characters[$index]; 
    } 
  
    return $randomString; 
  } 
}
/**
 * [gkInvestRupiah]
 *   Function for make an rupiah
 *   @author Rico Oktavian Adhi Wibowo
 */
if( !function_exists('gkInvestUUID') ) {
  function gkInvestUUID() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      // 32 bits for "time_low"
      mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

      // 16 bits for "time_mid"
      mt_rand( 0, 0xffff ),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      mt_rand( 0, 0x0fff ) | 0x4000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      mt_rand( 0, 0x3fff ) | 0x8000,

      // 48 bits for "node"
      mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
  }
}

?>