<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array('per_page' => 10,
                'uri_segment' => 4,
                'full_tag_open' => '<ul class="pagination mt-2">',
                'full_tag_close' => '</ul>',
                'next_tag_open' => '<li class="page-item">',
                'num_tag_open' => '<li class="page-item">',
                'prev_tag_open' => '<li class="page-item">',
                'first_tag_open' => '<li class="page-item">',
                'last_tag_open' => '<li class="page-item">',
                'next_tag_close' => '</li>',
                'num_tag_close' => '</li>',
                'prev_tag_close' => '</li>',
                'first_tag_close' => '</li>',
                'last_tag_close' => '</li>',
                'cur_tag_open' => '<li class="active page-item"><a href="#">',
                'cur_tag_close' => '</a></li>'
                );

/* End of file pagination.php */
/* Location: ./application/config/pagination.php */
