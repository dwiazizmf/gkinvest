<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_inventory extends CI_Model {
	public function __construct() {
      parent::__construct();
  }

  public function get() {
    return $this->db
                ->select('id_barang, sum(stock_barang) as stock')
                ->group_by('id_barang')
                ->get_where( 'ecom_inventory' )
                ->result_array();
  }

  public function getById( $id ) {
    return $this->db
                ->select_sum('stock_barang')
                ->where('id_barang', $id)
                ->get_where( 'ecom_inventory' )
                ->row_array();
  }

  public function post( $data ){
    $this->db->insert('ecom_inventory', $data);
  }
}
