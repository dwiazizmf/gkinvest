<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_permintaan_barang extends CI_Model {
	public function __construct() {
    parent::__construct();
    $this->load->library( 'session' );
  }

  public function get($params = array()){
    $sql= "SELECT a.*, b.*, a.createdAt AS tanggal
           FROM   ecom_permintaan_barang a, ecom_pengguna b
           WHERE  a.nik = b.nik
           AND    a.nik = ".$this->session->userdata('id')."
          ";
    return $this->db->query($sql)->result_array();
  }

  public function getLastOrderByNIK(){
    $sql= "SELECT a.*, b.*, a.createdAt AS tanggal, a.status_permintaan_barang AS status_admin_ga, a.status_manager_departemen as status_manager_departemen
           FROM   ecom_permintaan_barang a, ecom_pengguna b
           WHERE  a.nik = b.nik
           AND    a.nik = ".$this->session->userdata('id')."
           ORDER BY tanggal DESC
           LIMIT  1
          ";
    return $this->db->query($sql)->row_array();
  }

  public function getDetailPermintaanBarang($params = array()){
    if(isset($params['id'])) {
      $this->db->where('ecom_permintaan_barang.id_permintaan_barang', $params['id']);
      $this->db->where('ecom_permintaan_barang.nik', $this->session->userdata('id'));
    }

    if(isset($params['limit'])) {
      if(!isset($params['offset'])) {
        $params['offset'] = NULL;
      }

      $this->db->limit($params['limit'], $params['offset']);
    }

    if(isset($params['order_by'])) {
      $this->db->order_by('ecom_permintaan_barang.createdAt', $params['order_by']);
    } else {
      $this->db->order_by('ecom_permintaan_barang.createdAt', 'desc');
    }

    $this->db->select('*');
    $this->db->join( 'ecom_detail_permintaan_barang', 'ecom_permintaan_barang.id_permintaan_barang = ecom_detail_permintaan_barang.id_permintaan_barang' );
    $this->db->join( 'ecom_barang', 'ecom_detail_permintaan_barang.id_barang = ecom_barang.kode_barang' );
    $this->db->join( 'ecom_merk', 'ecom_barang.id_merk = ecom_merk.id_merk' );
    $this->db->join( 'ecom_pengguna', 'ecom_permintaan_barang.nik = ecom_pengguna.nik' );
    $res = $this->db->get('ecom_permintaan_barang');

    return $res->result_array();
  }

  public function post( $data ){
    $this->db->insert('ecom_permintaan_barang', $data);
    $id = $this->db->insert_id();
    return (isset($id)) ? $id : FALSE;
  }

  public function postDetailPermintaanBarang( $data ){
    $this->db->insert('ecom_detail_permintaan_barang', $data);
  }
}
