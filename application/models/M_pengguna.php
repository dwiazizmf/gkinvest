<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_pengguna extends CI_Model
{
  /**
   * [__construct description]
   */
  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
  }

  /**
   * [login description]
   * @param  [type] $username [description]
   * @param  [type] $password [description]
   * @return [type]           [description]
   */
  public function login($email, $password)
  {
    $psw   = gkinvestEncrypt($password);
    $query = $this->db->query("SELECT * FROM gkinvest_users 
                               LEFT JOIN gkinvest_role_user 
                               ON gkinvest_users.id_role_user = gkinvest_role_user.id_role_user 
                               WHERE gkinvest_users.email = '$email' AND gkinvest_users.password = '$psw'
                               AND gkinvest_users.status = 1
                               AND gkinvest_users.id_role_user = 4");
    if ($query->num_rows() == 1) {
      $data = $query->row_array();
      $newdata = array(
        'id'            => $data['id_user'],
        'email'         => $data['email'],
        'active'        => $data['status'],
        'status'        => $data['id_role_user'],
        'level'         => $data['role_user'],
        'username'      => $data['username'],
        'employee_name' => $data['fullname'],
        'is_AB'         => $data['is_AB'],
        'is_login'      => TRUE
      );
      $this->session->set_userdata($newdata);
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function get($params = array())
  {
    if (isset($params['id'])) {
      $this->db->where('id_user', $params['id']);
    }

    if (isset($params['limit'])) {
      if (!isset($params['offset'])) {
        $params['offset'] = NULL;
      }

      $this->db->limit($params['limit'], $params['offset']);
    }

    $res = $this->db->get('gkinvest_users');

    if (isset($params['id'])) {
      return $res->row_array();
    } else {
      return $res->result_array();
    }
  }

  public function getRewardActive($params = array())
  {
    if (isset($params['id'])) {
      $this->db->where('gkinvest_reward_active.id_user', $params['id']);
    } elseif (isset($params['date'])) {
      $this->db->where('gkinvest_reward_active.join_date', $params['date']);
    } elseif (isset($params['status'])) {
      $this->db->where('gkinvest_reward_active.id_status_reward_active', $params['status']);
      $this->db->where('gkinvest_reward_active.id_user', $this->session->userdata('id'));
    } elseif (isset($params['claimed'])) {
      $this->db->where('gkinvest_reward_active.id_status_reward_active', $params['claimed']);
      $this->db->where('gkinvest_reward_active.id_user', $this->session->userdata('id'));
    }

    if (isset($params['limit'])) {
      if (!isset($params['offset'])) {
        $params['offset'] = NULL;
      }

      $this->db->limit($params['limit'], $params['offset']);
    }

    if (isset($params['order_by'])) {
      $this->db->order_by($params['order_by']);
    } else {
      $this->db->order_by('gkinvest_reward_active.join_date', 'desc');
    }

    $this->db->select('*');
    $this->db->join('gkinvest_users', 'gkinvest_users.id_user = gkinvest_reward_active.id_user', 'left');
    $this->db->join('gkinvest_rewards', 'gkinvest_rewards.id_reward = gkinvest_reward_active.id_reward', 'left');

    if (isset($params['id'])) {
      $res = $this->db->get('gkinvest_reward_active');
      return $res->result_array();
    } elseif (isset($params['claimed'])) {
      $this->db->group_by('gkinvest_reward_active.id_transaction_reward');
      $res = $this->db->get('gkinvest_reward_active');
      return $res->result_array();
    } else {
      $this->db->group_by('gkinvest_reward_active.id_transaction_reward');
      $res = $this->db->get('gkinvest_reward_active');
      return $res->row_array();
    }
  }

  /**
   * [getByID description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function getByID($id)
  {
    $this->db->where('id_user', $id);
    $query = $this->db->get('gkinvest_users');
    return $query->row_array();
  }

  /**
   * Create new user
   * @param  {object}  data - data object.
   * @return {Promise}
   */
  public function post($data)
  {
    $this->db->insert('gkinvest_users', $data);
  }

  /**
   * [update description]
   * @param  [type] $id   [description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function update($id, $data)
  {
    $this->db->where('id_user', $id);
    return $this->db->update('gkinvest_users', $data);
  }
}
