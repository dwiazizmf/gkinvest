<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_reward_active extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function get($params = array())
  {
    if (isset($params['id'])) {
      $this->db->where('gkinvest_reward_active.id_user', $params['id']);
    } elseif (isset($params['date'])) {
      $this->db->where('gkinvest_reward_active.join_date', $params['date']);
    } elseif (isset($params['status'])) {
      $this->db->where('gkinvest_reward_active.id_status_reward_active', $params['status']);
    }

    if (isset($params['limit'])) {
      if (!isset($params['offset'])) {
        $params['offset'] = NULL;
      }

      $this->db->limit($params['limit'], $params['offset']);
    }

    if (isset($params['order_by'])) {
      $this->db->order_by($params['order_by']);
    } else {
      $this->db->order_by('gkinvest_reward_active.join_date', 'desc');
    }

    $this->db->select('*');
    $this->db->join('gkinvest_users', 'gkinvest_users.id_user = gkinvest_reward_active.id_user', 'left');
    $this->db->join('gkinvest_status_reward_active', 'gkinvest_status_reward_active.id_status_reward_active = gkinvest_reward_active.id_status_reward_active', 'left');
    $res = $this->db->get('gkinvest_reward_active');

    if (isset($params['id'])) {
      return $res->row_array();
    } else {
      return $res->result_array();
    }
  }

  public function getPoint($id)
  {
    $sql = "SELECT SUM(point_reward_active) as yourPoint
            FROM `gkinvest_reward_active`
            WHERE id_user = '$id'";

    return $this->db->query($sql)->result_array();
  }

  public function getByID($id)
  {
    $this->db->where('id_reward_active', $id);
    $query = $this->db->get('gkinvest_reward_active');
    return $query->row_array();
  }

  public function post($data)
  {
    $this->db->insert('gkinvest_reward_active', $data);
  }

  public function update_reward($id, $data)
  {
    $this->db->where('id_reward_active', $id);
    return $this->db->update('gkinvest_reward_active', $data);
  }

  public function update($id, $data)
  {
    $this->db->where('id_reward_active', $id);
    return $this->db->update('gkinvest_reward_active', $data);
  }

  public function delete($id)
  {
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_reward_active', $id);
    $this->db->delete('gkinvest_reward_active');
  }
}
