<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_claim_reward extends CI_Model {
	public function __construct() {
    parent::__construct();
  }

  public function post( $data ){
    $this->db->insert('gkinvest_claim_reward', $data);
  }

  public function delete( $id ){
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_claim_reward',$id);
    $this->db->delete('gkinvest_claim_reward');
  }
}
