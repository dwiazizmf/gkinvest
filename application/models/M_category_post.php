<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_category_post extends CI_Model {
	public function __construct() {
    parent::__construct();
  }

  public function get(){
    return $this->db
                ->order_by( "id_category_post", "desc" )
                ->get_where( 'gkinvest_category_news' )
                ->result_array();
  }

  public function getByID( $id ) {
    $this->db->where('id_category_post', $id);
    $query = $this->db->get('gkinvest_category_news');
    return $query->row_array();
  }
}
