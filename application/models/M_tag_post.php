<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tag_post extends CI_Model {
	public function __construct() {
        parent::__construct();
    }

    public function get(){
        return $this->db
                    ->order_by( "id_tag_post", "desc" )
                    ->get_where( 'gkinvest_tag_news' )
                    ->result_array();
    }

    public function getByID( $id ) {
        $this->db->where('id_tag_post', $id);
        $query = $this->db->get('gkinvest_tag_news');
        return $query->row_array();
    }
}
