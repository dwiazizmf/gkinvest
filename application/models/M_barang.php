<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang extends CI_Model {
	public function __construct() {
    parent::__construct();
  }

  public function get($params = array()) {
    if(isset($params['id'])) {
      $this->db->where('ecom_barang.kode_barang', $params['id']);
    } elseif(isset($params['title'])) {
      $this->db->where('ecom_barang.nama_barang', $params['title']);
    } elseif(isset($params['date'])) {
      $this->db->where('ecom_barang.createdAt', $params['date']);
    } elseif(isset($params['photo'])) {
      $this->db->where('ecom_barang.photo_barang', $params['photo']);
    }  

    if(isset($params['limit'])) {
      if(!isset($params['offset'])) {
        $params['offset'] = NULL;
      }

      $this->db->limit($params['limit'], $params['offset']);
    }

    if(isset($params['order_by'])) {
      $this->db->order_by('ecom_barang.createdAt', $params['order_by']);
    } else {
      $this->db->order_by('ecom_barang.createdAt', 'desc');
    }

    $this->db->select('*');
    $this->db->join( 'ecom_jenis_barang', 'ecom_barang.id_jenis_barang = ecom_jenis_barang.id_jenis_barang', 'left' );
    $this->db->join( 'ecom_merk', 'ecom_barang.id_merk = ecom_merk.id_merk', 'left' );
    $res = $this->db->get('ecom_barang');

    if(isset($params['id'])) {
      return $res->row_array();
    } else {
      return $res->result_array();
    }
  }

  public function search($keyword) {
    $this->db->select('*');
    $this->db->join( 'ecom_jenis_barang', 'ecom_barang.id_jenis_barang = ecom_jenis_barang.id_jenis_barang' );
    $this->db->join( 'ecom_merk', 'ecom_barang.id_merk = ecom_merk.id_merk' );
    $this->db->or_like(array('kode_barang' => $keyword, 'nama_barang' => $keyword, 'nama_merk' => $keyword));
    $query = $this->db->get('ecom_barang');
    return $query->result_array();
  }

  public function getByID( $id ) {
    return $this->db
                ->order_by( "id_barang", "desc" )
                ->join( 'ecom_jenis_barang', 'ecom_barang.id_jenis_barang = ecom_jenis_barang.id_jenis_barang' ) 
                ->join( 'ecom_merk', 'ecom_barang.id_merk = ecom_merk.id_merk' )
                ->where('id_barang', $id)
                ->get_where( 'ecom_barang' )
                ->row_array();
  }

  public function post( $data ){
    $this->db->insert('ecom_barang', $data);
  }
  
  public function patch( $id, $data ){
    $this->db->where('id_barang', $id);
    return $this->db->update('ecom_barang', $data); 
  }

  public function delete($id){
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_barang',$id);
    $this->db->delete('ecom_barang');
  }
}
