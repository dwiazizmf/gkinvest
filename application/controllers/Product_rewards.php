<?php
ini_set('date.timezone', 'Asia/Jakarta');

defined('BASEPATH') or exit('No direct script access allowed');

class Product_rewards extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(array('m_product_rewards', 'm_reward_active', 'm_pengguna'));
    $this->load->library('pagination');
    $this->load->helper('url');
    if ($this->session->userdata('is_login') && $this->session->userdata('status') != 4) {
      redirect('admin/home');
    }
  }

  public function index($offset = NULL)
  {
    $data = array();
    $data['product_rewards'] = $this->m_product_rewards->get(array('order_by' => 'DESC', 'offset' => $offset));

    $config['base_url']     = site_url('product_rewards/index');
    $config['total_rows']   = count($this->m_product_rewards->get(array('order_by' => 'desc'))) + 1;
    $config['per_page']     = 12;
    $config['uri_segment']  = 3;

    $data['pagination'] = $this->pagination->create_links();

    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_reward_active->get(array('id' => $this->session->userdata('id'), 'limit' => 1));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));


    $data['product_reward_active']  = $this->m_pengguna->getRewardActive(array('status' => 2));
    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header', null, true);
    $html['menu']      = $this->load->view('menu', $data, true);
    $html['content']  = $this->load->view('product-rewards/list', $data, true);
    $html['mobile_view']   = $this->load->view('mobile_view', null, true);
    $html['footer']   = $this->load->view('footer', null, true);
    $html['js']       = $this->load->view('js', null, true);
    $html['product'] = 'home';
    $this->load->view('template', $html);
  }

  //perubahan fnc ini
  public function point($offset = NULL)
  {
    $data['point']  = $this->m_reward_active->getPoint($this->session->userdata('id'));

    $uri = $data['point'][0]['yourPoint'];

    $data = array();
    $data['product_rewards_point'] = $this->m_product_rewards->get(array('uri' => $uri, 'order_by' => 'DESC', 'offset' => $offset));

    $data['product_rewards'] = $this->m_product_rewards->get(array('order_by' => 'DESC', 'offset' => $offset));
    $config['base_url']     = site_url('product_rewards/index');
    $config['total_rows']   = count($this->m_product_rewards->get(array('uri' => $uri, 'order_by' => 'desc'))) + 1;
    $config['per_page']     = 12;
    $config['uri_segment']  = 3;

    $data['pagination'] = $this->pagination->create_links();

    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_reward_active->get(array('id' => $this->session->userdata('id'), 'limit' => 1));

    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));
    //sampai sini

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header', null, true);
    $html['menu']     = $this->load->view('menu', $data, true);
    $html['content']  = $this->load->view('product-rewards/list_point', $data, true);
    $html['footer']   = $this->load->view('footer', null, true);
    $html['js']       = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }
  //sampai sini


  public function detail($id = NULL, $name = '')
  {
    $data = array();
    $data['product_rewards']  = $this->m_product_rewards->get(array('id' => $id));
    $data['product_random']   = $this->m_product_rewards->get_random(array('limit' => 6));

    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_pengguna->getRewardActive(array('status' => 2));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));
    $data['point_last']     = $name;

    $data['product_reward_active']  = $this->m_pengguna->getRewardActive(array('status' => 2));

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('product-rewards/detail', $data, true);
    $html['mobile_view']   = $this->load->view('mobile_view', null, true);
    $html['footer']         = $this->load->view('footer', null, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }

  public function quickview($id = NULL, $name = '')
  {
    $data = array();
    $data['product_rewards']        = $this->m_product_rewards->get(array('id' => $id));

    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_pengguna->getRewardActive(array('status' => 2));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));
    $data['point_last']     = $name;
    echo $this->load->view('quickview', $data, true);
  }


  public function category($id = NULL, $offset = NULL, $name = '')
  {
    $data = array();
    $data['product_rewards'] = $this->m_product_rewards->get(array('cid' => $id, 'order_by' => 'DESC', 'limit' => 2, 'offset' => $offset));

    $config['base_url']     = site_url('product_rewards/category');
    $config['total_rows']   = count($this->m_product_rewards->get(array('order_by' => 'desc'))) + 1;
    $config['per_page']     = 2;
    $config['uri_segment']  = 5;
    $this->pagination->initialize($config);

    $data['pagination'] = $this->pagination->create_links();

    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_reward_active->get(array('id' => $this->session->userdata('id'), 'limit' => 1));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));


    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('product-rewards/list', $data, true);
    $html['footer']         = $this->load->view('footer', null, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }
}
