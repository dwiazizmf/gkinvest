<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {
	function __construct() {
    parent::__construct();
    $this->load->model('M_pengguna');
    $this->load->model('M_permintaan_barang');
    $this->load->model('M_inventory');
    $this->load->library('cart');
    $this->load->library('session');
    $this->load->library('email');
    if( !$this->session->userdata('is_login' ) ){
      redirect('auth');
    }
  }

  /*
   * [GET] /request
   *    Get all request barang
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function index() { 
    $data = array();

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header',null, true);
    $html['menu']     = $this->load->view('menu', null, true);
    $html['content']  = $this->load->view('permintaan/list', null, true);   
    $html['footer']   = $this->load->view('footer',null, true);
    $html['js']       = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  /*
   * [GET] /request
   *    Get all request barang
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function history() { 
    $data = array();
    $data['list'] = $this->M_permintaan_barang->get();

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header',null, true);
    $html['menu']     = $this->load->view('menu', null, true);
    $html['content']  = $this->load->view('permintaan/all', $data, true);   
    $html['footer']   = $this->load->view('footer',null, true);
    $html['js']       = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  /*
   * [GET] /request
   *    Get all request barang
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function detail($id) { 
    $data = array();
    $data['list'] = $this->M_permintaan_barang->getDetailPermintaanBarang(array('id' => $id));

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header',null, true);
    $html['menu']     = $this->load->view('menu', null, true);
    $html['content']  = $this->load->view('permintaan/detail', $data, true);   
    $html['footer']   = $this->load->view('footer',null, true);
    $html['js']       = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

	/*
   * [ADD] /request
   *    Add new request barang
   * @author Rico Oktavian Adhi Wibowo
   */
  public function add() {
    $data = array(  'id'    => $this->input->post('kode_barang'),
                    'qty'   => $this->input->post('qty'),
                    'price' => $this->input->post('harga_barang'),
                    'name'  => $this->input->post('nama_barang')
                 );
    // var_dump($data);
    $this->cart->insert($data);
    // if success
    $this->session->set_flashdata('success', 'Barang berhasil direquest cek <strong>keranjang permintaanmu</strong>!');
    redirect('barang');
  }

  /*
   * [PATCH] /request/:id
   *    Update request's info by id
   * @author Rico Oktavian Adhi Wibowo.
   */
  public function put() {
    $cart_info = $_POST['cart'] ;
    foreach( $cart_info as $id => $cart) {
      $rowid  = $cart['rowid'];
      $price  = $cart['price'];
      $gambar = $cart['gambar'];
      $amount = $price * $cart['qty'];
      $qty    = $cart['qty'];
      $data   = array(  'rowid'   => $rowid,
                        'price'   => $price,
                        'gambar'  => $gambar,
                        'amount'  => $amount,
                        'qty'     => $qty
                     );
      $this->cart->update($data);
    }
    redirect('request');
  }

  /*
   * [DELETE] /request/:id
   *    Remove request from cart. Please becareful using this!
   * @author Rico Oktavian Adhi Wibowo.
   */
  public function delete($rowid) {
    if ($rowid == "all") {
      $this->cart->destroy();
    } else {
      $data = array(  'rowid' => $rowid,
                      'qty'   => 0
                   );
      $this->cart->update($data);
    }
    redirect('request');
  }

  /**
   * 
   */
  public function checkout() {
    $data = array();
    // $data['user'] = $this->M_pengguna->get(array('id' => $id));

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header',null, true);
    $html['menu']     = $this->load->view('menu', null, true);
    $html['content']  = $this->load->view('permintaan/checkout', $data, true);   
    $html['footer']   = $this->load->view('footer',null, true);
    $html['js']       = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  public function postOrder() {
    $dataOrder = array( 'nik'                       => $this->input->post('nik'),
                        'status_manager_departemen' => 'Pending',
                        'createdAt'                 => $this->input->post('createdAt')
                      );
    $idOrder = $this->M_permintaan_barang->post( $dataOrder );

    if ($cart = $this->cart->contents()) {
      foreach ($cart as $item) {
        $dataDetailOrder = array( 'id_permintaan_barang'      => $idOrder,
                                  'id_barang'                 => $item['id'],
                                  'quantity_permintaan_barang'=> $item['qty'],
                                  'createdAt'                 => date("Y-m-d H:i:s")
                                );
        $dataInventory   = array( 'id_permintaan_barang'      => $idOrder,
                                  'id_barang'                 => $item['id'],
                                  'jenis_transaksi'           => 1,
                                  'stock_barang'              => '-'.$item['qty'],
                                  'credit'                    => $item['qty'],
                                  'createdAt'                 => date("Y-m-d H:i:s")
                                );
        $proses = $this->M_permintaan_barang->postDetailPermintaanBarang( $dataDetailOrder );
        $insertInvetory = $this->M_inventory->post( $dataInventory );
      }
    }

    $this->cart->destroy();
    $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
    redirect('request/history');
  }
}
