<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(array('m_news', 'm_category_rewards', 'm_product_rewards', 'm_reward_active', 'm_pengguna'));
    $this->load->library('pagination');
    $this->load->model('M_permintaan_barang');
    $this->load->library('session');
    if ($this->session->userdata('is_login') && $this->session->userdata('status') != 4) {
      redirect('admin/home');
    }
  }

  public function index($offset = NULL)
  {
    // date_default_timezone_set('Asia/Jakarta');
    // echo 'Indonesian Timezone: ' . date('d-m-Y H:i:s');

    $data['news']                   = $this->m_news->get();
    $data['category_rewards']       = $this->m_category_rewards->get(array('limit' => 2));
    $data['product_rewards']        = $this->m_product_rewards->get(array('limit' => 6));
    $data['product_rewards_old']    = $this->m_product_rewards->get(array('cid' => 3, 'order_by' => 'DESC', 'limit' => 6));
    $data['product_rewards_big']    = $this->m_product_rewards->get(array('cid' => 2, 'order_by' => 'DESC', 'limit' => 6));
    $data['product_rewards_monthly']    = $this->m_product_rewards->get(array('cid' => 1, 'order_by' => 'DESC', 'limit' => 6));


    $data['point']                  = $this->m_reward_active->getPoint($this->session->userdata('id'));
    $data['product_reward_active']  = $this->m_pengguna->getRewardActive(array('status' => 2));

    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_reward_active->get(array('id' => $this->session->userdata('id'), 'limit' => 1));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));


    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header', array("home" => 'home'), true);
    $html['menu']      = $this->load->view('menu', $data, true);
    $html['content']  = $this->load->view('home/index', $data, true);
    $html['footer']   = $this->load->view('footer', null, true);
    $html['mobile_view']   = $this->load->view('mobile_view', null, true);
    $html['js']       = $this->load->view('js', null, true);
    $html['home'] = 'home';
    $this->load->view('template', $html);
  }

  public function search_keyword()
  {
    $keyword        = $this->input->post('query');
    $data['barang'] = $this->m_barang->search($keyword);
    $data['stock']  = $this->m_inventory->get();
    $data['request'] = $this->M_permintaan_barang->getLastOrderByNIK();

    /**
     * [$html call all wireframe] 
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header', null, true);
    $html['menu']     = $this->load->view('menu', null, true);
    $html['content']  = $this->load->view('barang/list', $data, true);
    $html['footer']   = $this->load->view('footer', null, true);
    $html['js']       = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }

  public function detail($id = NULL, $name = '')
  {
    $this->load->helper('url');
    $data = array();
    $data['barang'] = $this->m_barang->get(array('id' => $id));

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('home/index', $data, true);
    $html['footer']         = $this->load->view('footer', null, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }

  public function login_view()
  {
    $this->load->view('login');
  }
}
