<?php
ini_set('date.timezone', 'Asia/Jakarta');

defined('BASEPATH') or exit('No direct script access allowed');

class News extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(array('m_news', 'm_tag_post', 'm_category_post', 'm_reward_active', 'm_pengguna'));
    $this->load->library('pagination');
    $this->load->helper('url');
    if ($this->session->userdata('is_login') && $this->session->userdata('status') != 4) {
      redirect('admin/home');
    }
  }

  public function index($offset = NULL)
  {
    $data = array();
    $data['cat_post'] = $this->m_category_post->get();
    $data['tag_post'] = $this->m_tag_post->get();
    $data['news'] = $this->m_news->get(array('status' => 'Publish', 'offset' => $offset));

    $data['product_reward_active']  = $this->m_pengguna->getRewardActive(array('status' => 2));

    $config['base_url'] = site_url('news/index');
    $config['total_rows'] = count($this->m_news->get(array('status' => 'Publish'))) + 1;;
    $config['uri_segment'] = 3;
    $config['per_page'] = 10;

    $data['pagination'] = $this->pagination->create_links();

    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_reward_active->get(array('id' => $this->session->userdata('id'), 'limit' => 1));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));


    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header', null, true);
    $html['menu']      = $this->load->view('menu', $data, true);
    $html['mobile_view']   = $this->load->view('mobile_view', null, true);
    $html['content']  = $this->load->view('news/list', $data, true);
    $html['footer']   = $this->load->view('footer', null, true);
    $html['js']       = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }

  public function detail($id = NULL, $name = '')
  {
    $data = array();
    $data['cat_post'] = $this->m_category_post->get();
    $data['tag_post'] = $this->m_tag_post->get();
    $data['news'] = $this->m_news->get(array('id' => $id));

    $data['product_reward_active']  = $this->m_pengguna->getRewardActive(array('status' => 2));
    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_pengguna->getRewardActive(array('status' => 2));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));


    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('news/detail', $data, true);
    $html['mobile_view']   = $this->load->view('mobile_view', null, true);
    $html['footer']         = $this->load->view('footer', null, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }
}
