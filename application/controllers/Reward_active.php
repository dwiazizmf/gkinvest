<?php
ini_set('date.timezone', 'Asia/Jakarta');

defined('BASEPATH') or exit('No direct script access allowed');

class Reward_active extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(array('m_reward_active', 'm_claim_reward'));
    $this->load->library('email');
    if (!$this->session->userdata('is_login')) {
      redirect('auth');
    } elseif ($this->session->userdata('is_login') && $this->session->userdata('status') != 4) {
      redirect('admin/home');
    }
  }

  /*
   * [GET] /request
   *    Get all request barang
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function index()
  {
    $data = array();
    $data['reward_active'] = $this->m_reward_active->get(array('id' => $this->session->userdata('id')));

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header', null, true);
    $html['menu']     = $this->load->view('menu', $data, true);
    $html['content']  = $this->load->view('permintaan/list', null, true);
    $html['footer']   = $this->load->view('footer', null, true);
    $html['js']       = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }

  /*
   * [ADD] /request
   *    Add new request barang
   * @author Rico Oktavian Adhi Wibowo
   */
  public function add()
  {
    $data = array(
      'id_transaction_reward'   => substr(md5(time()), 0, 12),
      'id_user'                 => $this->input->post('id_user'),
      'id_reward'               => $this->input->post('id_reward'),
      'id_status_reward_active' => 1,
      'join_date'               => $this->input->post('join_date')

    );
    $this->m_reward_active->post($data);
    $this->session->set_flashdata('success', 'Success and please check your reward!');
    redirect('/');
  }

  public function update()
  {
    $id = $this->input->post('id_reward_active');
    $data = array('id_reward' => $this->input->post('id_reward'));

    $this->m_reward_active->update_reward($id,  $data);
    $this->session->set_flashdata('success', 'Success and please check your reward!');
    redirect('/');
  }

  /**
   * [claim]
   */
  public function claim()
  {
    $data = array(
      'id_transaction_reward'   => $this->input->post('id_transaction_reward'),
      'id_user'                 => $this->input->post('id_user'),
      'id_reward'               => $this->input->post('id_reward'),
      'id_status_reward_active' => $this->input->post('id_status_reward_active'),
      'point_reward_active'     => $this->input->post('point_reward_active'),
      'join_date'               => date("Y-m-d H:i:s")
    );
    $this->m_reward_active->post($data);

    $dataClaim = array(
      'id_transaction_reward'  => $this->input->post('id_transaction_reward'),
      'name'                   => $this->input->post('name'),
      'handphone_number'       => $this->input->post('handphone_number'),
      'address'                => $this->input->post('address'),
      'post_code'              => $this->input->post('post_code'),
      'updateAt'               => date("Y-m-d H:i:s"),
      'createdAt'              => date("Y-m-d H:i:s")
    );
    $this->m_claim_reward->post($dataClaim);
    $this->session->set_flashdata('success', 'Success and please check your reward!');
    redirect('users/your_reward_active');
  }

  /**
   * [Confirmation product reward has been arrived]
   */
  public function confirm()
  {
    $id = $this->input->post('id_reward_active');

    $data = array('id_status_reward_active' => 6);
    $this->m_reward_active->update($id, $data);
    $this->session->set_flashdata('success', 'Success and please check your reward!');
    redirect('users/your_reward_active');
  }
}
