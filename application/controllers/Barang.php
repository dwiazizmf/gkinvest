<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
	function __construct() {
  parent::__construct();
    $this->load->model('M_barang');
    $this->load->model('M_inventory');
    $this->load->model('M_permintaan_barang');
    $this->load->library('pagination');
    $this->load->helper('url');
    // $this->load->library('session');
    // if( !$this->session->userdata('is_login') ){
    //     redirect('admin/login');
    // }
  }

	public function index($offset = NULL) { 
    $data = array();
    $data['barang'] = $this->M_barang->get(array('order_by' => 'desc', 'limit' => 12, 'offset' => $offset));
    $data['stock']  = $this->M_inventory->get();

    if( $this->session->userdata('id') != null )
    $data['request']= $this->M_permintaan_barang->getLastOrderByNIK();

    $config['base_url'] = site_url('barang/index');
    $config['total_rows'] = count($this->M_barang->get(array('order_by' => 'desc')))+1;
    $config['per_page'] = 12;
    $config['uri_segment'] = 3;
    $this->pagination->initialize($config);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header',null, true);
    $html['menu']    	= $this->load->view('menu', null, true);
    $html['content']  = $this->load->view('barang/list', $data, true);   
    $html['footer']   = $this->load->view('footer',null, true);
    $html['js']       = $this->load->view('js',null, true);
    $this->load->view('template',$html);
	}

  public function detail($id = NULL, $name = '') {
    $data = array();
    $data['barang'] = $this->M_barang->get(array('id' => $id));
    $data['stock']  = $this->M_inventory->getById( $id );
    $data['request']= $this->M_permintaan_barang->getLastOrderByNIK();
    
    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']           = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('barang/detail', $data, true);   
    $html['footer']         = $this->load->view('footer',null, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }
}
