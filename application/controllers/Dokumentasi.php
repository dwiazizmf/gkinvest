<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumentasi extends CI_Controller {
	function __construct() {
  parent::__construct();
    $this->load->model('M_barang');
    $this->load->library('pagination');
    // $this->load->library('session');
    // if( !$this->session->userdata('is_login' ) ){
    //     redirect('admin/login');
    // }
  }

	public function index() {
    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header',null, true);
    $html['menu']    	= $this->load->view('menu', null, true);
    $html['content']  = $this->load->view('dokumentasi/index', null, true);   
    $html['footer']   = $this->load->view('footer',null, true);
    $html['js']       = $this->load->view('js',null, true);
    $this->load->view('template',$html);
	}

  public function search_keyword() {
    $keyword        = $this->input->post('query');
    $data['barang'] = $this->M_barang->search($keyword);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header',null, true);
    $html['menu']     = $this->load->view('menu', null, true);
    $html['content']  = $this->load->view('barang/list', $data, true);   
    $html['footer']   = $this->load->view('footer',null, true);
    $html['js']       = $this->load->view('js',null, true);
    $this->load->view('template', $html);
  }
}
