<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Not_found extends CI_Controller {
	function __construct() {
  parent::__construct();
    $this->load->model(array('m_reward_active', 'm_pengguna'));
    $this->load->library('pagination');
    // $this->load->library('session');
    // if( !$this->session->userdata('is_login' ) ){
    //     redirect('admin/login');
    // }
  }

	public function index($offset = NULL) {     
    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_reward_active->get(array('id' => $this->session->userdata('id'), 'limit' => 1));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));


    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header',null, true);
    $html['menu']    	= $this->load->view('menu', $data, true);
    $html['content']  = $this->load->view('errors/404', $data, true);   
    $html['footer']   = $this->load->view('footer',null, true);
    $html['js']       = $this->load->view('js',null, true);
    $this->load->view('template',$html);
	}
}
