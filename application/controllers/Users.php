<?php
ini_set('date.timezone', 'Asia/Jakarta');

defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->model(array('m_pengguna', 'm_reward_active', 'm_product_rewards'));
  }

  /*
   * [GET] /users
   *    Get all pengguna
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function index()
  {
    redirect('users/register');
  }

  /*
   * [GET] /:detail/users
   *    Get all pengguna
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function me($id = NULL)
  {
    if (!$this->session->userdata('is_login')) {
      redirect('/');
    }

    $data = array();
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_reward_active->get(array('id' => $this->session->userdata('id')));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('users/me', $data, true);
    $html['footer']         = $this->load->view('footer', null, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }

  public function changepassword($id = NULL)
  {
    if (!$this->session->userdata('is_login')) {
      redirect('/');
    }

    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['user'] = $this->m_pengguna->getByID($id);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('users/change-password', $data, true);
    $html['footer']         = $this->load->view('footer', null, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }

  /*
   * [ADD] /user
   *    Add new user
   * @author Rico Oktavian Adhi Wibowo
   */
  public function register()
  {
    $this->load->view('users/register');
  }

  public function post()
  {
    $config['file_name'] = 'pict_' . date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/fe/users';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    if ($this->validation() == false) {
      echo $this->upload->display_errors();
      $this->session->set_flashdata('error', 'Please try again, you failed to add new pengguna8.');
      redirect('users/register');
    } else {
      if ($_FILES['file']['name'] == "") {
        $data = array(
          'id_user'             => mt_rand(10000000, 99999999),
          'username'            => $this->input->post('username'),
          'email'               => $this->input->post('email'),
          'id_gkinvest'         => $this->input->post('id-gk'),
          'id_role_user'        => 4,
          'password'            => gkinvestEncrypt($this->input->post('password')),
          'status'              => 0,
          'createdAt'           => date("Y-m-d H:i:s")
        );
        $this->m_pengguna->post($data);
        $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
        redirect('auth');
      } else {
        if ($this->upload->do_upload('file')) {
          $image = $this->upload->data();
          $data = array(
            'id_user'             => mt_rand(10000000, 99999999),
            'username'            => $this->input->post('username'),
            'email'               => $this->input->post('email'),
            'id_role_user'        => 4,
            'password'            => gkinvestEncrypt($this->input->post('password')),
            'status'              => 0,
            'createdAt'           => date("Y-m-d H:i:s"),
            'photo'               => $image['file_name']
          );
          // var_dump($data);
          $this->m_pengguna->post($data);

          $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
          redirect('auth');
        } else {
          // var_dump($data);
          $this->session->set_flashdata('error', 'Please try again, you failed to add new pengguna.');
          redirect('users/register');
          echo $this->upload->display_errors();
        }
      }
    }
  }

  /*
   * [PATCH] /user
   *    Add new user
   * @author Rico Oktavian Adhi Wibowo
   */
  public function edit($id)
  {
    if (!$this->session->userdata('is_login')) {
      redirect('/');
    }

    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['rows'] = $this->m_pengguna->getByID($id);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('users/edit', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }

  public function put()
  {
    $config['file_name'] = 'pict_' . date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/fe/users';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    $id = $this->input->post('id_user');
    $password = $this->input->post('password');

    if ($_FILES['file']['name'] == "" && empty($password)) {
      $data = array(
        'fullname'            => $this->input->post('fullname'),
        'username'            => $this->input->post('username'),
        'email'               => $this->input->post('email'),
        'departemen'          => $this->input->post('departemen'),
        'tanggal_lahir'       => $this->input->post('tanggal_lahir'),
        'alamat'              => $this->input->post('alamat'),
        'kode_pos'            => $this->input->post('kode_pos'),
        'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
        'handphone'           => $this->input->post('handphone'),
        'status'              => 1,
        'updatedAt'           => $this->input->post('updatedAt')
      );
      $this->m_pengguna->update($id, $data);
      $this->session->set_flashdata('success', 'Congratulations, You have successfully to updated your data profile');
      redirect('users/me/');
    } elseif ($_FILES['file']['name'] != "") {
      if ($this->upload->do_upload('file')) {
        $image = $this->upload->data();
        $data = array(
          'fullname'            => $this->input->post('fullname'),
          'username'            => $this->input->post('username'),
          'email'               => $this->input->post('email'),
          'departemen'          => $this->input->post('departemen'),
          'tanggal_lahir'       => $this->input->post('tanggal_lahir'),
          'alamat'              => $this->input->post('alamat'),
          'kode_pos'            => $this->input->post('kode_pos'),
          'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
          'handphone'           => $this->input->post('handphone'),
          'status'              => 1,
          'updatedAt'           => $this->input->post('updatedAt'),
          'photo'               => $image['file_name']
        );
        // var_dump($data);
        $this->m_pengguna->update($id, $data);
        $this->session->set_flashdata('success', 'Congratulations, You have successfully to updated your data profile');
        redirect('users/me/');
      } else {
        // var_dump($data);
        $this->session->set_flashdata('error', 'Please try again, you failed to add new pengguna.');
        redirect('users/me/');
        echo $this->upload->display_errors();
      }
    } elseif ($_FILES['file']['name'] == "" && !empty($password)) {
      $data = array(
        'password'            => gkinvestEncrypt($this->input->post('password')),
        'updatedAt'           => $this->input->post('updatedAt')
      );
      $this->m_pengguna->update($id, $data);
      $this->session->set_flashdata('success', 'Congratulations, You have successfully to updated your password');
      redirect('users/changepassword/' . $id);
    }
  }

  /*
   * [GET] /reward active
   *    Get all reward active for user
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function your_reward_active()
  {
    if (!$this->session->userdata('is_login')) {
      redirect('auth');
    }

    $data = array();
    $data['product_reward_active']  = $this->m_pengguna->getRewardActive(array('status' => 2));

    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_reward_active->get(array('id' => $this->session->userdata('id'), 'limit' => 1));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));

    $data['product_random']   = $this->m_product_rewards->get_random(array('limit' => 6));
    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['mobile_view']   = $this->load->view('mobile_view', null, true);
    $html['header']   = $this->load->view('header', null, true);
    $html['menu']     = $this->load->view('menu', $data, true);
    $html['content']  = $this->load->view('users/reward-active', $data, true);
    $html['footer']   = $this->load->view('footer', null, true);
    $html['js']       = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }

  /*
   * [GET] /reward claim
   *    Get all reward active for user
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function your_reward_claim()
  {
    if (!$this->session->userdata('is_login')) {
      redirect('auth');
    }

    $data = array();
    $data['product_reward_active']  = $this->m_pengguna->getRewardActive(array('claimed' => 7));

    // data on menu
    $data['user']           = $this->m_pengguna->get(array('id' => $this->session->userdata('id')));
    $data['reward_active']  = $this->m_reward_active->get(array('id' => $this->session->userdata('id'), 'limit' => 1));
    $data['point']          = $this->m_reward_active->getPoint($this->session->userdata('id'));

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html             = array();
    $html['header']   = $this->load->view('header', null, true);
    $html['menu']     = $this->load->view('menu', $data, true);
    $html['content']  = $this->load->view('users/claim-reward', $data, true);
    $html['footer']   = $this->load->view('footer', null, true);
    $html['js']       = $this->load->view('js', null, true);
    $this->load->view('template', $html);
  }

  /*
   * [VALIDATION] 
   *     Validation form
   *     @author Rico Oktavian Adhi Wibowo
   */
  public function validation()
  {
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|matches[password]');
    return $this->form_validation->run();
  }
}
