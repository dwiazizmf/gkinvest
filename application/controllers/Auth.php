<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper('form');
    $this->load->library('upload');
    $this->load->model('m_pengguna');
  }

  public function index()
  {
    redirect('auth/login');
  }

  public function login()
  {
    if ($this->session->userdata('is_login') === TRUE) {
      redirect('/', 'refresh');
    }
    $this->load->view('login');
  }

  public function log()
  {
    $target     = '';
    $msg        = '';
    $email      = $this->input->post('email');
    $password   = $this->input->post('password');
    $login      = $this->m_pengguna->login($email, $password);

    echo json_encode($login);
    /*if ($login == TRUE) {
      redirect('/');
    } else {
      $this->session->set_flashdata('error', 'E-mail and password do not match, please try again!');
      redirect('/auth/login/');
    } */
  }

  public function logout()
  {
    $this->session->sess_destroy();
    redirect(base_url());
  }

  private function _get_flashdata()
  {
    $msg = $this->session->flashdata("process_msg");
    if (empty($msg)) {
      return array("type" => "hidden", "content" => "");
    } else {
      return $msg;
    }
  }
}
