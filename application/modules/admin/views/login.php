<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>GKInvest</title>

  <link href="<?php echo base_url('assets/backend/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/backend/dist/css/sb-admin-2.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/backend/vendor/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Please Sign In</h3>
          </div>
          <div class="panel-body">
            <form role="form" action="<?php echo base_url('admin/login/log'); ?>" method="post">
              <fieldset>
                <div class="form-group">
                  <input class="form-control" placeholder="Email" name="email" type="email" autocomplete="off">
                </div><!-- /.form-group -->
                <div class="form-group">
                  <input class="form-control" placeholder="Password" name="password" type="password" value="">
                </div><!-- /.form-group -->
                <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
              </fieldset>
            </form><!-- /.form -->
          </div><!-- /.panel-body -->
        </div><!-- /.login-panel panel panel-default -->
      </div><!-- /.col-md-4 col-md-offset-4 -->
    </div><!-- /.row -->
  </div><!-- /.container -->

  <!-- jQuery -->
  <script src="<?php echo base_url('assets/backend/vendor/jquery/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/backend/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/backend/dist/js/sb-admin-2.js') ?>"></script>
</body>
</html>
