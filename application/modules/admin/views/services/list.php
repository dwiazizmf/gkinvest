<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Services</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-4">
      <h4>Add Services</h4>
      <form class="" method="post" action="<?php echo base_url('admin/services/post') ?>">
        <div class="form-group">
          <label>Tite</label>
          <input type="text" class="form-control" name="service_taxonomy">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Link URL</label>
          <input type="text" class="form-control" name="service_url">
        </div><!-- /.form-group -->
        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="createdAt">
        </div>
        <button class="btn btn-primary" type="submit"><i class="fa fa-plus margin-right-15px"></i> Add New Link</button>
      </form>
    </div><!-- /.col-lg-4 --> 

    <div class="col-lg-8">
      <div class="panel panel-primary">
        <div class="panel-heading">
          List Services
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
          <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered" id="dataTables-example">
              <thead>
                <tr>
                  <th>Title</th>
                  <th width="65%">Link URL</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $i = 1; 
                  foreach( $rows as $row ):
                ?>
                <tr class="odd gradeX">
                  <td>
                    <div><?php echo $row['service_taxonomy']; ?></div>
                    <div class="row-actions">
                      <a href="<?php echo base_url('admin/services/edit/'.$row['id_service']); ?>" class="link-no-border">
                        <button type="button" class="btn btn-warning btn-xs">Edit</button>
                      </a>
                      <a href="<?php echo base_url('admin/services/delete/'.$row['id_service']); ?>" class="link-no-border delete">
                        <button type="button" class="btn btn-danger btn-xs">Delete</button>
                      </a>
                    </div><!-- /.row-actions -->
                  </td>
                  <td><?php echo $row['service_url']; ?></td>
                </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div><!-- /.table-responsive -->
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->