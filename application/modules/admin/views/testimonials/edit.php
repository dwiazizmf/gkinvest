<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Testimonials</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-4">
      <h4>Edit Testimonial</h4>
      <form class="" method="post" action="<?php echo base_url('admin/testimonials/put') ?>">
        <div class="form-group">
          <label>Name</label>
          <input type="text" class="form-control" name="testimoni_taxonomy" value="<?php echo $rows['testimoni_taxonomy']; ?>">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Email</label>
          <input type="text" class="form-control" name="testimoni_email" value="<?php echo $rows['testimoni_email']; ?>">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Company</label>
          <input type="text" class="form-control" name="testimoni_company" value="<?php echo $rows['testimoni_company']; ?>">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Testimoni</label>
          <textarea class="form-control" rows="3" name="testimoni_description"><?php echo $rows['testimoni_description'] ?></textarea>
        </div><!-- /.form-group -->
        <div class="form-group">                
          <input type="hidden" value="<?php echo $rows['id_testimoni']; ?>" name="id_testimoni" />
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="updatedAt">
        </div>
        <button class="btn btn-primary" type="submit"><i class="fa fa-pencil margin-right-15px"></i> Edit Testimoni</button>
      </form>
    </div><!-- /.col-lg-4 --> 

    <div class="col-lg-8">
      <div class="panel panel-primary">
        <div class="panel-heading">
            List Categories Product Reward
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
          <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered" id="dataTables-example">
              <thead>
                <tr>
                  <th>Name</th>
                  <th width="65%">Description</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $i = 1; 
                  foreach( $lists as $list ):
                ?>
                <tr class="odd gradeX">
                  <td>
                    <div><?php echo $list['testimoni_taxonomy']; ?></div>
                    <div class="row-actions">
                      <a href="<?php echo base_url('admin/testimonials/edit/'.$list['id_testimoni']) ?>" class="link-no-border">
                        <button type="button" class="btn btn-warning btn-xs">Edit</button>
                      </a>
                      <a href="<?php echo base_url('admin/testimonials/delete/'.$list['id_testimoni']) ?>" class="link-no-border">
                        <button type="button" class="btn btn-danger btn-xs">Delete</button>
                      </a>
                    </div><!-- /.row-actions -->
                  </td>
                  <td><?php echo $list['testimoni_description']; ?></td>
                </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div><!-- /.table-responsive -->
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->