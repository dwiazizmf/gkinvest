<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Testimonials</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-4">
      <h4>Add Testimoni</h4>
      <form class="" method="post" action="<?php echo base_url('admin/testimonials/post') ?>">
        <div class="form-group">
          <label>Name</label>
          <input type="text" class="form-control" name="testimoni_taxonomy" required="required">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Email</label>
          <input class="form-control" name="testimoni_email">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Company</label>
          <input class="form-control" name="testimoni_company">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Testimoni</label>
          <textarea class="form-control" rows="3" name="testimoni_description" required="required"></textarea>
        </div><!-- /.form-group -->
        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="createdAt">
        </div>
        <button class="btn btn-primary" type="submit">
          <i class="fa fa-plus margin-right-15px"></i> Add New Testimoni
        </button>
      </form>
    </div><!-- /.col-lg-4 --> 

    <div class="col-lg-8">
      <div class="panel panel-primary">
        <div class="panel-heading">
            List Testimonials
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
          <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered" id="dataTables-example">
              <thead>
                <tr>
                  <th>Name</th>
                  <th width="65%">Testimoni</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $i = 1; 
                  foreach( $rows as $row ):
                ?>
                <tr class="odd gradeX">
                  <td>
                    <div>
                      <!-- Name -->
                      <?php echo $row['testimoni_taxonomy']; ?><br>
                      
                      <!-- Company -->
                      <?php if($row['testimoni_company'] != null): ?>
                      <strong>
                        <?php echo $row['testimoni_company']; ?>
                      </strong><br>
                      <?php endif; ?>

                      <!-- Email -->
                      <?php if($row['testimoni_email'] != null): ?>
                      <small class="text-primary"><?php echo $row['testimoni_email']; ?></small>
                      <?php endif; ?>
                    </div>

                    <!-- Action -->
                    <div class="row-actions margin-top-10px">
                      <a href="<?php echo base_url('admin/testimonials/edit/'.$row['id_testimoni']) ?>" class="link-no-border">
                        <button type="button" class="btn btn-warning btn-xs">Edit</button>
                      </a>
                      <a href="<?php echo base_url('admin/testimonials/delete/'.$row['id_testimoni']) ?>" class="link-no-border delete">
                        <button type="button" class="btn btn-danger btn-xs">Delete</button>
                      </a>
                    </div><!-- /.row-actions -->
                  </td>
                  <td><?php echo $row['testimoni_description']; ?></td>
                </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div><!-- /.table-responsive -->
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->