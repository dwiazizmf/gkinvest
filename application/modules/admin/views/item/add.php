<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Item</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
    
  <div class="row">
    <form action="<?php echo base_url('admin/items/post') ?>" method="post" enctype="multipart/form-data">
      <div class="col-lg-8">
        <div class="form-group">
          <label>Item Name</label>
          <input  type="text" 
                  class="form-control" 
                  name="  item_title" 
                  placeholder="Add Title" 
                  required="required"
                  autocomplete="off">
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Description</label>         
          <textarea id="summernote" name="item_content"><p>Describe your description item here...</p></textarea>
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Excerpt</label>        

          <textarea class="form-control" rows="3" name="item_excerpt" placeholder="Describe your excerpt item here..."></textarea>
        </div><!-- /.form-group -->
      </div><!-- /.col-lg-8 --> 

      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Status
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <label>Choose One</label>
              <select class="selectpicker form-control" data-live-search="true" name="item_status">
                <option value="1">Publish</option>
                <option value="0">Draft</option>
              </select>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-primary -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Categories
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <label>Choose One</label>
              <select class="selectpicker form-control" data-live-search="true" name="id_category_item">
                <?php foreach($category_item as $row): ?>
                <option value="<?php echo $row['id_category_item'] ?>">
                  <?php echo $row['id_category_item'].' - '.$row['category_item_taxonomy'] ?>
                </option>
                <?php endforeach; ?>
              </select>

              <button type="button" 
                      class="btn btn-success btn-xs margin-top-10px"
                      data-trigger="modal"
                      data-toggle="modal"
                      data-target="#mod-fall-tambah-jenis-barang">Add New Categories</button>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-primary -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Featured Image
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="row">
              <div class="result-image"></div>
            </div>

            <input class="form-control margin-bottom-5px" name="file" id="files" type="file" multiple="multiple">

            <small class="form-text text-muted margin-top-30px">Tekan tombol Ctrl untuk memilih lebih dari satu gambar</small>  
          </div><!-- /.panel-body -->
        </div><!-- panel panel-primary -->

        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s"); ?>" name="createdAt">
          <input type="hidden" value="<?php echo $this->session->userdata('id'); ?>" name="id_user">
        </div><!-- /.form-group -->

        <button class="btn btn-primary pull-right" type="submit">
          <i class="fa fa-plus-circle margin-right-10px"></i> Publish
        </button>
      </div><!-- /.col-lg-4 --> 
    </form>      
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->

<div class="nifty-modal fall" id="mod-fall-tambah-jenis-barang">
  <div class="md-content">
    <div class="md-title">
      <h4>Add New Category</h4>
    </div>
    <div class="md-body">
      <form class="" method="post" action="<?php echo base_url('admin/barang/postJenisBarang') ?>">
        <div class="form-group">
          <label>Name</label>
          <input type="text" class="form-control" name="nama_jenis_barang" placeholder="Masukkan nama jenis barang" required="required">
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Description</label>
          <textarea class="form-control" rows="3" name="deskripsi_jenis_barang"></textarea>
        </div><!-- /.form-group -->

        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="createdAt">
        </div><!-- /.form-group -->

        <div class="pull-right">
          <button class="btn btn-danger modal-close" type="button" data-dismiss="modal">
            <i class="fa fa-close margin-right-10px"></i> Batal
          </button>
          
          <button class="btn btn-primary" type="submit">
            <i class="fa fa-plus-circle margin-right-10px"></i> Add New Category
          </button>
        </div><!-- /.pull-right -->
      </form>
    </div><!-- /.md-body -->
    <div class="md-footer"></div>
  </div><!-- /.md-content text-center -->
</div><!-- /.nifty-modal slide-in-right -->