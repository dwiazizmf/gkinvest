<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Daily Check List</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
            List Categories Item
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
          <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered" id="dataTables-example">
              <thead>
                <tr>
                  <th>ID Checklist</th>
                  <th>User</th>
                  <th>Created</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $i = 1; 
                  foreach($rows as $row):
                ?>
                <tr class="odd gradeX">
                  <td>
                    <?php echo $row['id_gen_checklist']; ?>
                    <br>
                    <small>
                      <a href="<?php echo base_url('admin/dailychecklist/detail/'.$row['id_gen_checklist']); ?>" class="link-no-border text-success">Detail</a>
                    </small>
                  </td>
                  <td><?php echo $row['username']; ?></td>
                  <td><?php echo tcrPrettyDate($row['createdAt']); ?></td>
                </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div><!-- /.table-responsive -->
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->