<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Daily Check List</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Form Daily Check Facilities
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
          <?php
          /*
          <table class="table table-responsive">
            <thead>
              <tr>
                <th width="15%">Day / Date</th>
                <th><?php echo $rows['createdAt']; ?></th>
              </tr>
              <tr>
                <th width="15%">Staff Pelaksana</th>
                <th><?php echo $rows['username']; ?></th>
              </tr>
            </thead>
          </table>
          */
          ?>

          <table class="table table-responsive">
            <thead>
              <tr>
                <th>No</th>
                <th>Items</th>
                <th>Qty</th>
                <th>Ada</th>
                <th>Tidak Ada</th>
                <th>Berfungsi</th>
                <th>Tidak Berfungsi</th>
                <th>Bersih</th>
                <th>Kotor</th>
                <th>Keterangan</th>
                <th>Status</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; foreach ($rows as $row) : ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $row['item_title']; ?></td>
                <td></td>
                <td><?php if(explode(',', $row['condition'])[0] == 'Ada') echo "Ada"; ?></td>
                <td><?php if(explode(',', $row['condition'])[0] == 'Tidak Ada') echo "Tidak Ada"; ?></td>
                <td><?php if(explode(', ', $row['condition'])[1] == 'Berfungsi') echo "Berfungsi"; ?></td>
                <td><?php if(explode(', ', $row['condition'])[1] == 'Tidak Berfungsi') echo "Tidak Berfungsi"; ?></td>
                <td><?php if(explode(', ', $row['condition'])[2] == 'Bersih') echo "Bersih"; ?></td>
                <td><?php if(explode(', ', $row['condition'])[2] == 'Kotor') echo "Kotor"; ?></td>
                <td><?php echo $row['information']; ?></td>
                <td><?php echo $row['status']; ?></td>
                <td>
                  <button class="btn btn-xs btn-warning"
                          data-trigger="modal"
                          data-toggle="modal"
                          data-target="#mod-fall-<?php echo $row['id_checklist'] ?>">Edit</button>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->

<?php foreach( $rows as $row ): ?>
<div class="nifty-modal fall" id="mod-fall-<?php echo $row['id_checklist']; ?>">
  <div class="md-content">
    <div class="md-title">
      <h4><?php echo $row['item_title']; ?></h4>
    </div>

    <div class="md-body">
      <div class="row margin-bottom-30px">
        <div class="col-md-4">
          <button class="btn btn-xs btn-danger"><?php echo $row['status']; ?></button>
        </div><!-- /.col-1 -->

        <div class="col-md-8 text-right">
          <strong>
            <small><?php echo tcrPrettyDate($row['createdAt']); ?></small>
          </strong>
        </div><!-- /.col-11 -->
      </div><!-- /.row -->

      <div class="row">
        <div class="col-md-12">
          <form method="post" action="<?php echo base_url('admin/dailychecklist/put'); ?>">
            <div class="form-group">
              <label>Status</label>
              <input  type="hidden" 
                      class="form-control" 
                      name="id_checklist" 
                      value="<?php echo $row['id_checklist']; ?>">
              <select class="selectpicker form-control" data-live-search="true" name="status">
                <option value="On Progress">On Progress</option>
                <option value="Done">Done</option>
                <option value="Pending">Pending</option>
                <option value="Cancel">Cancel</option>
              </select>
            </div><!-- /.form-group -->

            <div class="form-group">
              <label>Keterangan</label>         
              <textarea class="form-control" rows="10" name="information"></textarea>
            </div><!-- /.form-group -->

            <button class="btn btn-warning modal-close pull-right" 
                    type="button" 
                    data-dismiss="modal">Close</button>

            <button class="btn btn-primary pull-right margin-right-10px" 
                    type="submit">Save</button>
          </form>
        </div><!-- /.col-md-12 -->
      </div><!-- /.row -->
    </div><!-- /.md-body -->

    <div class="md-footer"></div>
  </div><!-- /.md-content text-center -->
</div><!-- /.nifty-modal slide-in-right -->
<?php endforeach ?>