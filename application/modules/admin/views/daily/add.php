<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Item</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
    
  <div class="row">
    <div class="col-lg-12">
      <form action="<?php echo base_url('admin/dailychecklist/post') ?>" method="post" enctype="multipart/form-data">
      <?php
        $priorGroup = "";
        $i = 0; 
        $number = 10;
        $id_gen_checklist = tcrGetId($number);
        foreach($items AS $entry):
          if($entry["category_item_taxonomy"] != $priorGroup):
      ?>
        <div class="form-group">
          <label class="text-danger"><?php echo $entry["category_item_taxonomy"]; ?></label>
        </div><!-- /.form-group -->
      <?php
          endif; // check category_item_taxonomy = priorGroup
      ?>
        <div class="form-group">
          <div class="row">
            <input type="hidden" name="id_gen_checklist[]" value="<?php echo $id_gen_checklist; ?>" class="form-control">
          </div><!-- /.row -->

          <div class="row">
            <label class="col-sm-12 control-label text-primary"><?php echo $entry["item_title"]; ?></label>
            <input type="hidden" name="id_item[]" value="<?php echo $entry["id_item"]; ?>" class="form-control">
          </div><!-- /.row -->

          <div class="row">
            <label class="col-sm-2 control-label">Condition</label>

            <div class="col-sm-10">
              <div class="form-check">
                <label>Ada</label>

                <br>
                
                <div class="margin-bottom-5px">
                  <input type="checkbox" name="condition<?php echo $i; ?>[]" value="Ada"> Ada
                  <input type="checkbox" name="condition<?php echo $i; ?>[]" value="Tidak Ada"> Tidak Ada
                </div>
              </div>
              <div class="form-check">
                <label>Berfungsi</label>

                <br>
                
                <div class="margin-bottom-5px">
                  <input type="checkbox" name="condition<?php echo $i; ?>[]" value="Berfungsi"> Berfungsi
                  <input type="checkbox" name="condition<?php echo $i; ?>[]" value="Tidak Berfungsi"> Tidak Berfungsi
                </div>
              </div>
              <div class="form-check">
                <label>Bersih</label>

                <br>

                <input type="checkbox" name="condition<?php echo $i; ?>[]" value="Bersih"> Bersih
                <input type="checkbox" name="condition<?php echo $i; ?>[]" value="Kotor"> Kotor
              </div>
            </div><!-- /.col-sm-10 -->
          </div><!-- /.row -->

          <div class="row margin-top-5px">
            <label class="col-sm-2 control-label margin-top-5px">Remarks</label>

            <div class="col-sm-10">
              <textarea class="form-control" name="information[]"></textarea>
            </div><!-- /.col-sm-10 -->
          </div><!-- /.row -->

          <div class="row margin-top-5px">
            <label class="col-sm-2 control-label margin-top-5px">Status</label>

            <div class="col-sm-10">
              <select name="status[]" class="form-control">
                <option>-- Please Choose One --</option>
                <option value="Waiting">Waiting</option>
                <option value="Process">Process</option>
                <option value="Complete">Complete</option>
              </select>
            </div><!-- /.col-sm-10 -->
          </div><!-- /.row -->
        </div><!-- /.form-group -->
      <?php
          $i++;
          $priorGroup = $entry["category_item_taxonomy"]; //update priorGroup.
        endforeach; // looping items
      ?>

        <button class="btn btn-primary pull-right" type="submit">
          <i class="fa fa-plus-circle margin-right-10px"></i> Publish
        </button>
      </form>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->