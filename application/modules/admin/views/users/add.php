<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Users</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
    
  <div class="row">
    <?php echo validation_errors(); ?>
    <?php echo form_open_multipart('admin/users/post'); ?>
    <form class="" method="post" action="">
      <div class="col-lg-8">
        <div class="alert alert-info">
          <label>Create a brand new user and add them to this site.</label>
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>ID Number</label>
          <input  type="text" 
                  class="form-control" 
                  name="id_user" 
                  autocomplete="off" 
                  required>
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Full of Name</label>
          <input  type="text" 
                  class="form-control" 
                  name="fullname" 
                  autocomplete="off"
                  required>
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Username</label>
          <input  type="text" 
                  class="form-control" 
                  name="username" 
                  autocomplete="off">
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Email</label>
          <input type="email" class="form-control" name="email" placeholder="Masukkan email pengguna aplikasi" required>
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Department</label>
          <select class="selectpicker form-control" data-live-search="true" name="departemen">
            <option data-tokens="engineering" value="Engineering">Engineering</option>
            <option data-tokens="accounting" value="Accounting">Accounting</option>
            <option data-tokens="bod" value="BOD">BOD</option>
            <option data-tokens="finance" value="Finance">Finance</option>
            <option data-tokens="hr ga whs" value="HR, GA and Warehouse">HR, GA and Warehouse</option>
            <option data-tokens="it" value="IT">IT</option>
            <option data-tokens="leasing" value="Leasing">Leasing</option>
            <option data-tokens="legal" value="Legal">Legal</option>
            <option data-tokens="marcomm" value="Marcomm">Marcomm</option>
            <option data-tokens="operation" value="Operation">Operation</option>
            <option data-tokens="purchasing" value="Purchasing">Purchasing</option>
            <option data-tokens="tcr" value="TCR">TCR</option>
          </select>
        </div><!-- /.form-group -->

        <div class="form-group">
          <input  type="hidden" 
                  class="ecom-datepicker form-control" 
                  name="tanggal_lahir" 
                  placeholder="Date of Birth"
                  data-date-format="yyyy/mm/dd">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input  type="hidden" 
                  class="form-control" 
                  name="alamat" 
                  placeholder="Address">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input type="hidden" class="form-control" name="kode_pos" value="0" placeholder="Post Code">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input type="hidden" class="form-control" name="pendidikan_terakhir" placeholder="Masukkan pendidikan terakhir">
        </div><!-- /.form-group -->
      </div><!-- /.col-lg-8 -->

      <div class="col-md-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Profile Photo
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group" id="uploadForm">
              <label class="btn btn-primary">
                Browse&hellip; <input type="file" name="file" id="file" style="display: none;">
              </label>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-primary -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Password
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <label>Password</label>
              <input  type="password" 
                      class="ecom-password-hideshow form-control" 
                      name="password" 
                      placeholder="New Password" required>
            </div><!-- /.form-group -->                

            <div class="form-group">
              <label>Confirm Password</label>
              <input  type="password" 
                      class="ecom-password-hideshow form-control" 
                      name="confirm_password" 
                      placeholder="Konfirmasi kata sandi" required>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-primary -->

        <div class="panel panel-red">
          <div class="panel-heading">
            Status
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <label>Role User</label>
              
            <div class="clearfix"></div>

            <input type="radio" name="id_role_user" value="1" required="required">
            Super Admin

            <div class="clearfix"></div>

            <input type="radio" name="id_role_user" value="2" required="required">
            Business Development

            <div class="clearfix"></div>
          
          
            <input type="radio" name="id_role_user" value="3" required="required">
            Digital Marketing

            <div class="clearfix"></div>
          
          
            <input type="radio" name="id_role_user" value="4" required="required">
            User GKinvest

            <div class="clearfix"></div>
            
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-primary -->

        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="createdAt">
        </div><!-- /.form-group -->

        <button class="btn btn-primary pull-right" type="submit">
          <i class="fa fa-plus-circle margin-right-10px"></i> Add New User
        </button>
      </div><!-- /.col-md-3 -->
    </form>      
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->