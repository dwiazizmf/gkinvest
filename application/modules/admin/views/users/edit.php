<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Users</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
    
  <div class="row">
    <form class="" method="post" action="<?php echo base_url('admin/users/put') ?>" enctype="multipart/form-data">
      <div class="col-lg-8">
        <div class="alert alert-info">
          <label>Create a brand new user and add them to this site.</label>
        </div><!-- /.form-group -->

        <div class="form-group">
          <input type="hidden" value="<?php echo $rows['id_user']; ?>" name="id_user" />
        </div>

        <div class="form-group">
          <label>Fullname</label>
          <input  type="text" 
                  class="form-control" 
                  name="fullname"
                  value = "<?php echo $rows['fullname'] ?>" 
                  required>
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Username</label>
          <input  type="text" 
                  class="form-control" 
                  name="username" 
                  value="<?php echo $rows['username'] ?>">
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Email</label>
          <input  type="email" 
                  class="form-control" 
                  name="email" 
                  value="<?php echo $rows['email'] ?>" 
                  required>
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Departemen</label>
          <select class="selectpicker form-control" data-live-search="true" name="departemen">
            <option value="<?php echo $rows['departemen'] ?>" selected="selected"><?php echo $rows['departemen'] ?></option>
            <option data-tokens="engineering" value="Engineering">Engineering</option>
            <option data-tokens="accounting" value="Accounting">Accounting</option>
            <option data-tokens="bod" value="BOD">BOD</option>
            <option data-tokens="finance" value="Finance">Finance</option>
            <option data-tokens="hr ga whs" value="HR, GA and Warehouse">HR, GA and Warehouse</option>
            <option data-tokens="it" value="IT">IT</option>
            <option data-tokens="leasing" value="Leasing">Leasing</option>
            <option data-tokens="legal" value="Legal">Legal</option>
            <option data-tokens="marcomm" value="Marcomm">Marcomm</option>
            <option data-tokens="operation" value="Operation">Operation</option>
            <option data-tokens="purchasing" value="Purchasing">Purchasing</option>
            <option data-tokens="tcr" value="TCR">TCR</option>
          </select>
        </div><!-- /.form-group -->

        <div class="form-group">
          <input  type="hidden" 
                  class="ecom-datepicker form-control" 
                  name="tanggal_lahir" 
                  placeholder="Date of Birth"
                  data-date-format="yyyy/mm/dd"
                  value="<?php echo $rows['tanggal_lahir'] ?>">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input  type="hidden" 
                  class="form-control" 
                  name="alamat" 
                  placeholder="Address"
                  value="<?php echo $rows['alamat'] ?>">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input type="hidden" class="form-control" name="kode_pos" value="<?php echo $rows['kode_pos'] ?>">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input type="hidden" class="form-control" name="pendidikan_terakhir" value="<?php echo $rows['pendidikan_terakhir'] ?>">
        </div><!-- /.form-group -->
      </div><!-- /.col-lg-8 -->

      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Photo Profile
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group" id="uploadForm">
              <?php if($rows['photo'] == NULL) : ?>
              <label class="btn btn-primary">
                Browse&hellip; <input type="file" name="file" id="file" style="display: none;">
              </label>
              <?php else : ?>
              <div class="col-lg-12 text-center">
                <img class="img-circle" width="180" height="180" src="<?php echo base_url('upload/be/pengguna/'.$rows['photo']) ?>" alt="">
              </div><!-- /.col-lg-12 -->
              <?php endif; ?>  
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-primary -->

        <div class="panel panel-red">
          <div class="panel-heading">
            Status
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <label>Role User</label>
              
            <div class="clearfix"></div>

            <input  type="radio" 
                    name="id_role_user" 
                    value="1" 
                    <?php if($rows['id_role_user'] == 1) echo 'checked="checked"'; ?>
                    required="required">
            Super Admin

            <div class="clearfix"></div>

            <input  type="radio" 
                    name="id_role_user" 
                    value="2" 
                    <?php if($rows['id_role_user'] == 2) echo 'checked="checked"'; ?>
                    required="required">
            Business Development

            <div class="clearfix"></div>
          
            <input  type="radio" 
                    name="id_role_user" 
                    value="3" 
                    <?php if($rows['id_role_user'] == 3) echo 'checked="checked"'; ?>
                    required="required">
            Digital Marketing

            <div class="clearfix"></div>
          
            <input  type="radio" 
                    name="id_role_user" 
                    value="4" 
                    <?php if($rows['id_role_user'] == 4) echo 'checked="checked"'; ?>
                    required="required">
            User GKinvest

            <div class="clearfix"></div>
            
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-primary -->

        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="updatedAt">
        </div><!-- /.form-group -->
          
        <div class="form-group">
          <button class="btn btn-primary" type="submit">Edit</button>
        </div><!-- /.form-group -->
      </div><!-- /.col-lg-4 -->
    </form>      
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->