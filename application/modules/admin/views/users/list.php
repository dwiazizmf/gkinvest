<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Users</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Users
        </div><!-- /.panel-heading -->
        <div class="panel-body">
          <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered" id="dataTables-example">
              <thead>
                <tr>
                  <th>ID User</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $i = 1; 
                  foreach( $rows as $row ):
                  $status = $row['id_role_user'];
                ?>
                <tr class="odd gradeX">
                  <td><?php echo $row['id_user'] ?></td>
                  <td><?php echo $row['fullname'] ?></td>
                  <td><?php echo $row['email']; ?></td>
                  <td><?php echo $row['role_user']; ?></td>

                  <?php if($this->session->userdata('id') == $row['id_user']) : ?>
                  <td><button type="button" class="btn btn-success btn-xs">Anda</button></td>
                  <?php else: ?>
                  <td>
                    <button class="btn btn-primary btn-xs" 
                            data-trigger="modal"
                            data-toggle="modal"
                            data-target="#mod-slide-fall-in-<?php echo $row['id_user'] ?>">Detail</button>
                    <a href="<?php echo base_url('admin/users/edit/'.$row['id_user']) ?>" class="link-no-border">
                      <button type="button" class="btn btn-warning btn-xs">Ubah</button>
                    </a>
                    <a href="<?php echo base_url('admin/users/delete/'.$row['id_user']) ?>" class="link-no-border delete">
                      <button type="button" class="btn btn-danger btn-xs">Hapus</button>
                    </a>
                  </td>
                  <?php endif; ?>
                </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div><!-- /.dataTable_wrapper -->
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->