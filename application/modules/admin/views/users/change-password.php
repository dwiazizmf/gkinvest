<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Users</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <form method="post" action="<?php echo base_url('admin/users/put') ?>">
    <div class="row">
      <div class="col-lg-12">
        <div class="form-group">
          <input type="hidden" value="<?php echo $user['id_user']; ?>" name="id_user" />
        </div>
      </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->

    <div class="row">
      <div class="col-lg-4">
        <h4>Password</h4>
        <p>This password is required as access to this system.</p>
      </div><!-- /.col-lg-4 -->

      <div class="col-lg-8">
        <div class="form-group">
          <label>New Password</label>
          <input  type="password" 
                  class="form-control" 
                  name="password"
                  placeholder="New Password" 
                  required="required">
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Confirm Password</label>
          <input  type="password" 
                  class="form-control" 
                  name="confirm_password" 
                  placeholder="Confirm Password"
                  required="required">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s"); ?>" name="updatedAt">
          <input type="hidden" value="<?php echo $this->session->userdata('status'); ?>" name="id_role_user">
        </div><!-- /.form-group -->
      </div><!-- /.col-lg-8 -->
    </div><!-- /.row -->

    <div class="row">
      <div class="col-lg-4">
        &nbsp;
      </div><!-- /.col-lg-4 -->

      <div class="col-lg-8">
        <button class="btn btn-primary" type="submit">Edit</button>
      </div><!-- /.col-lg-8 -->
    </div><!-- /.row -->
  </form>
</div><!-- /.page-wrapper -->