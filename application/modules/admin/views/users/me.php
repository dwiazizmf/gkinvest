<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Users</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <form class="" method="post" action="<?php echo base_url('admin/users/putProfile') ?>" enctype="multipart/form-data">
    <div class="row">
      <div class="col-lg-12">
        <div class="form-group">
          <input type="hidden" value="<?php echo $user['id_user']; ?>" name="id_user" />
        </div>
      </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->

    <div class="row">
      <div class="col-lg-4 profile-settings-sidebar">
        <h4>Photo Profile</h4>
        <p>You can change your avatar here</p>
      </div>
      <div class="col-lg-8">
        <div class="row">
          <?php if($user['photo'] == NULL) : ?>
          <div class="col-lg-6 result-image"></div><!-- /.col-lg-6 -->

          <div class="col-lg-6">
            <h5>Upload new photo</h5>

            <input class="form-control" name="file[]" id="files" type="file" multiple="" />

            <div class="form-text text-muted">The maximum file size allowed is 200KB.</div>  
          </div><!-- /.col-lg-6 -->

          <?php else : ?>
          <div class="col-lg-12">
            <img src="upload/be/users/<?php echo $user['photo'] ?>" alt="">
          </div><!-- /.col-lg-12 -->
          <?php endif; ?>    
        </div><!-- /.row -->
      </div><!-- /.col-lg-8 -->
    </div><!-- /.row -->

    <hr>

    <div class="row">
      <div class="col-lg-4">
        <h4>Information</h4>
      </div><!-- /.col-lg-4 -->

      <div class="col-lg-8">
        <div class="form-group">
          <label>Fullname</label>
          <input  type="text" 
                  class="form-control" 
                  name="fullname"
                  value = "<?php echo $user['fullname']; ?>"
                  required>
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Username</label>
          <input  type="text" 
                  class="form-control" 
                  name="username" 
                  value="<?php echo $user['username']; ?>">
        </div><!-- /.form-group -->

        <div class="form-group">
          <label>Email</label>
          <input  type="email" 
                  class="form-control" 
                  name="email" 
                  value="<?php echo $user['email']; ?>" 
                  required>
        </div><!-- /.form-group -->

        <div class="form-group">
          <input  type="hidden" 
                  class="ecom-datepicker form-control" 
                  name="tanggal_lahir" 
                  placeholder="Birth of date"
                  data-date-format="yyyy-mm-dd"
                  value="<?php echo $user['tanggal_lahir']; ?>">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input  type="hidden" 
                  class="form-control" 
                  placeholder="Address"
                  value="<?php echo $user['alamat']; ?>">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input  type="hidden" 
                  class="form-control" 
                  name="kode_pos" 
                  value="<?php echo $user['kode_pos']; ?>">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input  type="hidden" 
                  class="form-control" 
                  name="pendidikan_terakhir" 
                  value="<?php echo $user['pendidikan_terakhir']; ?>">
        </div><!-- /.form-group -->

        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s"); ?>" name="updatedAt">
          <input type="hidden" value="<?php echo $this->session->userdata('status'); ?>" name="status">
        </div><!-- /.form-group -->
      </div><!-- /.col-lg-8 -->
    </div><!-- /.row -->

    <div class="row">
      <div class="col-lg-4">
        &nbsp;
      </div><!-- /.col-lg-4 -->

      <div class="col-lg-8">
        <button class="btn btn-primary" type="submit">Edit</button>
      </div><!-- /.col-lg-8 -->
    </div><!-- /.row -->
  </form>
</div><!-- /.page-wrapper -->