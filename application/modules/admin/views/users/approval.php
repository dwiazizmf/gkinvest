<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Users</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Approval Users
        </div><!-- /.panel-heading -->
        <div class="panel-body">
          <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered" id="dataTables-example">
              <thead>
                <tr>
                  <th>ID User</th>
                  <th>Identity</th>
                  <th>Status</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $i = 1; 
                  foreach( $rows as $row ):
                  $status = $row['id_role_user'];
                ?>
                <tr class="odd gradeX">
                  <td><?php echo $row['id_user'] ?></td>
                  <td>
                    <?php echo $row['username'].'<br><small>'.$row['fullname'].'</small>'; ?>
                  </td>
                  <td><?php echo $row['role_user']; ?></td>

                  <?php if($this->session->userdata('id') == $row['id_user']) : ?>
                  <td><button type="button" class="btn btn-success btn-xs">Anda</button></td>
                  <?php else: ?>
                  <td>
                    <button class="btn btn-danger btn-xs" 
                            data-trigger="modal"
                            data-toggle="modal"
                            data-target="#mod-slide-fall-in-<?php echo $row['id_user'] ?>">Reject</button>

                    <form action="<?php echo base_url('admin/users/putForApprovalNewUser'); ?>" method="post" enctype="multipart/form-data" style="display: inline-block;">
                      <input type="hidden" name="id_user" value="<?php echo $row['id_user']; ?>">
                      <input type="hidden" name="id_role_user" value="<?php echo $row['id_role_user']; ?>">
                      <input type="hidden" name="fullname" value="<?php echo $row['fullname']; ?>">
                      <input type="hidden" name="username" value="<?php echo $row['username']; ?>">
                      <input type="hidden" name="email" value="<?php echo $row['email']; ?>">
                      <input type="hidden" name="departemen" value="<?php echo $row['departemen']; ?>">
                      <input type="hidden" name="tanggal_lahir" value="<?php echo $row['tanggal_lahir']; ?>">
                      <input type="hidden" name="alamat" value="<?php echo $row['alamat']; ?>">
                      <input type="hidden" name="kode_pos" value="<?php echo $row['kode_pos']; ?>">
                      <input type="hidden" name="pendidikan_terakhir" value="<?php echo $row['pendidikan_terakhir']; ?>">
                      <input type="hidden" name="photo" value="<?php echo $row['photo']; ?>">
                      <input type="hidden" name="status" value="1">
                      <input type="hidden" name="is_AB" value="0">
                      <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="updatedAt">
                      
                      <button type="submit" class="btn btn-primary btn-xs">Approve Non AB</button>
                    </form>

                    <form action="<?php echo base_url('admin/users/putForApprovalNewUser'); ?>" method="post" enctype="multipart/form-data" style="display: inline-block;">
                      <input type="hidden" name="id_user" value="<?php echo $row['id_user']; ?>">
                      <input type="hidden" name="id_role_user" value="<?php echo $row['id_role_user']; ?>">
                      <input type="hidden" name="fullname" value="<?php echo $row['fullname']; ?>">
                      <input type="hidden" name="username" value="<?php echo $row['username']; ?>">
                      <input type="hidden" name="email" value="<?php echo $row['email']; ?>">
                      <input type="hidden" name="departemen" value="<?php echo $row['departemen']; ?>">
                      <input type="hidden" name="tanggal_lahir" value="<?php echo $row['tanggal_lahir']; ?>">
                      <input type="hidden" name="alamat" value="<?php echo $row['alamat']; ?>">
                      <input type="hidden" name="kode_pos" value="<?php echo $row['kode_pos']; ?>">
                      <input type="hidden" name="pendidikan_terakhir" value="<?php echo $row['pendidikan_terakhir']; ?>">
                      <input type="hidden" name="photo" value="<?php echo $row['photo']; ?>">
                      <input type="hidden" name="status" value="1">
                      <input type="hidden" name="is_AB" value="1">
                      <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="updatedAt">
                      
                      <button type="submit" class="btn btn-primary btn-xs">Approve AB</button>
                    </form>
                  </td>
                  <?php endif; ?>
                </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div><!-- /.dataTable_wrapper -->
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->

<?php foreach( $rows as $row ): ?>
<div class="nifty-modal fall" id="mod-slide-fall-in-<?php echo $row['id_user']; ?>">
  <div class="md-content">
    <div class="md-title">
      <h4>Detail User</h4>
    </div>

    <div class="md-body">
      <form action="<?php echo base_url('admin/users/put'); ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id_user" value="<?php echo $row['id_user']; ?>">
        <input type="hidden" name="id_role_user" value="<?php echo $row['id_role_user']; ?>">
        <input type="hidden" name="fullname" value="<?php echo $row['fullname']; ?>">
        <input type="hidden" name="username" value="<?php echo $row['username']; ?>">
        <input type="hidden" name="email" value="<?php echo $row['email']; ?>">
        <input type="hidden" name="departemen" value="<?php echo $row['departemen']; ?>">
        <input type="hidden" name="tanggal_lahir" value="<?php echo $row['tanggal_lahir']; ?>">
        <input type="hidden" name="alamat" value="<?php echo $row['alamat']; ?>">
        <input type="hidden" name="kode_pos" value="<?php echo $row['kode_pos']; ?>">
        <input type="hidden" name="pendidikan_terakhir" value="<?php echo $row['pendidikan_terakhir']; ?>">
        <input type="hidden" name="photo" value="<?php echo $row['photo']; ?>">
        <input type="hidden" name="status" value="0">
        <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="updatedAt">


        <table class="table table-responsive">
          <tbody>
            <tr>
              <td><?php echo $row['id_user']; ?></td>
              <td><?php echo $row['fullname']; ?></td>
            </tr>
            <tr>
              <td>Remark</td>
              <td>
                <textarea class="form-control" name="remark"></textarea>
              </td>
            </tr>
          </tbody>
        </table>

        <button type="submit" class="btn btn-danger">Reject</button>
        <button class="btn btn-warning modal-close pull-right" type="button" data-dismiss="modal">Close</button>
      </form>
    </div><!-- /.md-body -->

    <div class="md-footer"></div>
  </div><!-- /.md-content text-center -->
</div><!-- /.nifty-modal slide-in-right -->
<?php endforeach ?>