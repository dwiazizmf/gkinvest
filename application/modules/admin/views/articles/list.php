<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Articles</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Articles
        </div><!-- /.panel-heading -->
        <div class="panel-body">
          <table class="table table-striped table-bordered" id="dataTables-example">
            <thead>
              <tr>
                <th>Item Name</th>                
                <th>Author</th>                
                <th>Categories</th>
                <th>Date</th>
              </tr>
            </thead>

            <tbody>
              <?php foreach($rows as $row): ?>
              <tr>
                <td>
                  <?php echo $row['post_title']; ?>
                  <div class="row-actions margin-top-5px">
                    <small>
                      <a href="<?php echo base_url('admin/articles/edit/'.$row['id_post']); ?>" class="link-no-border text-warning">Edit</a> |
                      <a href="<?php echo base_url('admin/articles/delete/'.$row['id_post']); ?>" class="link-no-border text-danger delete">Delete</a>
                    </small>
                  </div><!-- /.row-actions -->
                </td>
                <td><?php // echo $row['username']; ?></td>
                <td><?php echo $row['category_post']; ?></td>
                <td>
                  <?php 
                    echo $row['post_status'] = 1? 'Published' : 'Draft';
                    echo '<br>';
                    echo gkInvestPrettyDate($row['createdAt']); 
                  ?>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->

