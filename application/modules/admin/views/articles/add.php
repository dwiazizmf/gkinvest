<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Articles</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
    
  <div class="row">
    <form action="<?php echo base_url('admin/news/post'); ?>" method="post" enctype="multipart/form-data">
      <div class="col-lg-8">
        <div class="form-group">
          <input type="text" class="form-control" name="post_title" placeholder="Enter title here">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Description</label>
          <!-- <textarea class="form-control" rows="3" name="post_content" placeholder="Describe your description post here..."></textarea> -->

          <textarea id="summernote" name="post_content"><p>Describe your description post here...</p></textarea>
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Excerpt</label>
          <textarea class="form-control" rows="3" name="post_excerpt" placeholder="Describe your excerpt post here..."></textarea>
        </div><!-- /.form-group -->
      </div><!-- /.col-lg-8 -->

      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Publish
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <label>Status</label>
              
              <div class="clearfix"></div>

              <label class="radio-inline">
                <input type="radio" name="post_status" value="Draft">
                Draft
              </label>
              <label class="radio-inline">
                <input type="radio" name="post_status" value="Publish">
                Publish
              </label>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-default -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Categories
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <label>Categories</label>

              <?php foreach($categorynews as $listcategory): ?>
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="<?php echo $listcategory['category_post_taxonomy']; ?>" name="category_post[]">
                  <?php 
                    echo $listcategory['category_post_taxonomy']; 
                  ?>
                </label>
              </div><!-- /.checkbox -->
              <?php endforeach; ?>
            </div><!-- /.form-group -->    
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-body -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Tags
          </div><!-- /.panel-heading -->

          <div class="panel-body">       
            <div class="form-group">
              <label>Tags</label>

              <?php foreach($tagpost as $listtag): ?>
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="<?php echo $listtag['tag_taxonomy']; ?>" name="tag_post[]">
                  <?php 
                      echo $listtag['tag_taxonomy']; 
                  ?>
                </label>
              </div><!-- /.checkbox -->
              <?php endforeach; ?>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-body -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Featured Image
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group" id="uploadForm">
              <label class="btn btn-primary">
                Browse&hellip; <input type="file" name="file" id="file" style="display: none;">
              </label>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div>

        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="createdAt">
          <input type="hidden" value="<?php echo $this->session->userdata('id') ?>" name="id_user">
        </div><!-- /.form-group -->

        <button class="btn btn-primary" type="submit"><i class="fa fa-plus margin-right-15px"></i> Add New Post</button>
      </div><!-- /.col-lg-4 --> 
    </form>
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->