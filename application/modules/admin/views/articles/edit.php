<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Articles</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
    
  <div class="row">
    <form action="<?php echo base_url('admin/articles/put'); ?>" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <input type="hidden" value="<?php echo $rows['id_post']; ?>" name="id_post" />
      </div>
      <div class="col-lg-8">
        <div class="form-group">
          <input type="text" class="form-control" name="post_title" value="<?php echo $rows['post_title']; ?>" placeholder="Enter title here">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Description</label>
          <!-- <textarea class="form-control" rows="3" name="post_content" placeholder="Describe your description post here..."></textarea> -->

          <textarea id="summernote" name="post_content"><p><?php echo $rows['post_content']; ?></p></textarea>
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Excerpt</label>
          <textarea class="form-control" rows="3" name="post_excerpt" placeholder="Describe your excerpt post here..."><?php echo trim($rows['post_excerpt']); ?></textarea>
        </div><!-- /.form-group -->
      </div><!-- /.col-lg-8 -->

      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Publish
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <label>Status</label>
              
              <div class="clearfix"></div>

              <label class="radio-inline">
                <input type="radio" name="post_status" value="Draft" checked="<?php echo $rows['post_status']; ?>" >
                Draft
              </label>
              <label class="radio-inline">
                <input type="radio" name="post_status" value="Publish" checked="<?php echo $rows['post_status']; ?>" s>
                Publish
              </label>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-primary -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Categories
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <label>Categories</label>

              <?php 
                foreach($category_post as $list_post): 
                  // print each element as list item category post
                  $categoryPost = $rows['category_post'];
                  $arrayCategoryPost = explode(', ', $categoryPost);
              ?>
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="<?php echo $list_post['category_post_taxonomy']; ?>" name="category_post[]" <?php if(in_array($list_post['category_post_taxonomy'], $arrayCategoryPost)) echo 'checked="checked"'; ?>>
                  <?php echo $list_post['category_post_taxonomy']; ?>
                </label>
              </div><!-- /.checkbox -->
              <?php endforeach; ?>
            </div><!-- /.form-group -->    
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-primary -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Tags
          </div><!-- /.panel-heading -->

          <div class="panel-body">       
            <div class="form-group">
              <label>Tags</label>

              <?php 
                foreach($tag_post as $list_tag): 
                  // print each element as list item category post
                  $tagPost = $rows['tag_post'];
                  $arrayTagPost = explode(',', $tagPost);
              ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="<?php echo $list_tag['tag_taxonomy']; ?>" name="tag_post[]" <?php if(in_array($list_tag['tag_taxonomy'], $arrayTagPost)) { ?> checked="checked" <?php } ?>>
                    <?php echo $list_tag['tag_taxonomy']; ?>
                  </label>
                </div><!-- /.checkbox -->
              <?php endforeach; ?>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-primary -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Featured Image
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <input type="file" name="userfile">
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div>

        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="createdAt">
          <input type="hidden" value="<?php echo $this->session->userdata('id') ?>" name="id_user">
        </div><!-- /.form-group -->

        <button class="btn btn-primary" type="submit"><i class="fa fa-pencil margin-right-15px"></i> Update Post</button>
      </div><!-- /.col-lg-4 --> 
    </form>
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->