<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>GK Invest</title>

<!-- header-icon -->
<link rel="icon" type="image/png" sizes="16x16" href="http://kuningancity.com/wp-content/themes/kuncit/lib/img/favicons/favicon-16x16.png">

<link href="<?php echo base_url('assets/backend/vendor/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/backend/vendor/metisMenu/metisMenu.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/backend/vendor/datatables-plugins/dataTables.bootstrap.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/backend/vendor/datatables-responsive/dataTables.responsive.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/backend/vendor/dropzone/dropzone.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/backend/vendor/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/backend/vendor/dropzone/dropzone.min.css') ?>" rel="stylesheet" type="text/css">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/backend/vendor/hideshowpassword/css/example.wink.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/backend/vendor/modification/style.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/backend/dist/css/sb-admin-2.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/backend/bower_components/nifty-js/dist/nifty.min.css') ?>" rel="stylesheet" type="text/css" >
<link href="<?php echo base_url('assets/backend/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/backend/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css">
<style>
	.tb1 { font-family: arial; text-align: justify; padding-right: 10px; padding-left: 10px; }
	.swal-text { text-align: center; }
	.swal-footer { text-align: center; }
</style>


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

