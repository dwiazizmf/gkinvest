<script src="<?php echo base_url('assets/backend/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/vendor/datatables/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/vendor/datatables-plugins/dataTables.bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/vendor/datatables-responsive/dataTables.responsive.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/vendor/metisMenu/metisMenu.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/vendor/hideshowpassword/hideShowPassword.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/bower_components/nifty-js/dist/nifty.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/bower_components/bootstrap-select/dist/js/i18n/defaults-id_ID.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('assets/backend/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script src="<?php echo base_url('assets/backend/dist/js/sb-admin-2.js') ?>"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
function filePreview(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#uploadForm + img').remove();
      $('#uploadForm').after('<img src="'+e.target.result+'" class="img-responsive">');
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$("#file").change(function () {
  filePreview(this);
});

$('.datepicker').datepicker({
	format: 'yyyy-mm-dd'
});

/**
 * Confirmation data delete or not !
 */
$ ('.delete').on("click", function (e) {
  e.preventDefault ();
  var url = $ (this).attr('href');
  swal({
    title: "Yakin ingin menghapus?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      swal("Berhasil, data sudah dihapus!", {
        icon: "success"
      });
      setTimeout(
        function() {
          window.location.replace(url);
        }, 1500);  
    } else {
      swal("Hapus data dibatalkan");
    }
  });
});

var $rows = $('.table tr');
$('.search').keyup(function() {
  var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
  
  $rows.show().filter(function() {
    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
  }).hide();
});
</script>

