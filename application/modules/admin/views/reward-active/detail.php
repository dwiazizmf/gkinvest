<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Transaction History Reward</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Transaction History Reward
        </div><!-- /.panel-heading -->
        <div class="panel-body">
          <table class="table table-striped table-bordered" id="dataTables-example">
            <thead>
              <tr>
                <th>User</th>                 
                <th>Reward</th>
                <th>&nbsp;</th>
              </tr>
            </thead>

            <tbody>
              <?php foreach($rows as $row): ?>
              <tr>
                <td>
                  <?php echo $row['fullname']; ?>
                  <br>
                  <small class="text-primary">
                    Join Date - <?php echo gkinvestPrettyDate($row['join_date']); ?>
                  </small>
                </td>
                <td>
                  <?php echo $row['name_reward']; ?>
                  <br>
                  <small>
                    <?php echo $row['status_program'] = 'active'? 'Active' : 'Non Active'; ?>
                  </small>
                </td>
                <td>
                  <!-- Check if data has been approved -->
                  <?php if($row['id_status_reward_active'] == 7): ?>
                  <button class="btn btn-xs btn-success" type="button">Transaction Complete</button>
                  <?php elseif($row['id_status_reward_active'] == 8): ?>
                  <button class="btn btn-xs btn-danger" type="button">Rejected</button>
                  <?php elseif($row['id_status_reward_active'] == 6): ?>
                  <form action="<?php echo base_url('admin/reward_active/closeRewardActive'); ?>" method="post">
                    <input type="hidden" name="id_transaction_reward" value="<?php echo $row['id_transaction_reward']; ?>">
                    <input type="hidden" name="approved_by_id_user" value="<?php echo $this->session->userdata('id'); ?>">
                    <input type="hidden" name="id_status_reward_active" value="7">
                    <input type="hidden" name="approved_date" value="<?php echo date("Y-m-d H:i:s"); ?>">

                    <button type="submit" class="btn btn-danger btn-xs">Closed</button>
                  </form>
                  <?php elseif($row['id_status_reward_active'] == 1 || $row['id_status_reward_active'] == 2): ?>
                  
                  <?php else: ?>
                  <button class="btn btn-primary btn-xs" 
                            data-trigger="modal"
                            data-toggle="modal"
                            data-target="#mod-slide-fall-in-status-<?php echo $row['id_reward_active'] ?>">Change Status Claim</button>
                  <?php endif; ?>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->

<?php foreach( $rows as $row ): ?>
<div class="nifty-modal fall" id="mod-slide-fall-in-status-<?php echo $row['id_reward_active']; ?>">
  <div class="md-content">
    <div class="md-title">
      <h4>Change Status</h4>
    </div>

    <div class="md-body">
      <form action="<?php echo base_url('admin/reward_active/putForApprovalRewardActive'); ?>" method="post">
        <input type="hidden" name="id_reward_active" value="<?php echo $row['id_reward_active']; ?>">
        <input type="hidden" name="approved_by_id_user" value="<?php echo $this->session->userdata('id'); ?>">

        <label>Now Status <b class="text-danger"><?php echo $row['name_status_reward']; ?></b></label>
        <select class="form-control margin-bottom-15px" name="id_status_reward_active">
          <option value="1" <?php if($row['id_status_reward_active'] == 1) echo "selected"; ?>>Join Request</option>
          <option value="2" <?php if($row['id_status_reward_active'] == 2) echo "selected"; ?>>Join Approved</option>
          <option value="3" <?php if($row['id_status_reward_active'] == 3) echo "selected"; ?>>Claim Reward</option>
          <option value="4" <?php if($row['id_status_reward_active'] == 4) echo "selected"; ?>>Claim Process</option>
          <option value="5" <?php if($row['id_status_reward_active'] == 5) echo "selected"; ?>>Sent</option>
          <option value="6" <?php if($row['id_status_reward_active'] == 6) echo "selected"; ?>>Arrived</option>
          <option value="7" <?php if($row['id_status_reward_active'] == 7) echo "selected"; ?>>Closed</option>
        </select>
        
        <input type="hidden" name="approved_date" value="<?php echo date("Y-m-d H:i:s") ?>">

        <button type="submit" class="btn btn-danger">Change</button>
        <button class="btn btn-warning modal-close pull-right" type="button" data-dismiss="modal">Close</button>
      </form>
    </div><!-- /.md-body -->

    <div class="md-footer"></div>
  </div><!-- /.md-content text-center -->
</div><!-- /.nifty-modal slide-in-right -->
<?php endforeach ?>

<?php foreach( $rows as $row ): ?>
<div class="nifty-modal fall" id="mod-slide-fall-in-reject-<?php echo $row['id_reward_active']; ?>">
  <div class="md-content">
    <div class="md-title">
      <h4>Change Status</h4>
    </div>

    <div class="md-body">
      <form action="<?php echo base_url('admin/reward_active/putForApprovalRewardActive'); ?>" method="post">
        <input type="hidden" name="id_reward_active" value="<?php echo $row['id_reward_active']; ?>">
        <input type="hidden" name="approved_by_id_user" value="<?php echo $this->session->userdata('id'); ?>">
        <input type="hidden" name="id_status_reward_active" value="8">
        <input type="hidden" name="approved_date" value="<?php echo date("Y-m-d H:i:s") ?>">

        <table class="table table-responsive">
          <tbody>
            <tr>
              <td>Remark</td>
              <td>
                <textarea class="form-control" name="remark"></textarea>
              </td>
            </tr>
          </tbody>
        </table>

        <button type="submit" class="btn btn-danger">Reject</button>
        <button class="btn btn-warning modal-close pull-right" type="button" data-dismiss="modal">Close</button>
      </form>
    </div><!-- /.md-body -->

    <div class="md-footer"></div>
  </div><!-- /.md-content text-center -->
</div><!-- /.nifty-modal slide-in-right -->
<?php endforeach ?>



