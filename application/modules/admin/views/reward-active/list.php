<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Reward Active</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Reward Active
        </div><!-- /.panel-heading -->
        <div class="panel-body">
          <table class="table table-striped table-bordered" id="dataTables-example">
            <thead>
              <tr>
                <th>User</th>                 
                <th>Reward</th>
                <th>&nbsp;</th>
              </tr>
            </thead>

            <tbody>
              <?php foreach($rows as $row): ?>
              <tr>
                <td>
                  <?php echo $row['fullname']; ?>
                  <br>
                  <small class="text-primary">
                    Join Date - <?php echo gkinvestPrettyDate($row['join_date']); ?>
                  </small>
                </td>
                <td>
                  <?php echo $row['name_reward']; ?>
                  <br>
                  <small><?php echo $row['status_program'] = 'active'? 'Active' : 'Non Active';?></small>
                </td>
                <td>
                  <a  class="btn btn-xs btn-primary" 
                      href="<?php echo base_url('admin/reward_active/detail/'.$row['id_transaction_reward']); ?>" >Detail</a>

                  <?php if($row['id_status_reward_active'] == 6): ?>
                  <form action="<?php echo base_url('admin/reward_active/closeRewardActive'); ?>" method="post">
                    <input type="hidden" name="id_transaction_reward" value="<?php echo $row['id_transaction_reward']; ?>">
                    <input type="hidden" name="approved_by_id_user" value="<?php echo $this->session->userdata('id'); ?>">
                    <input type="hidden" name="id_status_reward_active" value="7">
                    <input type="hidden" name="approved_date" value="<?php echo date("Y-m-d H:i:s") ?>">

                    <button type="submit" class="btn btn-danger btn-xs">Close Reward</button>
                  </form>
                  <?php elseif($row['id_status_reward_active'] == 1): ?>
                  <button class="btn btn-danger btn-xs margin-left-5px" 
                          data-trigger="modal"
                          data-toggle="modal"
                          data-target="#mod-slide-fall-in-reject-<?php echo $row['id_reward_active'] ?>">Reject</button>

                  <form class="form-group row margin-left-5px" action="<?php echo base_url('admin/reward_active/putForApprovalRewardActive'); ?>" method="post">
                    <input type="hidden" name="id_reward_active" value="<?php echo $row['id_reward_active']; ?>">
                    <input type="hidden" name="approved_by_id_user" value="<?php echo $this->session->userdata('id'); ?>">
                    <input type="hidden" name="id_status_reward_active" value="2">
                    <input type="hidden" name="approved_date" value="<?php echo date("Y-m-d H:i:s") ?>">

                    <button type="submit" class="btn btn-primary btn-xs">Approved</button>
                  </form>
                  <?php elseif($row['id_status_reward_active'] == 2): ?>
                  <button class="btn btn-warning btn-xs margin-left-5px" 
                          data-trigger="modal"
                          data-toggle="modal"
                          data-target="#mod-slide-fall-in-edit-<?php echo $row['id_reward_active'] ?>">Update</button>
                  <?php elseif($row['id_status_reward_active'] == 7): ?>
                  <button class="btn btn-xs btn-success" type="button">Closed</button>
                  <?php elseif($row['id_status_reward_active'] == 8): ?>
                  <button class="btn btn-xs btn-danger" type="button">Rejected</button>
                  <?php endif; ?>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->

<?php foreach( $rows as $row ): ?>
<div class="nifty-modal fall" id="mod-slide-fall-in-reject-<?php echo $row['id_reward_active']; ?>">
  <div class="md-content">
    <div class="md-title">
      <h4>Change Status</h4>
    </div>

    <div class="md-body">
      <form action="<?php echo base_url('admin/reward_active/putForApprovalRewardActive'); ?>" method="post">
        <input type="hidden" name="id_reward_active" value="<?php echo $row['id_reward_active']; ?>">
        <input type="hidden" name="approved_by_id_user" value="<?php echo $this->session->userdata('id'); ?>">
        <input type="hidden" name="id_status_reward_active" value="8">
        <input type="hidden" name="approved_date" value="<?php echo date("Y-m-d H:i:s") ?>">

        <table class="table table-responsive">
          <tbody>
            <tr>
              <td>Remark</td>
              <td>
                <textarea class="form-control" name="remark"></textarea>
              </td>
            </tr>
          </tbody>
        </table>

        <button type="submit" class="btn btn-danger">Reject</button>
        <button class="btn btn-warning modal-close pull-right" type="button" data-dismiss="modal">Close</button>
      </form>
    </div><!-- /.md-body -->

    <div class="md-footer"></div>
  </div><!-- /.md-content text-center -->
</div><!-- /.nifty-modal slide-in-right -->
<?php endforeach ?>

<?php foreach( $rows as $row ): ?>
<div class="nifty-modal fall" id="mod-slide-fall-in-edit-<?php echo $row['id_reward_active']; ?>">
  <div class="md-content">
    <div class="md-title">
      <h4>Change Product Reward</h4>
    </div>

    <div class="md-body">
      <form action="<?php echo base_url('admin/reward_active/putProductRewardofRewardActive'); ?>" method="post">
        <input type="hidden" name="id_reward_active" value="<?php echo $row['id_reward_active']; ?>">
        
        <table class="table table-responsive">
          <tbody>
            <tr>
              <td>Product Rewards</td>
              <td>
                <select class="form-control" name="id_reward">
                  <?php foreach ($product as $rowProduct): ?>
                  <option value="<?php echo $rowProduct['id_reward']; ?>">
                    <?php echo $rowProduct['name_reward']; ?>
                  </option>
                  <?php endforeach; ?>
                </select>
              </td>
            </tr>
          </tbody>
        </table>

        <button type="submit" class="btn btn-primary">Update</button>
        <button class="btn btn-warning modal-close pull-right" type="button" data-dismiss="modal">Close</button>
      </form>
    </div><!-- /.md-body -->

    <div class="md-footer"></div>
  </div><!-- /.md-content text-center -->
</div><!-- /.nifty-modal slide-in-right -->
<?php endforeach ?>

