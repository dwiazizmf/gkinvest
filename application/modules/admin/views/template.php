<!DOCTYPE html>
<html lang="en">

<head>
  <?php echo $header; ?>
</head>

<body>
  <div id="wrapper">
    <!-- navigation -->
    <?php echo $menu; ?>
    <!-- end of navigation -->

    <!-- content-->
    <?php echo $content; ?>
    <!-- end content -->
  </div><!-- /#wrapper -->

  <!-- javascript -->
  <?php echo $js; ?>
  <!-- end of javascript -->

</body>

</html>
