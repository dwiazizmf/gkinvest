<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Product Rewards</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
    
  <div class="row">
    <form action="<?php echo base_url('admin/product_rewards/post'); ?>" method="post" enctype="multipart/form-data">
      <div class="col-lg-8">
        <div class="form-group">
          <input type="text" class="form-control" name="name_reward" placeholder="Enter title here">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Description</label>
          <!-- <textarea class="form-control" rows="3" name="detail_reward" placeholder="Describe your description post here..."></textarea> -->

          <textarea id="summernote" name="detail_reward"><p>Describe your description post here...</p></textarea>
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Minimum Deposit</label>
          <input type="text" class="form-control" name="minimum_deposit" placeholder="Enter minimum deposit here">
        </div><!-- /.form-group -->
        <div class="form-group">
          <label>Request Lot</label>
          <input type="text" class="form-control" name="request_lot" placeholder="Enter request lot here">
        </div><!-- /.form-group -->        
        <div class="form-group">
          <label>Quota Reward</label>
          <input type="text" class="form-control" name="quota_reward" placeholder="Enter quota reward here">
        </div><!-- /.form-group -->        
        <div class="form-group">
          <label>Start Reward</label>
          <input type="text" class="form-control datepicker" name="start_reward" placeholder="Enter start reward here">
        </div><!-- /.form-group -->        
        <div class="form-group">
          <label>End Reward</label>
          <input type="text" class="form-control datepicker" name="end_reward" placeholder="Enter end reward here">
        </div><!-- /.form-group -->
      </div><!-- /.col-lg-8 -->

      <div class="col-lg-4">
        <div class="panel panel-primary">
          <div class="panel-heading">
            Status Program
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <label>Status</label>
              
              <div class="clearfix"></div>

              <label class="radio-inline">
                <input type="radio" name="status_program" value="Active">
                Active
              </label>
              <label class="radio-inline">
                <input type="radio" name="status_program" value="Non Active">
                Non Active
              </label>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-default -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Status Product for Users
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <label>Status</label>
              
              <div class="clearfix"></div>

              <label class="radio-inline">
                <input type="radio" name="is_AB" value="1">
                AB
              </label>
              <label class="radio-inline">
                <input type="radio" name="is_AB" value="0">
                Non AB
              </label>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-default -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Categories
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group">
              <label>Categories</label>

              <select class="selectpicker form-control" data-live-search="true" name="id_category_reward">
                <?php foreach($categoryrewards as $listcategory): ?>
                <option value="<?php echo $listcategory['id_category_reward'] ?>">
                  <?php echo $listcategory['category_reward_taxonomy'] ?>
                </option> 
                <?php endforeach; ?>
              </select>
            </div><!-- /.form-group -->    
          </div><!-- /.panel-body -->
        </div><!-- /.panel .panel-body -->

        <div class="panel panel-primary">
          <div class="panel-heading">
            Featured Image
          </div><!-- /.panel-heading -->

          <div class="panel-body">
            <div class="form-group" id="uploadForm">
              <label class="btn btn-primary">
                Browse&hellip; <input type="file" name="file" id="file" style="display: none;">
              </label>
            </div><!-- /.form-group -->
          </div><!-- /.panel-body -->
        </div>

        <div class="form-group">
          <input type="hidden" value="<?php echo date("Y-m-d H:i:s") ?>" name="createdAt">
        </div><!-- /.form-group -->

        <button class="btn btn-primary" type="submit"><i class="fa fa-plus margin-right-15px"></i> Add New Product Reward</button>
      </div><!-- /.col-lg-4 --> 
    </form>
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->