<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12" style="margin-top: 15px">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
        <li class="breadcrumb-item" aria-current="page">Product Reward</li>
      </ol><!-- /.breadcrumb -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <?php $this->load->view('admin/notification') ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Product Reward
        </div><!-- /.panel-heading -->
        <div class="panel-body">
          <table class="table table-striped table-bordered" id="dataTables-example">
            <thead>
              <tr>
                <th>Item Name</th>                 
                <th>Categories</th>
                <th>Date</th>
              </tr>
            </thead>

            <tbody>
              <?php foreach($rows as $row): ?>
              <tr>
                <td>
                  <?php echo $row['name_reward']; ?>
                  <div class="row-actions margin-top-5px">
                    <small>
                      <a href="<?php echo base_url('admin/product_rewards/edit/'.$row['id_reward']); ?>" class="link-no-border text-warning">Edit</a> |
                      <a href="<?php echo base_url('admin/product_rewards/delete/'.$row['id_reward']); ?>" class="link-no-border text-danger delete">Delete</a>
                    </small>
                  </div><!-- /.row-actions -->
                </td>
                <td><?php echo $row['category_reward_taxonomy']; ?></td>
                <td>
                  <?php 
                    echo $row['status_program'] = 'active'? 'Active' : 'Non Active';
                    echo '<br>';
                    echo gkinvestPrettyDate($row['createdAt']); 
                  ?>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div><!-- /.col-lg-12 -->        
  </div><!-- /.row -->
</div><!-- /.page-wrapper -->

