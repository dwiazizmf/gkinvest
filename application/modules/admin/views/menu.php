<nav class="navbar navbar-ecom navbar-static-top" role="navigation" style="margin-bottom: 0">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?php echo base_url('/admin/home'); ?>">GK Invest</a>
  </div><!-- /.navbar-header -->

  <ul class="nav navbar-top-links navbar-right hidden-sm hidden-xs">      
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu dropdown-user">
        <li>
          <a href="<?php echo base_url("admin/users/profile/".$this->session->userdata('id')) ?>">
            <i class="fa fa-user fa-fw"></i> <?php echo $this->session->userdata('username') ?>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url("admin/users/changepassword/".$this->session->userdata('id')) ?>">
            <i class="fa fa-gear fa-fw"></i> Change Password
          </a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="<?php echo base_url("admin/login/logout") ?>">
            <i class="fa fa-sign-out fa-fw"></i> Logout
          </a>
        </li>
      </ul>
      <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
  </ul>
  <!-- /.navbar-top-links -->

  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <?php if($this->session->userdata('status') == 1): ?>
      <ul class="nav" id="side-menu">
        <li>
          <a href="<?php echo base_url('/admin/home'); ?>"><i class="fa fa-dashboard fa-fw text-danger"></i> Dashboard</a>
        </li>       
        <li>
          <a href="<?php echo base_url('/admin/category_news'); ?>"><i class="fa fa-clipboard fa-fw"></i> Categories</a>
        </li>       
        <li>
          <a href="<?php echo base_url('/admin/tags'); ?>"><i class="fa fa-hashtag fa-fw"></i> Tags</a>
        </li>
        <li>
          <a href="<?php echo base_url('/admin/services'); ?>"><i class="fa fa-link"></i> Social Media Configuration</a>
        </li>
        <li>
          <a href="#"><i class="fa fa-file-text-o fa-fw"></i> Article<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/articles') ?>">All Articles</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/articles/add') ?>">Add New</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="#"><i class="fa fa-newspaper-o fa-fw"></i> News<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/news') ?>">All News</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/news/add') ?>">Add New</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="#"><i class="fa fa-external-link-square fa-fw text-danger"></i> Page<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/items') ?>">All Pages</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/items/add') ?>">Add New</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/category_item'); ?>">Categories</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="#"><i class="fa fa-picture-o fa-fw text-danger"></i> Gallery<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/items') ?>">All Galleries</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/items/add') ?>">Add New</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/category_item'); ?>">Categories</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="#"><i class="fa fa-clone"></i> Product Reward<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/product_rewards') ?>">All Product Reward</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/product_rewards/add') ?>">Add New</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/category_rewards'); ?>">Categories</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="<?php echo base_url('/admin/reward_active'); ?>"><i class="fa fa-comments"></i> Rewards Active</a>
        </li>
        <li>
          <a href="<?php echo base_url('/admin/reward_active'); ?>"><i class="fa fa-comments"></i> Report Rewards</a>
        </li>
        <li>
          <a href="<?php echo base_url('/admin/testimonials'); ?>"><i class="fa fa-comments"></i> Testimonials</a>
        </li>
        <li>
          <a href="#"><i class="fa fa-users fa-fw"></i> User<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/users') ?>">All Users</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/users/add') ?>">Add New</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/role_users'); ?>">Roles</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/users/approval'); ?>">Approval</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="<?php echo base_url('/admin/upload_data'); ?>"><i class="fa fa-upload fa-fw"></i> Upload Data Transact</a>
        </li>
        <!-- End of GKINVEST's Super Admin Menus -->
      </ul>

      <?php elseif($this->session->userdata('status') == 2): ?>
      <ul class="nav" id="side-menu">
        <li>
          <a href="<?php echo base_url('/admin/home'); ?>"><i class="fa fa-dashboard fa-fw text-danger"></i> Dashboard</a>
        </li>
        <li>
          <a href="#"><i class="fa fa-clone"></i> Product Reward<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/product_rewards') ?>">All Product Reward</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/product_rewards/add') ?>">Add New</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/category_rewards'); ?>">Categories</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="<?php echo base_url('/admin/reward_active'); ?>"><i class="fa fa-comments"></i> Rewards Active</a>
        </li>
        <li>
          <a href="<?php echo base_url('/admin/reward_active'); ?>"><i class="fa fa-comments"></i> Report Rewards</a>
        </li>
        <li>
          <a href="#"><i class="fa fa-users fa-fw"></i> User<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/users') ?>">All Users</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/users/add') ?>">Add New</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/role_users'); ?>">Roles</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/users/approval'); ?>">Approval</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="<?php echo base_url('/admin/upload_data'); ?>"><i class="fa fa-upload fa-fw"></i> Upload Data Transact</a>
        </li>
        <!-- End of GKINVEST's Business Development Menus -->
      </ul>
      <?php endif; ?>

      <?php if($this->session->userdata('status') == 3): ?>
      <ul class="nav" id="side-menu">   
        <li>
          <a href="<?php echo base_url('/admin/category_news'); ?>"><i class="fa fa-clipboard fa-fw"></i> Categories</a>
        </li>       
        <li>
          <a href="<?php echo base_url('/admin/tags'); ?>"><i class="fa fa-hashtag fa-fw"></i> Tags</a>
        </li>
        <li>
          <a href="<?php echo base_url('/admin/services'); ?>"><i class="fa fa-link"></i> Social Media Configuration</a>
        </li>
        <li>
          <a href="#"><i class="fa fa-file-text-o fa-fw"></i> Article<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/articles') ?>">All Articles</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/articles/add') ?>">Add New</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="#"><i class="fa fa-newspaper-o fa-fw"></i> News<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/news') ?>">All News</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/news/add') ?>">Add New</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="#"><i class="fa fa-external-link-square fa-fw text-danger"></i> Page<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/items') ?>">All Pages</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/items/add') ?>">Add New</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/category_item'); ?>">Categories</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="#"><i class="fa fa-picture-o fa-fw text-danger"></i> Gallery<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/items') ?>">All Galleries</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/items/add') ?>">Add New</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/category_item'); ?>">Categories</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="#"><i class="fa fa-clone"></i> Product Reward<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="<?php echo base_url('/admin/product_rewards') ?>">All Product Reward</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/product_rewards/add') ?>">Add New</a>
            </li>
            <li>
              <a href="<?php echo base_url('/admin/category_rewards'); ?>">Categories</a>
            </li>
          </ul><!-- /.nav-second-level -->
        </li>
        <li>
          <a href="<?php echo base_url('/admin/testimonials'); ?>"><i class="fa fa-comments"></i> Testimonials</a>
        </li>
        <!-- End of GKINVEST's Super Admin Menus -->
      </ul>
      <?php endif; ?>
    </div><!-- /.sidebar-collapse -->
  </div><!-- /.navbar-static-side -->
</nav>