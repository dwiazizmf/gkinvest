<style type="text/css">
.loader {
    border: 5px solid #f3f3f3;
    /* Light grey */
    border-top: 5px solid #3498db;
    /* Blue */
    border-radius: 50%;
    width: 50px;
    height: 50px;
    animation: spin 1s linear infinite;
    margin-top: 10px;
}

@keyframes spin {
    0% {
        transform: rotate(0deg);
    }

    100% {
        transform: rotate(360deg);
    }
}
</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12" style="margin-top: 15px">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Data Transactions</li>
            </ol><!-- /.breadcrumb -->
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php $this->load->view('admin/notification') ?>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-red">
                <div class="panel-heading">
                    Upload Data Transaction
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <p>Save the transaction data to be uploaded in : "upload/be/transact" folder!</p>
                    <p>Choose one of following file below </p>
                    <form action="<?php echo base_url('admin/upload_data/start')?>" method="POST">
                        <select name="file_name" class="form-control" style="margin-bottom: 10px;" id="file_name">
                            <?php 
          		foreach ($list_file as $key => $value) { ?>
                            <option value="<?php echo $value;?>">
                                <?php echo $value;?>
                            </option>
                            <?php		}
          ?>
                        </select>
                        <?php if (count($list_file) > 0) {
            	//echo '<a href="'..'" class="link-no-border text-danger">';
            		echo '<button type="submit" onclick="showLoading()" class="btn btn-primary btn-xs">Start Upload</button>';
          		//echo "</a>";
            }else{
            	echo "<p>Tidak ada file yang bisa di upload</p>";
            } ?>
                    </form>
                    <div class="hidden" id="loading">
                        <div class="loader"></div>
                        <p><strong>Please wait until the upload process is successful</strong></p>
                    </div>
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.page-wrapper -->
<script type="text/javascript">
function showLoading() {
    $('#loading').removeClass('hidden');
}
</script>