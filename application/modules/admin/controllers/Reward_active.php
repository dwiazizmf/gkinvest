<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reward_active extends CI_Controller {
	function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper(array('form', 'url')); 
    $this->load->model(array('m_reward_active', 'm_product_rewards'));
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

  /**
   * get list or find of data
   * @param  {number}  id - id.
   * @param  {object}  params - query parameters.
   * @return {Promise}
   */
	public function index($offset = NULL) {
    $data = array();
    $data['rows']     = $this->m_reward_active->get(array('limit' => 12, 'offset' => $offset)); 
    $data['product']  = $this->m_product_rewards->get();

    $config['base_url'] = site_url('admin/reward_active/index');
    $config['total_rows'] = count($this->m_reward_active->get())+1;
    $config['per_page'] = 12;
    $config['uri_segment'] = 4;
    $this->pagination->initialize($config);

		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']    		    = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('reward-active/list', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
	}

  /**
   * get list detail or find of data
   * @param  {number}  id - id.
   * @param  {object}  params - query parameters.
   * @return {Promise}
   */
  public function detail($id, $offset = NULL) {
    $data = array();
    $data['rows'] = $this->m_reward_active->get(array('id' => $id, 'limit' => 12, 'offset' => $offset));

    $config['base_url'] = site_url('admin/reward_active/index');
    $config['total_rows'] = count($this->m_reward_active->get())+1;
    $config['per_page'] = 12;
    $config['uri_segment'] = 4;
    $this->pagination->initialize($config);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('reward-active/detail', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
  }

  public function putForApprovalRewardActive() {
    $id = $this->input->post('id_reward_active');
    $data = array('approved_by_id_user'     => $this->input->post('approved_by_id_user'),
                  'id_status_reward_active' => $this->input->post('id_status_reward_active'),
                  'remark'                  => $this->input->post('remark'),
                  'approved_date'           => $this->input->post('approved_date')
    );
    $this->m_reward_active->update( $id, $data );

    $this->session->set_flashdata('success', 'Congratulations, You have successfully approved reward active');
    redirect('admin/reward_active');
  }

  public function putProductRewardofRewardActive() {
    $id = $this->input->post('id_reward_active');
    $data = array('id_reward'     => $this->input->post('id_reward')
    );
    $this->m_reward_active->update( $id, $data );

    $this->session->set_flashdata('success', 'Congratulations, You have successfully approved reward active');
    redirect('admin/reward_active');
  }

  public function closeRewardActive() {
    $id = $this->input->post('id_transaction_reward');
    $data = array('approved_by_id_user'     => $this->input->post('approved_by_id_user'),
                  'id_status_reward_active' => $this->input->post('id_status_reward_active'),
                  'remark'                  => $this->input->post('remark'),
                  'approved_date'           => $this->input->post('approved_date')
    );
    $this->m_reward_active->closeReward( $id, $data );

    $this->session->set_flashdata('success', 'Congratulations, You have successfully closed reward active');
    redirect('admin/reward_active');
  }
}
