<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonials extends CI_Controller {
	function __construct(){
    parent::__construct();
    $this->load->library('session');
    $this->load->model('m_testimonials');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

	public function index() {
    $data = array();
    $data['msg']  = $this->_get_flashdata();
    $data['rows'] = $this->m_testimonials->get();

		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']    		    = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('testimonials/list',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
	}

  /**
   * [add description]
   */
  public function post(){
    $data = array(  'id_testimoni'          => $this->input->post('id_testimoni'),
                    'testimoni_taxonomy'    => $this->input->post('testimoni_taxonomy'),     
                    'testimoni_email'       => $this->input->post('testimoni_email'),       
                    'testimoni_company'     => $this->input->post('testimoni_company'),
                    'testimoni_description' => $this->input->post('testimoni_description'),
                    'createdAt'             => $this->input->post('createdAt')
                    
    );
    $this->m_testimonials->post( $data );
    redirect('admin/testimonials');
  }

  /**
   * [edit proccess]
   * @return [rewards] [Controller view edit tags and function of put tags ]
   */
  public function edit($id){
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['lists']      = $this->m_testimonials->get();
    $data['rows']       = $this->m_testimonials->getByID($id);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']           = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('testimonials/edit',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  public function put(){
    $id= $this->input->post('id_testimoni');
    $data = array(  'testimoni_taxonomy'      => $this->input->post('testimoni_taxonomy'),      
                    'testimoni_email'         => $this->input->post('testimoni_email'),      
                    'testimoni_company'       => $this->input->post('testimoni_company'),
                    'testimoni_description'   => $this->input->post('testimoni_description'),
                    'updatedAt'               => $this->input->post('updatedAt')
    );
    $this->m_testimonials->update($id, $data);

    $this->session->set_flashdata('success', 'Congratulations, You have successfully edited data testimoni from your list.');
    redirect('admin/testimonials');
  }

  /**
   * [delete description]
   */
  public function delete(){
    $id = $this->uri->segment(4); 
    $h  = $this->m_testimonials->delete($id); 
    if($h==1451)
      $this->session->set_flashdata('message', $this->message->message_error('Sorry You failed to remove testimoni from your list. Please try again!'));
    else
      $this->session->set_flashdata('success', 'Congratulations, You have successfully remove testimoni from your list.');
      redirect(base_url('admin/testimonials'));
  }

  private function _get_flashdata() {
    $msg = $this->session->flashdata("process_msg");
    if (empty($msg))
        return array("type" => "hidden", "content" => "");
    else
        return $msg;
  }
}
