<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->model('m_users');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

  /*
   * [GET] /users
   *    Get all users
   *    @author Rico Oktavian Adhi Wibowo
   */
	public function index() {
    $data = array();
    $data['rows']       = $this->m_users->get();
    // $data['count']      = count($this->m_permintaan_barang->get());

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']    		    = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('users/list',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
	}

  /*
   * [GET] /:detail/users
   *    Get all users
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function profile($id = NULL) {
    $data = array();
    $data['user']       = $this->m_users->get(array('id' => $id));
    // $data['count']      = count($this->m_permintaan_barang->get());

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('users/me',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  /*
   * [GET] /:detail/users
   *    Get all users
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function changepassword($id = NULL) {
    $data = array();
    $data['user']       = $this->m_users->get(array('id' => $id));
    // $data['count']      = count($this->m_permintaan_barang->get());

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('users/change-password',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  /*
   * [ADD] /user
   *    Add new user
   * @author Rico Oktavian Adhi Wibowo
   */
  public function add() {
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array();
    // $data['count']      = count($this->m_permintaan_barang->get());

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('users/add', null, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
  }

  public function post() { 
    $config['file_name'] = 'pict_'.date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/be/users';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    if($this->validation() == false){
      echo $this->upload->display_errors();
      $this->session->set_flashdata('error', 'Please try again, you failed to add new users.');
      redirect('admin/users/add');
    } else {
      if($_FILES['file']['name'] == "") {                                 
        $data = array('id_user'             => $this->input->post('id_user'),
                      'fullname'            => $this->input->post('fullname'),
                      'username'            => $this->input->post('username'),
                      'email'               => $this->input->post('email'),
                      'departemen'          => $this->input->post('departemen'),
                      'tanggal_lahir'       => $this->input->post('tanggal_lahir'),
                      'alamat'              => $this->input->post('alamat'),
                      'kode_pos'            => $this->input->post('kode_pos'),
                      'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
                      'password'            => gkinvestEncrypt($this->input->post('password')),
                      'id_role_user'        => $this->input->post('id_role_user'),
                      'createdAt'           => $this->input->post('createdAt')
        );
        $this->m_users->post( $data );
        $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
        redirect('admin/users');
      } else {
        if ($this->upload->do_upload('file')){
          $image = $this->upload->data(); 
          $data = array('id_user'             => $this->input->post('id_user'),
                        'fullname'            => $this->input->post('fullname'),
                        'username'            => $this->input->post('username'),
                        'email'               => $this->input->post('email'),
                        'departemen'          => $this->input->post('departemen'),
                        'tanggal_lahir'       => $this->input->post('tanggal_lahir'),
                        'alamat'              => $this->input->post('alamat'),
                        'kode_pos'            => $this->input->post('kode_pos'),
                        'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
                        'password'            => gkinvestEncrypt($this->input->post('password')),
                        'id_role_user'        => $this->input->post('id_role_user'),
                        'createdAt'           => $this->input->post('createdAt'),
                        'photo'               => $image['file_name']
          );
          // var_dump($data);
          $this->m_users->post( $data );
          $this->session->set_flashdata('success', 'Congratulations, You have successfully added new user');
          redirect('admin/users');
        } else {
          // var_dump($data);
          $this->session->set_flashdata('error', 'Please try again, you failed to add new user.');
          redirect('admin/users/add');
          echo $this->upload->display_errors();
        }
      }
    }
  }

  /*
   * [PATCH] /user
   *    Add new user
   * @author Rico Oktavian Adhi Wibowo
   */
  public function edit($id) {
    /**
     * [$data get data from database]
     * @var array
     */
    $data               = array($id);
    $data['rows']       = $this->m_users->getByID($id);
    // $data['count']      = count($this->m_permintaan_barang->get());

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('users/edit', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
  }

  public function put() { 
    $config['file_name'] = 'pict_'.date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/be/users';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    $id = $this->input->post('id_user');
    $password = $this->input->post('password');

    if($_FILES['file']['name'] == "" && empty($password)) {                                 
      $data = array('fullname'            => $this->input->post('fullname'),
                    'username'            => $this->input->post('username'),
                    'email'               => $this->input->post('email'),
                    'departemen'          => $this->input->post('departemen'),
                    'tanggal_lahir'       => $this->input->post('tanggal_lahir'),
                    'alamat'              => $this->input->post('alamat'),
                    'kode_pos'            => $this->input->post('kode_pos'),
                    'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
                    'status'              => $this->input->post('status'),
                    'remark'              => $this->input->post('remark'),
                    'id_role_user'        => $this->input->post('id_role_user'),
                    'updatedAt'           => $this->input->post('updatedAt')
      );
      $this->m_users->update( $id, $data );
      $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
      redirect('admin/users');
    } elseif($_FILES['file']['name'] != "" && empty($password)) {
      if ($this->upload->do_upload('file')){
        $image = $this->upload->data(); 
        $data = array('fullname'            => $this->input->post('fullname'),
                      'username'            => $this->input->post('username'),
                      'email'               => $this->input->post('email'),
                      'departemen'          => $this->input->post('departemen'),
                      'tanggal_lahir'       => $this->input->post('tanggal_lahir'),
                      'alamat'              => $this->input->post('alamat'),
                      'kode_pos'            => $this->input->post('kode_pos'),
                      'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
                      'status'              => $this->input->post('status'),
                      'remark'              => $this->input->post('remark'),
                      'id_role_user'        => $this->input->post('id_role_user'),
                      'updatedAt'           => $this->input->post('updatedAt'),
                      'photo'               => $image['file_name']
        );
        // var_dump($data);
        $this->m_users->update( $id, $data );
        $this->session->set_flashdata('success', 'Congratulations, You have successfully added new user');
        redirect('admin/users');
      } else {
        // var_dump($data);
        $this->session->set_flashdata('error', 'Please try again, you failed to add new user.');
        redirect('admin/users/add');
        echo $this->upload->display_errors();
      }
    } elseif($_FILES['file']['name'] == "" && !empty($password)) {
      $data = array(  'password'            => gkinvestEncrypt($this->input->post('password')),
                      'id_role_user'        => $this->input->post('id_role_user'),
                      'updatedAt'           => $this->input->post('updatedAt')
      );
      $this->m_users->update( $id, $data );
      $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
      redirect('admin/users');
    }
  }

  /*
   * [PATCH] /user
   *    Approval new user
   * @author Rico Oktavian Adhi Wibowo
   */
  public function approval() {
    /**
     * [$data get data from database]
     * @var array
     */
    $data['rows'] = $this->m_users->get(array('status' => 0));
    // $data['count']      = count($this->m_permintaan_barang->get());

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', $data, true);
    $html['content']        = $this->load->view('users/approval', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
  }

  public function putForApprovalNewUser() {
    $id = $this->input->post('id_user');
    $data = array('is_AB'     => $this->input->post('is_AB'),
                  'status'    => $this->input->post('status'),
                  'remark'    => $this->input->post('remark'),
                  'updatedAt' => $this->input->post('updatedAt')
    );
    $this->m_users->update( $id, $data );

    // $this->load->library('email');
    // $this->email->from('info@myreward.gkinvest.co.id');
    // $list = array($this->input->post('email'));
    // $this->email->to($list);
    // $this->email->subject('Confirm Your Registration');
    // $this->email->message('Thank you for creating a GKInvest account. Please visit the link below and sign into your account to verify your email address and complete your registration.');
    
    $this->load->library('email');
    $this->email->from('info@timediaconnection.com', 'GKINVEST.CO.ID');
    $list = array($this->input->post('email'));
    $this->email->to($list);
    $this->email->subject('Confirm Your Registration');
    $this->email->message('Thank you for creating a myreward.gkinvest.co.id account. You are receiving this email because you recently created an account or changed your email address. If you did not do this, please contact us.');
    // if($this->email->send()) {
    //   echo 'Email sent.';
    // } else {
    //   show_error($this->email->print_debugger());
    // } 
    

    $this->session->set_flashdata('success', 'Congratulations, You have successfully approved new user');
    redirect('admin/users');
  }

  public function putProfile() {
    $config['file_name'] = 'pict_'.date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/be/users';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    $id = $this->input->post('id_user');
    $passowrd = $this->input->post('password');

    if($_FILES['file']['name'] == "" && empty($password)) {                                 
      $data = array('fullname'            => $this->input->post('fullname'),
                    'username'            => $this->input->post('username'),
                    'email'               => $this->input->post('email'),
                    'departemen'          => $this->input->post('departemen'),
                    'tanggal_lahir'       => $this->input->post('tanggal_lahir'),
                    'alamat'              => $this->input->post('alamat'),
                    'kode_pos'            => $this->input->post('kode_pos'),
                    'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
                    'id_role_user'        => $this->input->post('id_role_user'),
                    'updatedAt'           => $this->input->post('updatedAt')
      );
      $this->m_users->update( $id, $data );
      $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
      redirect('admin/users');
    } elseif($_FILES['file']['name'] != "") {
      $name_array = array();
      $count = count($_FILES['file']['size']);
      foreach($_FILES as $value)
      for($s=0; $s<=$count-1; $s++) {
        $_FILES['file']['name']     = $value['name'][$s];
        $_FILES['file']['type']     = $value['type'][$s];
        $_FILES['file']['tmp_name'] = $value['tmp_name'][$s];
        $_FILES['file']['error']    = $value['error'][$s];
        $_FILES['file']['size']     = $value['size'][$s];   
          $config['file_name'] = 'pict_'.date('Y_m_d_H_i_s').'.jpg';
          $config['upload_path'] = './upload/be/users';
          $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
          $config['max_size'] = '8000';
          $config['max_width']  = '1366';
          $config['max_height']  = '1024';
        $this->load->library('upload', $config);
        $this->upload->do_upload();
        $data = $this->upload->data();
        $name_array[] = $data['file_name'];
      }
      $names = implode(', ', $name_array);

      $data = array('fullname'            => $this->input->post('fullname'),
                    'username'            => $this->input->post('username'),
                    'email'               => $this->input->post('email'),
                    'departemen'          => $this->input->post('departemen'),
                    'tanggal_lahir'       => $this->input->post('tanggal_lahir'),
                    'alamat'              => $this->input->post('alamat'),
                    'kode_pos'            => $this->input->post('kode_pos'),
                    'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
                    'id_role_user'        => $this->input->post('id_role_user'),
                    'updatedAt'           => $this->input->post('updatedAt'),
                    'photo'               => $names
      );
      // var_dump($data);
      $this->m_users->update( $id, $data );
      $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
      redirect('admin/users');
    } elseif($_FILES['file']['name'] == "" && !empty($password)) {
      $data = array('password'            => gkinvestEncrypt($this->input->post('password')),
                    'updatedAt'           => $this->input->post('updatedAt')
      );
      $this->m_users->update( $id, $data );
      $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
      redirect('admin/changepassword');
    }
  }

  /*
   * [DELETE] /post/:id
   *     Remove user from database. Please becareful using this!
   *     @author Rico Oktavian Adhi Wibowo
   */
  public function delete() {
    $id = $this->uri->segment(4); 
    $h=$this->m_users->delete($id); 
    if($h==1451)
      $this->session->set_flashdata('error', 'You failed to delete users.');
    else
      $this->session->set_flashdata('success', 'Congratulations, You have successfully remove user from your list.');
      redirect(base_url('admin/users'));
  }

  /*
   * [VALIDATION] 
   *     Validation form
   *     @author Rico Oktavian Adhi Wibowo
   */
  public function validation(){
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|matches[password]');
    return $this->form_validation->run();
  }
}
