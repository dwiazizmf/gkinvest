<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_users extends CI_Controller {
	function __construct(){
    parent::__construct();
    $this->load->library('session');
    $this->load->model('m_role_users');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

	public function index() {
    $data = array();
    $data['msg']  = $this->_get_flashdata();
    $data['rows'] = $this->m_role_users->get();

		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']    		    = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('role-users/list',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
	}

  /**
   * [add description]
   */
  public function post(){
    $data = array(  'id_role_user'          => $this->input->post('id_role_user'),
                    'role_user'             => $this->input->post('role_user'),
                    'role_user_description' => $this->input->post('role_user_description'),
                    'createdAt'             => $this->input->post('createdAt'),
                    'updatedAt'             => $this->input->post('createdAt')
    );
    $this->m_role_users->post($data);
    redirect('admin/role_users');
  }

  /**
   * [edit proccess]
   * @return [news] [Controller view edit tags and function of put tags ]
   */
  public function edit($id){
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['lists']      = $this->m_role_users->get();
    $data['rows']       = $this->m_role_users->getByID($id);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']           = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('role-users/edit',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  public function put(){
    $id= $this->input->post('id_role_user');
    $data = array(  'role_user'             => $this->input->post('role_user'),
                    'role_user_description' => $this->input->post('role_user_description'),
                    'updatedAt'             => $this->input->post('updatedAt')
    );
    $this->m_role_users->update($id,$data);

    $this->session->set_flashdata('success', 'Congratulations, You have successfully edited data tag from your list.');
    redirect('admin/role_users');
  }

  /**
   * [delete description]
   */
  public function delete(){
    $id = $this->uri->segment(4); 
    $h  = $this->m_role_users->delete($id); 
    if($h==1451)
      $this->session->set_flashdata('message', $this->message->message_error('Sorry, You failed to remove role!'));
    else
      $this->session->set_flashdata('success', 'Congratulations, You have successfully remove role from your tags list.');
      redirect(base_url('admin/role_users'));
  }

  private function _get_flashdata() {
    $msg = $this->session->flashdata("process_msg");
    if (empty($msg))
      return array("type" => "hidden", "content" => "");
    else
      return $msg;
  }
}
