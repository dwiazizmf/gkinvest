<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {
	function __construct() {
    parent::__construct();
    $this->load->library('session');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

	public function index() {
		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']    		    = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('home/home', null, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
	}
}
