<?php
ini_set('max_execution_time', 0); 
ini_set('memory_limit','8192M');
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_data extends MX_Controller {
	function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->model('m_upload');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

  public function index() {
    // $date  = "2020_11_03_10_56";

    // create table temporary
    // $this->m_upload->createTempTable($date);

    // $getTransaction = $this->m_upload->getTransaction();

    // foreach($getTransaction as $row) {
    //   $getTemporary = $this->m_upload->getTemporary($date, $row['id_user']);

    //   $countTemporary = count($this->m_upload->getTemporary($date, $row['id_user']));
    //   if($countTemporary == 0) {
    //     $this->m_upload->insertTempTable($date, $row['id_user'], $row['tgl_in'], $row['tgl_out'], $row['BETMINUTE']);
    //   } else {
    //     foreach($getTemporary as $rowTemporary) {
    //       $firstPoint = $rowTemporary['point'];
    //       $newPoint = $firstPoint+1;

    //       $this->m_upload->updateTempTable($date, $newPoint, $row['id_user']);
    //     }
    //   }
    // }

    /**
     * [$html call all wireframe]
     * @var array
     */

    $this->load->helper('directory');
    $map = directory_map('./upload/be/transact', FALSE, TRUE);
    $data['list_file']      = $map;

    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('upload-data/index', $data, true);
    $html['js']             = $this->load->view('js', null, true);

    $this->load->view('template',$html);
  }

	public function start() {
    $name_file = $this->input->post('file_name');

		$file   = base_url('upload/be/transact/'.$name_file);
    $handle = fopen($file, 'r');
    $c      = 0;
    $date   = date("Y_m_d_h_i_s");
    //var_dump($file);
    //truncate table out and in temp
    $this->m_upload->truncateTransactInOutTemp();

    while(($filesop = fgetcsv($handle, 10000, ",")) !== false) {
      $userId     = $filesop[0];
      $userName   = $filesop[1];
      $password   = $filesop[2];
      $firstName  = $filesop[3];
      $repl       = str_replace(".","-",substr($userId, 0, 19));

      if($c<>0) {
        if($firstName == 'in') {
          $this->m_upload->postTransactInTemp($repl, $userName, $password, $firstName);
        } elseif($firstName == 'out') {
          $this->m_upload->postTransactOutTemp($repl, $userName, $password, $firstName);
        } else {
          $this->m_upload->postTransact($repl, $userName, $password, $firstName);
        }
      }

      $c = $c + 1;
    }

    // create table temporary
    $this->m_upload->createTempTable($date);

    $getTransaction = $this->m_upload->getTransaction();

    foreach($getTransaction as $row) {
      $getTemporary = $this->m_upload->getTemporary($date, $row['id_user']);


    
    // hanya point diatas 2 menit yg di masukan ke dalam temporary
    if($row['BETMINUTE'] >= 2) {
     $this->m_upload->insertTempTable($date, $row['id_user'], $row['tgl_in'], $row['tgl_out'], $row['BETMINUTE']);
        
    }
    
    
      //$countTemporary = count($this->m_upload->getTemporary($date, $row['id_user']));
     // if($countTemporary == 0) {
      //  $this->m_upload->insertTempTable($date, $row['id_user'], $row['tgl_in'], $row['tgl_out'], $row['BETMINUTE']);
    /*  } else {
        foreach($getTemporary as $rowTemporary) {
          $firstPoint = $rowTemporary['point'];
          $newPoint = $firstPoint+1;

          $this->m_upload->updateTempTable($date, $newPoint, $row['id_user']);
        }
      }
      
      */

      $getRewardActive = $this->m_upload->getRewardActive($row['id_user']);
      foreach($getRewardActive as $rowRewardActive) {
        $startPoint = $rowRewardActive['point_reward_active'];

        if($row['BETMINUTE'] >= 2) {
          $lastPoint = $startPoint + 1;

          $this->m_upload->updateRewardActiveTable($lastPoint, $row['id_user']);
          
         
          
          
          
        }
      }
    }

    $this->session->set_flashdata('success', 'Congratulations, You have successfully upload data post');
    redirect('/admin/upload_data/');
	}
}
