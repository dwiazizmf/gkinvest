<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_rewards extends CI_Controller {
	function __construct(){
    parent::__construct();
    $this->load->library('session');
    $this->load->model('m_category_rewards');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

	public function index() {
    $data = array();
    $data['msg']  = $this->_get_flashdata();
    $data['rows'] = $this->m_category_rewards->get();

		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']    		    = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('category-rewards/list',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
	}

  /**
   * [add description]
   */
  public function post(){
    $data = array(  'id_category_reward'          => $this->input->post('id_category_reward'),
                    'category_reward_taxonomy'    => $this->input->post('category_reward_taxonomy'),
                    'category_reward_description' => $this->input->post('category_reward_description'),
                    'createdAt'                   => $this->input->post('createdAt')
                    
    );
    $this->m_category_rewards->post( $data );
    redirect('admin/category_rewards');
  }

  /**
   * [edit proccess]
   * @return [rewards] [Controller view edit tags and function of put tags ]
   */
  public function edit($id){
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['lists']      = $this->m_category_rewards->get();
    $data['rows']       = $this->m_category_rewards->getByID($id);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']           = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('category-rewards/edit',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  public function put(){
    $id= $this->input->post('id_category_reward');
    $data = array(  'category_reward_taxonomy'      => $this->input->post('category_reward_taxonomy'),
                    'category_reward_description'   => $this->input->post('category_reward_description'),
                    'updatedAt'                     => $this->input->post('updatedAt')
    );
    $this->m_category_rewards->update($id, $data);

    $this->session->set_flashdata('success', 'Congratulations, You have successfully edited data category reward from your list.');
    redirect('admin/category_rewards');
  }

  /**
   * [delete description]
   */
  public function delete(){
    $id = $this->uri->segment(4); 
    $h  = $this->m_category_rewards->delete($id); 
    if($h==1451)
      $this->session->set_flashdata('message', $this->message->message_error('Sorry You failed to remove category reward from your list. Please try again!'));
    else
      $this->session->set_flashdata('success', 'Congratulations, You have successfully remove category reward from your list.');
      redirect(base_url('admin/category_rewards'));
  }

  private function _get_flashdata() {
    $msg = $this->session->flashdata("process_msg");
    if (empty($msg))
        return array("type" => "hidden", "content" => "");
    else
        return $msg;
  }
}
