<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends CI_Controller {
	function __construct(){
    parent::__construct();
    $this->load->library('session');
    $this->load->model('m_tags');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

	public function index() {
    $data = array();
    $data['msg']  = $this->_get_flashdata();
    $data['rows'] = $this->m_tags->get();

		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']    		    = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('tags/list',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
	}

  /**
   * [add description]
   */
  public function post(){
    $data = array(  'id_tag_post'         => $this->input->post('id_tag_post'),
                    'tag_taxonomy'        => $this->input->post('tag_taxonomy'),
                    'tag_description'     => $this->input->post('tag_description'),
                    'createdAt'           => $this->input->post('createdAt'),
                    'updatedAt'           => $this->input->post('createdAt')
    );
    $this->m_tags->post($data);
    redirect('admin/tags');
  }

  /**
   * [edit proccess]
   * @return [news] [Controller view edit tags and function of put tags ]
   */
  public function edit($id){
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['lists']      = $this->m_tags->get();
    $data['rows']       = $this->m_tags->getByID($id);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']           = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('tags/edit',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  public function put(){
    $id= $this->input->post('id_tag_post');
    $data = array(  'tag_taxonomy'        => $this->input->post('tag_taxonomy'),
                    'tag_description'     => $this->input->post('tag_description'),
                    'updatedAt'           => $this->input->post('updatedAt')
    );
    $this->m_tags->update($id,$data);

    $this->session->set_flashdata('success', 'Congratulations, You have successfully edited data tag from your list.');
    redirect('admin/tags');
  }

  /**
   * [delete description]
   */
  public function delete(){
    $id = $this->uri->segment(4); 
    $h  = $this->m_tags->delete($id); 
    if($h==1451)
      $this->session->set_flashdata('message', $this->message->message_error('Sorry, You failed to remove tag!'));
    else
      $this->session->set_flashdata('success', 'Congratulations, You have successfully remove tag from your tags list.');
      redirect(base_url('admin/tags'));
  }

  private function _get_flashdata() {
    $msg = $this->session->flashdata("process_msg");
    if (empty($msg))
        return array("type" => "hidden", "content" => "");
    else
        return $msg;
  }
}
