<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends CI_Controller {
	function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper(array('form', 'url')); 
    $this->load->model('m_category_item');
    $this->load->model('m_items');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

  /*
   * [GET] /posts
   *    Get all posts
   *    @author Rico Oktavian Adhi Wibowo
   */
	public function index() {
    $data = array();
    $data['rows'] = $this->m_items->get();

		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']    		    = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('item/list', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
	}

  /*
   * [ADD] /post
   *    Add new post
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function add() {
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array();
    // $data['msg']            = $this->_get_flashdata();
    $data['category_item']  = $this->m_category_item->get();

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('item/add', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
  }

  public function post() { 
    $config['file_name'] = 'pict_'.date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/be/items';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    $category_item  = $this->input->post('category_item');

    if($_FILES['file']['name'] == "") {                                 
      $data = array(  'id_item'               => $this->input->post('id_item'),
                      'id_user'               => $this->input->post('id_user'),
                      'id_category_item'      => $this->input->post('id_category_item'),
                      'item_title'            => $this->input->post('item_title'),
                      'item_content'          => $this->input->post('item_content'),
                      'item_excerpt'          => $this->input->post('item_excerpt'),
                      'item_status'           => $this->input->post('item_status'),
                      'createdAt'             => $this->input->post('createdAt')
      );
      $this->m_items->post( $data );
      $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
      redirect('admin/items');
    } else {
      if (!$this->upload->do_upload('file')){
        echo 'Error Bos';
        echo $this->upload->display_errors();
      }else{
        $image = $this->upload->data(); 
        $data = array(  'id_item'               => $this->input->post('id_item'),
                        'id_user'               => $this->input->post('id_user'),
                        'id_category_item'      => $this->input->post('id_category_item'),
                        'item_title'            => $this->input->post('item_title'),
                        'item_content'          => $this->input->post('item_content'),
                        'item_excerpt'          => $this->input->post('item_excerpt'),
                        'item_status'           => $this->input->post('item_status'),
                        'createdAt'             => $this->input->post('createdAt'),
                        'item_image'            => $image['file_name']
        );
        $this->m_posts->post( $data );
        $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
        redirect('admin/items/add');
        // var_dump($data);
      }
    }
  }

  /*
   * [PATCH] /item/:id
   *     Update item's info by id
   *     @author Rico Oktavian Adhi Wibowo
   */
  public function edit($id) {
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['row']           = $this->m_items->getByID($id);
    $data['category_item']  = $this->m_category_item->get();

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']           = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('item/edit',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  public function put() {
    $config['file_name'] = 'pict_'.date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/be/posts';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    $id = $this->input->post('id_item');
    $category_item  = $this->input->post('category_item');

    if($_FILES['file']['name'] == "") {                                 
        $data = array(  'id_user'               => $this->input->post('id_user'),
                        'id_category_item'      => $this->input->post('id_category_item'),
                        'item_title'            => $this->input->post('item_title'),
                        'item_content'          => $this->input->post('item_content'),
                        'item_excerpt'          => $this->input->post('item_excerpt'),
                        'item_status'           => $this->input->post('item_status'),
                        'updatedAt'             => $this->input->post('updatedAt')
        );
        $this->m_items->patch($id, $data);
        $this->session->set_flashdata('success', 'Congratulations, You have successfully updated an item');
        redirect('admin/items');
    } else {
      if (!$this->upload->do_upload()){
        echo 'Error Bos';
        echo $this->upload->display_errors();
      } else {
        $image = $this->upload->data(); 
        $data = array(  'id_user'               => $this->input->post('id_user'),
                        'id_category_item'      => $this->input->post('id_category_item'),
                        'item_title'            => $this->input->post('item_title'),
                        'item_content'          => $this->input->post('item_content'),
                        'item_excerpt'          => $this->input->post('item_excerpt'),
                        'item_status'           => $this->input->post('item_status'),
                        'updatedAt'             => $this->input->post('updatedAt'),
                        'item_image'            => $image['file_name']
        );
        $this->m_items->patch( $id, $data );
        $this->session->set_flashdata('success', 'Congratulations, You have successfully updated an item');
        redirect('admin/items');
      }
    }
  }

  /*
   * [DELETE] /post/:id
   *     Remove post from database. Please becareful using this!
   *     @author Rico Oktavian Adhi Wibowo
   */
  public function delete() {
    $id = $this->uri->segment(4); 
    $h = $this->m_items->delete($id); 
    if($h==1451)
      $this->session->set_flashdata('message', $this->message->message_error('Sorry You failed to delete item from items list. Please try again!'));
    else
      $this->session->set_flashdata('success', 'Congratulations, You have successfully delete item from items list');
      redirect(base_url('admin/items'));
  }
}
