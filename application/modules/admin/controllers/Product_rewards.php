<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_rewards extends CI_Controller {
	function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper(array('form', 'url')); 
    $this->load->model(array('m_category_rewards', 'm_product_rewards'));
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

  /**
   * get list or find of data
   * @param  {number}  id - id.
   * @param  {object}  params - query parameters.
   * @return {Promise}
   */
	public function index($offset = NULL) {
    $data = array();
    $data['rows'] = $this->m_product_rewards->get(array('limit' => 12, 'offset' => $offset));

    $config['base_url'] = site_url('admin/product_rewards/index');
    $config['total_rows'] = count($this->m_product_rewards->get())+1;
    $config['per_page'] = 12;
    $config['uri_segment'] = 4;
    $this->pagination->initialize($config);

		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']    		    = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('product-rewards/list', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
	}

  /*
   * [ADD] /post
   *    Add new post
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function add() {
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array();
    // $data['msg']           = $this->_get_flashdata();
    $data['categoryrewards']  = $this->m_category_rewards->get();
    $data['productrewards']   = $this->m_product_rewards->get();

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('product-rewards/add', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
  }

  public function post() { 
    $config['file_name'] = 'pict_'.date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/be/product-rewards';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    $category_reward  = $this->input->post('id_category_reward');

    if($_FILES['file']['name'] == "") {                                 
      $data = array('id_reward'             => $this->input->post('id_reward'),
                    'id_category_reward'    => $this->input->post('id_category_reward'),
                    'name_reward'           => $this->input->post('name_reward'),
                    'minimum_deposit'       => $this->input->post('minimum_deposit'),
                    'request_lot'           => $this->input->post('request_lot'),
                    'start_reward'          => $this->input->post('start_reward'),
                    'end_reward'            => $this->input->post('end_reward'),
                    'status_program'        => $this->input->post('status_program'),
                    'is_AB'                 => $this->input->post('is_AB'),
                    'quota_reward'          => $this->input->post('quota_reward'),
                    'detail_reward'         => $this->input->post('detail_reward'),
                    'createdAt'             => $this->input->post('createdAt')
      );
      $this->m_product_rewards->post($data);
      $this->session->set_flashdata('success', 'Congratulations, You have successfully added new product reward');
      redirect('admin/product_rewards');
    } else {
      if (!$this->upload->do_upload('file')){
        echo 'Error Bos';
        echo $this->upload->display_errors();
      }else{
        $image = $this->upload->data(); 
        $data = array('id_reward'             => $this->input->post('id_reward'),
                      'id_category_reward'    => $this->input->post('id_category_reward'),
                      'name_reward'           => $this->input->post('name_reward'),
                      'minimum_deposit'       => $this->input->post('minimum_deposit'),
                      'request_lot'           => $this->input->post('request_lot'),
                      'start_reward'          => $this->input->post('start_reward'),
                      'end_reward'            => $this->input->post('end_reward'),
                      'status_program'        => $this->input->post('status_program'),
                      'is_AB'                 => $this->input->post('is_AB'),
                      'quota_reward'          => $this->input->post('quota_reward'),
                      'detail_reward'         => $this->input->post('detail_reward'),
                      'createdAt'             => $this->input->post('createdAt'), 
                      'photo_reward'          => $image['file_name']
        );
        $this->m_product_rewards->post($data);
        $this->session->set_flashdata('success', 'Congratulations, You have successfully added new product reward');
        redirect('admin/product_rewards/add');
        // var_dump($data);
      }
    }
  }

  /*
   * [PATCH] /post/:id
   *     Update post's info by id
   *     @author Rico Oktavian Adhi Wibowo
   */
  public function edit($id) {
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['rows']             = $this->m_product_rewards->get(array('id' => $id));
    $data['categoryrewards']  = $this->m_category_rewards->get();  

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']           = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('product-rewards/edit',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  public function put() {
    $config['file_name'] = 'pict_'.date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/be/product-rewards';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    $id = $this->input->post('id_reward');

    if($_FILES['userfile']['name'] == "") {                                 
        $data = array('id_reward'             => $this->input->post('id_reward'),
                      'id_category_reward'    => $this->input->post('id_category_reward'),
                      'name_reward'           => $this->input->post('name_reward'),
                      'minimum_deposit'       => $this->input->post('minimum_deposit'),
                      'request_lot'           => $this->input->post('request_lot'),
                      'start_reward'          => $this->input->post('start_reward'),
                      'end_reward'            => $this->input->post('end_reward'),
                      'status_program'        => $this->input->post('status_program'),
                      'is_AB'                 => $this->input->post('is_AB'),
                      'quota_reward'          => $this->input->post('quota_reward'),
                      'detail_reward'         => $this->input->post('detail_reward'),
                      'updatedAt'             => $this->input->post('updatedAt')
        );
        $this->m_product_rewards->patch($id, $data);
        $this->session->set_flashdata('success', 'Congratulations, You have successfully updated product reward');
        redirect('admin/product_rewards');
    } else {
      if (!$this->upload->do_upload()){
        echo 'Error Bos';
        echo $this->upload->display_errors();
      } else {
        $image = $this->upload->data(); 
        $data = array('id_reward'             => $this->input->post('id_reward'),
                      'id_category_reward'    => $this->input->post('id_category_reward'),
                      'name_reward'           => $this->input->post('name_reward'),
                      'minimum_deposit'       => $this->input->post('minimum_deposit'),
                      'request_lot'           => $this->input->post('request_lot'),
                      'start_reward'          => $this->input->post('start_reward'),
                      'end_reward'            => $this->input->post('end_reward'),
                      'status_program'        => $this->input->post('status_program'),
                      'is_AB'                 => $this->input->post('is_AB'),
                      'quota_reward'          => $this->input->post('quota_reward'),
                      'detail_reward'         => $this->input->post('detail_reward'),
                      'photo_reward'          => $image['file_name']
        );
        $this->m_product_rewards->patch($id, $data);
        $this->session->set_flashdata('success', 'Congratulations, You have successfully updated product reward');
        redirect('admin/product_rewards');
      }
    }
  }

  /*
   * [DELETE] /product_reward/:id
   *   Remove product reward from database. Please becareful using this!
   *   @author Rico Oktavian Adhi Wibowo
   */
  public function delete() {
    $id = $this->uri->segment(4); 
    $h=$this->m_product_rewards->delete($id); 
    if($h==1451)
      $this->session->set_flashdata('message', $this->message->message_error('Sorry You failed to remove product reward from list. Please try again!'));
    else
      $this->session->set_flashdata('success', 'Congratulations, You have successfully remove product reward from list.');
      redirect(base_url('admin/product_rewards'));
  }
}
