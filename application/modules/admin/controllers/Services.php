<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {
	function __construct(){
    parent::__construct();
    $this->load->library('session');
    $this->load->model('m_services');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

	public function index() {
    $data = array();
    $data['msg']  = $this->_get_flashdata();
    $data['rows'] = $this->m_services->get();

		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']    		    = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('services/list',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
	}

  /**
   * [add description]
   */
  public function post(){
    $data = array('id_service'        => $this->input->post('id_service'),
                  'id_user'           => $this->session->userdata('id'),
                  'service_taxonomy'  => $this->input->post('service_taxonomy'),
                  'service_url'       => $this->input->post('service_url'),
                  'createdAt'         => $this->input->post('createdAt')
                    
    );
    $this->m_services->post( $data );
    redirect('admin/services');
  }

  /**
   * [edit proccess]
   * @return [rewards] [Controller view edit tags and function of put tags ]
   */
  public function edit($id){
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['lists']      = $this->m_services->get();
    $data['rows']       = $this->m_services->getByID($id);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']           = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('services/edit',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  public function put(){
    $id= $this->input->post('id_service');
    $data = array('id_user'           => $this->session->userdata('id'),
                  'service_taxonomy'  => $this->input->post('service_taxonomy'),
                  'service_url'       => $this->input->post('service_url'),
                  'updatedAt'         => $this->input->post('updatedAt')
    );
    $this->m_services->update($id, $data);

    $this->session->set_flashdata('success', 'Congratulations, You have successfully edited data category reward from your list.');
    redirect('admin/services');
  }

  /**
   * [delete description]
   */
  public function delete(){
    $id = $this->uri->segment(4); 
    $h  = $this->m_services->delete($id); 
    if($h==1451)
      $this->session->set_flashdata('message', $this->message->message_error('Sorry You failed to remove category reward from your list. Please try again!'));
    else
      $this->session->set_flashdata('success', 'Congratulations, You have successfully remove category reward from your list.');
      redirect(base_url('admin/services'));
  }

  private function _get_flashdata() {
    $msg = $this->session->flashdata("process_msg");
    if (empty($msg))
      return array("type" => "hidden", "content" => "");
    else
      return $msg;
  }
}
