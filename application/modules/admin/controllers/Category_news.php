<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_news extends CI_Controller {
	function __construct(){
    parent::__construct();
    $this->load->library('session');
    $this->load->model('m_category_news');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

	public function index() {
    $data = array();
    $data['msg']  = $this->_get_flashdata();
    $data['rows'] = $this->m_category_news->get();

		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']    		    = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('category-news/list',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
	}

  /**
   * [add description]
   */
  public function post(){
    $data = array(  'id_category_post'          => $this->input->post('id_category_post'),
                    'category_post_taxonomy'    => $this->input->post('category_post_taxonomy'),
                    'category_post_description' => $this->input->post('category_post_description'),
                    'createdAt'                 => $this->input->post('createdAt')
                    
    );
    $this->m_category_news->post( $data );
    redirect('admin/category_news');
  }

  /**
   * [edit proccess]
   * @return [news] [Controller view edit tags and function of put tags ]
   */
  public function edit($id){
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['lists']      = $this->m_category_news->get();
    $data['rows']       = $this->m_category_news->getByID($id);

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']           = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('category-news/edit',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  public function put(){
    $id= $this->input->post('id_category_post');
    $data = array(  'category_post_taxonomy'        => $this->input->post('category_post_taxonomy'),
                    'category_post_description'     => $this->input->post('category_post_description'),
                    'updatedAt'                     => $this->input->post('updatedAt')
    );
    $this->m_category_news->update($id,$data);

    $this->session->set_flashdata('success', 'Congratulations, You have successfully edited data category post from your list.');
    redirect('admin/category_news');
  }

  /**
   * [delete description]
   */
  public function delete(){
    $id = $this->uri->segment(4); 
    $h  = $this->m_category_news->delete($id); 
    if($h==1451)
      $this->session->set_flashdata('message', $this->message->message_error('Sorry You failed to remove incoming mail from your inbox'));
    else
      $this->session->set_flashdata('success', 'Congratulations, You have successfully remove category post from your list categories.');
      redirect(base_url('admin/category_news'));
  }

  private function _get_flashdata() {
    $msg = $this->session->flashdata("process_msg");
    if (empty($msg))
        return array("type" => "hidden", "content" => "");
    else
        return $msg;
  }
}
