<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->library( 'session' );
    $this->load->helper( 'form' );
    $this->load->library( 'upload' );
    $this->load->model( 'm_admin' );
  }    

  public function index() {
    if($this->session->userdata('is_login')===TRUE) {
      redirect('admin/home','refresh');
    }
    $data = array('msg'=>$this->_get_flashdata());
    $this->load->view('admin/login');
  }

  public function log() {
    $target     = '';
    $msg        = '';
    $email      = $this->input->post( 'email' );
    $password   = $this->input->post( 'password' );
    $login      = $this->m_admin->login( $email, $password ); 
    if($login == TRUE) {
      $target = 'admin/home';
    } else {
      $msg = array(
              "type"      => "alert-info",
              "content"   => "Username atau Password salah!!!"
             );
      $target = 'admin/login';
    }
    
    $this->session->set_flashdata("process_msg", $msg);
    redirect($target,'refresh');
  }
    
  public function test() {
    $salt = "4ku6anttenGGG";
    $enc = md5($salt.'rahasia');
    echo $enc;
  }

  public function logout() {       
    $this->session->sess_destroy();
    redirect('admin/login','refresh');
  }
    
  private function _get_flashdata() {
    $msg = $this->session->flashdata("process_msg");
    if (empty($msg)) {
      return array("type" => "hidden", "content" => "");
    } else {
      return $msg;
    }
  }
}
