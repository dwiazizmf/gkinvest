<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends CI_Controller {
	function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper(array('form', 'url')); 
    $this->load->model('m_category_news'); 
    $this->load->model('m_tags');
    $this->load->model('m_articles');
    if(!$this->session->userdata('is_login') || $this->session->userdata('status') == 4) {
      redirect('/');
    }
  }

  /**
   * get list or find of data
   * @param  {number}  id - id.
   * @param  {object}  params - query parameters.
   * @return {Promise}
   */
	public function index($offset = NULL) {
    $data = array();
    $data['rows'] = $this->m_articles->get(array('limit' => 12, 'offset' => $offset));

    $config['base_url']   = site_url('admin/articles/index');
    $config['total_rows'] = count($this->m_articles->get())+1;
    $config['per_page']   = 12;
    $config['uri_segment']= 4;
    $this->pagination->initialize($config);

		/**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']    		    = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('articles/list', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
	}

  /*
   * [ADD] /post
   *    Add new post
   *    @author Rico Oktavian Adhi Wibowo
   */
  public function add() {
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array();
    // $data['msg']          = $this->_get_flashdata();
    $data['categorynews'] = $this->m_category_news->get();        
    $data['tagpost']      = $this->m_tags->get();
    $data['news']         = $this->m_articles->get();

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header', null, true);
    $html['menu']           = $this->load->view('menu', null, true);
    $html['content']        = $this->load->view('articles/add', $data, true);
    $html['js']             = $this->load->view('js', null, true);
    $this->load->view('template',$html);
  }

  public function post() { 
    $config['file_name'] = 'pict_'.date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/be/articles';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    $category_post  = $this->input->post('category_post');
    $tag_post       = $this->input->post('tag_post');

    if($_FILES['file']['name'] == "") {                                 
      $data = array(  'id_post'               => $this->input->post('id_post'),
                      'id_user'               => 8,
                      'category_post'         => implode(", ", $category_post),
                      'tag_post'              => implode(", ", $tag_post),
                      'post_title'            => $this->input->post('post_title'),
                      'post_content'          => $this->input->post('post_content'),
                      'post_excerpt'          => $this->input->post('post_excerpt'),
                      'post_status'           => $this->input->post('post_status'),
                      'createdAt'             => $this->input->post('createdAt')
      );
      $this->m_articles->post($data);
      $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
      redirect('admin/articles');
    } else {
      if (!$this->upload->do_upload('file')){
        echo 'Error Bos';
        echo $this->upload->display_errors();
      }else{
        $image = $this->upload->data(); 
        $data = array(  'id_post'               => $this->input->post('id_post'),
                        'id_user'               => 8,
                        'category_post'         => implode(", ", $category_post),
                        'tag_post'              => implode(", ", $tag_post),
                        'post_title'            => $this->input->post('post_title'),
                        'post_content'          => $this->input->post('post_content'),
                        'post_excerpt'          => $this->input->post('post_excerpt'),
                        'post_status'           => $this->input->post('post_status'),
                        'createdAt'             => $this->input->post('createdAt'),
                        'post_image'            => $image['file_name']
        );
        $this->m_articles->post($data);
        $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
        redirect('admin/articles/add');
        // var_dump($data);
      }
    }
  }

  /*
   * [PATCH] /post/:id
   *     Update post's info by id
   *     @author Rico Oktavian Adhi Wibowo
   */
  public function edit($id) {
    /**
     * [$data get data from database]
     * @var array
     */
    $data = array($id);
    $data['rows']           = $this->m_articles->get(array('id' => $id));
    $data['category_post']  = $this->m_category_news->get();         
    $data['tag_post']       = $this->m_tags->get(); 

    /**
     * [$html call all wireframe]
     * @var array
     */
    $html                   = array();
    $html['header']         = $this->load->view('header',null, true);
    $html['menu']           = $this->load->view('menu',null, true);
    $html['content']        = $this->load->view('articles/edit',$data, true);
    $html['js']             = $this->load->view('js',null, true);
    $this->load->view('template',$html);
  }

  public function put() {
    $config['file_name'] = 'pict_'.date('Y_m_d_H_i_s');
    $config['upload_path'] = './upload/be/articles';
    $config['allowed_types'] = 'gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG';
    $config['max_size'] = '8000';
    $config['max_width']  = '1366';
    $config['max_height']  = '1024';
    $this->load->library('upload', $config);

    $id = $this->input->post('id_post');
    $category_post  = $this->input->post('category_post');
    $tag_post  = $this->input->post('tag_post');

    if($_FILES['userfile']['name'] == "") {                                 
        $data = array(  'id_user'               => 8,
                        'category_post'         => implode(", ", $category_post),
                        'tag_post'              => implode(", ", $tag_post),
                        'post_title'            => $this->input->post('post_title'),
                        'post_content'          => $this->input->post('post_content'),
                        'post_excerpt'          => $this->input->post('post_excerpt'),
                        'post_status'           => $this->input->post('post_status'),
                        'updatedAt'             => $this->input->post('updatedAt')
        );
        $this->m_articles->patch($id, $data);
        $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
        redirect('admin/articles');
    } else {
      if (!$this->upload->do_upload()){
        echo 'Error Bos';
        echo $this->upload->display_errors();
      } else {
        $image = $this->upload->data(); 
        $data = array(  'id_user'               => $this->input->post('id_user'),
                        'category_post'         => implode(",", $category_post),
                        'tag_post'              => implode(",", $tag_post),
                        'post_title'            => $this->input->post('post_title'),
                        'post_content'          => $this->input->post('post_content'),
                        'post_excerpt'          => $this->input->post('post_excerpt'),
                        'post_status'           => $this->input->post('post_status'),
                        'updatedAt'             => $this->input->post('updatedAt'),
                        'post_image'            => $image['file_name']
        );
        $this->m_articles->patch($id, $data);
        $this->session->set_flashdata('success', 'Congratulations, You have successfully added new post');
        redirect('admin/articles');
      }
    }
  }

  /*
   * [DELETE] /articles/:id
   *   Remove barang from database. Please becareful using this!
   *   @author Rico Oktavian Adhi Wibowo
   */
  public function delete() {
    $id = $this->uri->segment(4); 
    $h=$this->m_articles->delete($id); 
    if($h==1451)
      $this->session->set_flashdata('message', $this->message->message_error('Sorry You failed to remove news from list. Please try again!'));
    else
      $this->session->set_flashdata('success', 'Congratulations, You have successfully remove news from list.');
      redirect(base_url('admin/articles'));
  }
}
