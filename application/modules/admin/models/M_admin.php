<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class M_admin extends CI_Model{
  /**
   * [__construct description]
   */
  function __construct(){
    parent::__construct();
    $this->load->library( 'session' ); 
  }

  /**
   * [login description]
   * @param  [type] $username [description]
   * @param  [type] $password [description]
   * @return [type]           [description]
   */
  public function login( $email, $password ) {
    $psw   = gkinvestEncrypt( $password );
    $query = $this->db->query("SELECT gkinvest_users.* FROM gkinvest_users 
                               LEFT JOIN gkinvest_role_user 
                               ON gkinvest_users.id_role_user = gkinvest_role_user.id_role_user 
                               WHERE gkinvest_users.email = '$email' AND gkinvest_users.password = '$psw'
                               AND gkinvest_users.status = 1
                               AND gkinvest_users.id_role_user <> 4");
    if ($query->num_rows() == 1) { // jika data = 1
      $data = $query->row_array();
      $newdata = array(
                          'id'            => $data['id_user'],
                          'email'         => $data['email'],
                          'status'        => $data['id_role_user'],
                          'employee_name' => $data['fullname'],                          
                          'username'      => $data['username'],
                          'is_login'      => TRUE
                      );
      $this->session->set_userdata( $newdata );
      return TRUE;
    }else{
      return FALSE; 
    }
  }

  /**
   * [getByID description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function getByID( $id ){
    $this->db->where('id_user', $id);
    $query = $this->db->get('gkinvest_users');
    return $query->row_array();
  }

  /**
   * [update description]
   * @param  [type] $id   [description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function update($id, $data){
    $this->db->where('id_user', $id);
    return $this->db->update('gkinvest_users', $data); 
  }
}
