<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_category_item extends CI_Model {
	public function __construct() {
    parent::__construct();
  }

  public function get(){
    return $this->db
                ->order_by("id_category_item", "desc")
                ->get_where('categoryitem')
                ->result_array();
  }

  public function getByID( $id ) {
    $this->db->where('id_category_item', $id);
    $query = $this->db->get('categoryitem');
    return $query->row_array();
  }

  public function post( $data ){
    $this->db->insert('categoryitem', $data);
  }
  
  public function update( $id, $data ){
    $this->db->where('id_category_item', $id);
    return $this->db->update('categoryitem', $data); 
  }

  public function delete( $id ){
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_category_item',$id);
    $this->db->delete('categoryitem');
  }
}
