<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_items extends CI_Model {
	public function __construct() {
      parent::__construct();
  }

  public function get(){
    return $this->db
                ->order_by("id_item", "desc")
                ->join('categoryitem', 'item.id_category_item = categoryitem.id_category_item', 'left') 
                ->join('user', 'item.id_user = user.id_user', 'left') 
                ->get_where('item')
                ->result_array();
  }

  public function getByID($id) {
    $this->db->join('categoryitem', 'item.id_category_item = categoryitem.id_category_item', 'left');
    $this->db->where('id_item', $id);
    $query = $this->db->get('item');
    return $query->row_array();
  }

  public function post($data){
    $this->db->insert('item', $data);
  }
  
  public function patch($id, $data){
    $this->db->where('id_item', $id);
    return $this->db->update('item', $data); 
  }

  public function delete($id){
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_item',$id);
    $this->db->delete('item');
  }
}
