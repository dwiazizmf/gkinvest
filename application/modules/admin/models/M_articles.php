<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_articles extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function get($params = array()){
    if(isset($params['id'])) {
      $this->db->where('gkinvest_articles.id_post', $params['id']);
    } elseif(isset($params['date'])) {
      $this->db->where('gkinvest_articles.createdAt', $params['date']);
    } 

    if(isset($params['limit'])) {
      if(!isset($params['offset'])) {
        $params['offset'] = NULL;
      }

      $this->db->limit($params['limit'], $params['offset']);
    }

    if(isset($params['order_by'])) {
      $this->db->order_by($params['order_by']);
    } else {
      $this->db->order_by('gkinvest_articles.createdAt', 'desc');
    }

    $res = $this->db->get('gkinvest_articles');

    if(isset($params['id'])) {
      return $res->row_array();
    } else {
      return $res->result_array();
    }
  }

  public function post( $data ){
    $this->db->insert('gkinvest_articles', $data);
  }
  
  public function patch( $id, $data ){
    $this->db->where('id_post', $id);
    return $this->db->update('gkinvest_articles', $data); 
  }

  public function delete($id){
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_post',$id);
    $this->db->delete('gkinvest_articles');
  }
}
