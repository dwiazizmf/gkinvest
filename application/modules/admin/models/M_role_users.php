<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_role_users extends CI_Model {
	public function __construct() {
    parent::__construct();
  }

  public function get() {
    return $this->db
                ->order_by( "id_role_user", "desc" )
                ->get_where( 'gkinvest_role_user' )
                ->result_array();
  }

  public function getByID( $id ) {
    $this->db->where('id_role_user', $id);
    $query = $this->db->get('gkinvest_role_user');
    return $query->row_array();
  }

  public function post( $data ) {
    $this->db->insert('gkinvest_role_user', $data);
  }
    
  public function update( $id, $data ){
    $this->db->where('id_role_user', $id);
    return $this->db->update('gkinvest_role_user', $data); 
  }

  public function delete( $id ) {
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_role_user',$id);
    $this->db->delete('gkinvest_role_user');
  }
}
