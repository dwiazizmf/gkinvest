<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_services extends CI_Model {
	public function __construct() {
    parent::__construct();
  }

  public function get(){
    return $this->db
                ->order_by("id_service", "desc")
                ->get_where('gkinvest_services')
                ->result_array();
  }

  public function getByID( $id ) {
    $this->db->where('id_service', $id);
    $query = $this->db->get('gkinvest_services');
    return $query->row_array();
  }

  public function post( $data ){
    $this->db->insert('gkinvest_services', $data);
  }
  
  public function update( $id, $data ){
    $this->db->where('id_service', $id);
    return $this->db->update('gkinvest_services', $data); 
  }

  public function delete( $id ){
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_service',$id);
    $this->db->delete('gkinvest_services');
  }
}
