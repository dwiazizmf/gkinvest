<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_users extends CI_Model {
	public function __construct() {
    parent::__construct();
  }

  public function get($params = array()){
    if(isset($params['id'])) {
      $this->db->where('id_user', $params['id']);
    } elseif(isset($params['role_user'])) {
      $this->db->where('id_role_user', $params['role_user']);
    } elseif(isset($params['nama'])) {
      $this->db->where('fullname', $params['nama']);
    } elseif(isset($params['email'])) {
      $this->db->where('email', $params['email']);
    } elseif(isset($params['status'])) {
      $this->db->where('status', $params['status']);
    }

    if(isset($params['limit'])) {
      if(!isset($params['offset'])) {
        $params['offset'] = NULL;
      }

      $this->db->limit($params['limit'], $params['offset']);
    }

    $this->db->select('*');
    $this->db->join('gkinvest_role_user', 'gkinvest_role_user.id_role_user = gkinvest_users.id_role_user');
    $res = $this->db->get('gkinvest_users');

    if(isset($params['id'])) {
      return $res->row_array();
    } else {
      return $res->result_array();
    }
  }

  public function getByID( $id ) {
    $this->db->where('id_user', $id);
    $query = $this->db->get('gkinvest_users');
    return $query->row_array();
  }

  public function post( $data ){
    $this->db->insert('gkinvest_users', $data);
  }

  public function update( $id, $data ){
    $this->db->where('id_user', $id);
    return $this->db->update('gkinvest_users', $data); 
  }

  public function delete($id){
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_user',$id);
    $this->db->delete('gkinvest_users');
    // return $this->db->_error_number();
  }
}
