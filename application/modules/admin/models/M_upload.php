<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_upload extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function getTransaction() {
    $query = "SELECT  ti.id_gkinvest as id_user, 
                      ti.transaction_date AS tgl_in, 
                      tou.transaction_date AS tgl_out, 
                      TIMESTAMPDIFF(second, ti.transaction_date, tou.transaction_date) AS BETTSECOND,
                      TIMESTAMPDIFF(MINUTE, ti.transaction_date, tou.transaction_date) AS BETMINUTE
              FROM    gkinvest_transaction_in_temp ti 
              JOIN    gkinvest_transaction_out_temp tou 
              ON      ti.id_transaction = tou.id_transaction";
          
    return $this->db->query($query)->result_array();
  }

  public function getTemporary( $dateNameTable, $idUser ) {
    $query = "SELECT * FROM gkinvest_point_temp_".$dateNameTable." WHERE id_gkinvest='".$idUser."'";
          
    return $this->db->query($query)->result_array();
  }

  public function getRewardActive( $idUser ) {
    $query = "SELECT * FROM gkinvest_reward_active ra JOIN gkinvest_users gu ON ra.id_user=gu.id_user  WHERE gu.id_gkinvest='".$idUser."' AND ra.id_status_reward_active <= 2";
          
    return $this->db->query($query)->result_array();
  }

  public function createTempTable( $dateNameTable ){
    $query = "CREATE TABLE gkinvest_point_temp_".$dateNameTable." (id_gkinvest INT(10), 
                                                                   date_in DATETIME NOT NULL,
                                                                   date_out DATETIME NOT NULL,
                                                                   duration_minute INT(10),
                                        point INT(10) NOT NULL
    )";
    // FOREIGN KEY (id_user) REFERENCES gkinvest_users(id_user)
    $this->db->query($query);
  }  

  public function updateTempTable( $dateNameTable, $newPoint, $idUser ){
    $query = "UPDATE gkinvest_point_temp_".$dateNameTable." SET point = ".$newPoint." WHERE id_gkinvest='".$idUser."'";

    $this->db->query($query);
  }

  public function updateRewardActiveTable( $lastPoint, $idUser ){
  //  $query = "UPDATE gkinvest_reward_active SET point_reward_active = ".$lastPoint." WHERE id_user='".$idUser."'  AND id_status_reward_active <= 2";
// create new query
$query = "UPDATE gkinvest_reward_active SET point_reward_active = ".$lastPoint." WHERE id_user= (SELECT id_user from gkinvest_users where id_gkinvest='".$idUser."')  AND id_status_reward_active <= 2";
    $this->db->query($query);
  } 

  public function insertTempTable( $dateNameTable, $idUser, $dateIn, $dateOut, $betMinute ){
    $query = "INSERT INTO gkinvest_point_temp_".$dateNameTable."(`id_gkinvest`, `date_in`, `date_out`, `duration_minute`, `point`) VALUES('".$idUser."','".$dateIn."','".$dateOut."','".$betMinute."','1') ";

    $this->db->query($query);
  } 

  public function truncateTransactInOutTemp() {
    $query = "TRUNCATE gkinvest_transaction_in_temp";
     $query2 = "TRUNCATE gkinvest_transaction_out_temp";

    $this->db->query($query);
     $this->db->query($query2);
  }

  public function postTransactInTemp( $repl, $userName, $password, $firstName ) {
    $query = "INSERT INTO gkinvest_transaction_in_temp (transaction_date, id_gkinvest, id_transaction, transaction_status
) VALUES('$repl', '$userName', '$password', '$firstName')";

    $this->db->query($query);
  }

  public function postTransactOutTemp( $repl, $userName, $password, $firstName ) {
    $query = "INSERT INTO gkinvest_transaction_out_temp (transaction_date, id_gkinvest, id_transaction, transaction_status
) VALUES('$repl', '$userName', '$password', '$firstName')";

    $this->db->query($query);
  }

  public function postTransact( $repl, $userName, $password, $firstName ) {
    $query = "INSERT INTO gkinvest_transaction (transaction_date, id_user, id_transaction, transaction_status
) VALUES('$repl', '$userName', '$password', '$firstName')";

    $this->db->query($query);
  }
}
