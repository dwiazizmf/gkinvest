<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_testimonials extends CI_Model {
	public function __construct() {
    parent::__construct();
  }

  public function get(){
    return $this->db
                ->order_by("id_testimoni", "desc")
                ->get_where('gkinvest_testimonials')
                ->result_array();
  }

  public function getByID( $id ) {
    $this->db->where('id_testimoni', $id);
    $query = $this->db->get('gkinvest_testimonials');
    return $query->row_array();
  }

  public function post( $data ){
    $this->db->insert('gkinvest_testimonials', $data);
  }
  
  public function update( $id, $data ){
    $this->db->where('id_testimoni', $id);
    return $this->db->update('gkinvest_testimonials', $data); 
  }

  public function delete( $id ){
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_testimoni',$id);
    $this->db->delete('gkinvest_testimonials');
  }
}
