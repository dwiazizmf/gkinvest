<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_product_rewards extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function get($params = array()){
    if(isset($params['id'])) {
      $this->db->where('gkinvest_rewards.id_reward', $params['id']);
    } elseif(isset($params['date'])) {
      $this->db->where('gkinvest_rewards.createdAt', $params['date']);
    } 

    if(isset($params['limit'])) {
      if(!isset($params['offset'])) {
        $params['offset'] = NULL;
      }

      $this->db->limit($params['limit'], $params['offset']);
    }

    if(isset($params['order_by'])) {
      $this->db->order_by($params['order_by']);
    } else {
      $this->db->order_by('gkinvest_rewards.createdAt', 'desc');
    }

    $this->db->select('*');
    $this->db->join('gkinvest_category_rewards', 'gkinvest_category_rewards.id_category_reward = gkinvest_rewards.id_category_reward');
    $res = $this->db->get('gkinvest_rewards');

    if(isset($params['id'])) {
      return $res->row_array();
    } else {
      return $res->result_array();
    }
  }

  public function post( $data ){
    $this->db->insert('gkinvest_rewards', $data);
  }
  
  public function patch( $id, $data ){
    $this->db->where('id_reward', $id);
    return $this->db->update('gkinvest_rewards', $data); 
  }

  public function delete($id){
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_reward',$id);
    $this->db->delete('gkinvest_rewards');
  }
}
