<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_category_rewards extends CI_Model {
	public function __construct() {
    parent::__construct();
  }

  public function get(){
    return $this->db
                ->order_by("id_category_reward", "desc")
                ->get_where('gkinvest_category_rewards')
                ->result_array();
  }

  public function getByID( $id ) {
    $this->db->where('id_category_reward', $id);
    $query = $this->db->get('gkinvest_category_rewards');
    return $query->row_array();
  }

  public function post( $data ){
    $this->db->insert('gkinvest_category_rewards', $data);
  }
  
  public function update( $id, $data ){
    $this->db->where('id_category_reward', $id);
    return $this->db->update('gkinvest_category_rewards', $data); 
  }

  public function delete( $id ){
    $db_debug = $this->db->db_debug;
    $this->db->db_debug = true;
    $this->db->where('id_category_reward',$id);
    $this->db->delete('gkinvest_category_rewards');
  }
}
